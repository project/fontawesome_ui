<?php

namespace Drupal\fontawesome_ui;

/**
 * Interface for FontAwesomeMetadataManager.
 */
interface FontAwesomeMetadataManagerInterface {

  /**
   * Retrieves the list of Font Awesome icon categories.
   *
   * This function returns categories with their respective icons, or a specific
   * category if requested.
   *
   * @param string $version
   *   The version of Font Awesome icons (e.g., 5, 6).
   * @param string $license
   *   The license of library, either 'free' or 'pro'. Default is 'free'.
   * @param string $category_name
   *   The name of the category to retrieve. Default is 'all'.
   *
   * @return array
   *   An array of categories with their respective icons. If a specific
   *   category is requested, only that category and its icons are returned.
   */
  public function getCategories(string $version, string $license = 'free', string $category_name = 'all'): array;

  /**
   * Retrieves the list of Font Awesome all icons.
   *
   * This function allows filtering by license, category, styles, class
   * prefix, and icon name.
   *
   * @param string $version
   *   The version of Font Awesome icons (e.g., 5, 6).
   * @param string $license
   *   The license of library, either 'free' or 'pro'. Default is 'free'.
   * @param array $styles
   *   An array of styles to filter icons by. Default is an empty
   *   array (all styles).
   * @param string $icon_name
   *   A specific icon name to retrieve. Default is an empty string.
   *
   * @return array
   *   An array of icons where the key and value are both the icon name,
   *   optionally including style and class prefix.
   */
  public function getIcons(string $version, string $license = 'free', array $styles = [], string $icon_name = ''): array;

  /**
   * Retrieves categorized Font Awesome icons based on specified parameters.
   *
   * This function allows filtering by version, license, category,
   * styles, and icon name.
   *
   * @param string $version
   *   The version of Font Awesome icons (e.g., 5, 6).
   * @param string $license
   *   The license of library, either 'free' or 'pro'. Default is 'free'.
   * @param array $styles
   *   An array of styles to filter icons by. Default is an empty
   *   array (all styles).
   * @param string $category_name
   *   The name of the category to retrieve. Default is 'all'.
   * @param string $icon_name
   *   A specific icon name to retrieve. Default is an empty string.
   *
   * @return array
   *   An array of categorized icons.
   */
  public function getCategorizedIcons(string $version, string $license = 'free', array $styles = [], string $category_name = 'all', string $icon_name = ''): array;

}
