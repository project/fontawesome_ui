<?php

namespace Drupal\fontawesome_ui;

/**
 * Provides helper functions for Font Awesome icons.
 */
class FontAwesomeIconHelper implements FontAwesomeIconHelperInterface {

  /**
   * {@inheritdoc}
   */
  public static function getIconAnimations() {
    return [
      'none' => t('No Animation'),
      'fa-beat' => t('Beat'),
      'fa-beat-fade' => t('Beat-Fade'),
      'fa-fade' => t('Fade'),
      'fa-bounce' => t('Bounce'),
      'fa-flip' => t('Flip'),
      'fa-shake' => t('Shake'),
      'fa-spin' => t('Spin'),
      'fa-spin-reverse' => t('Spin Reverse'),
      'fa-spin-pulse' => t('Spin-Pulse'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconSizeOptions() {
    return [
      '2xs' => t('2xs'),
      'xs' => t('xs'),
      'sm' => t('sm'),
      'none' => t('Default Size'),
      'lg' => t('lg'),
      'xl' => t('xl'),
      '2xl' => t('2xl'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconFlipOptions() {
    return [
      'none' => t('No Flip'),
      'flip-horizontal' => t('Flip Horizontal'),
      'flip-vertical' => t('Flip Vertical'),
      'flip-both' => t('Flip Both'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconRotateOptions() {
    return [
      'none' => t('No Rotation'),
      'rotate-90' => t('90°'),
      'rotate-180' => t('180°'),
      'rotate-270' => t('270°'),
      'rotate-by' => t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconBorderStyleOptions() {
    return [
      'none' => t('No Style'),
      'dotted' => t('Dotted'),
      'dashed' => t('Dashed'),
      'solid' => t('Solid'),
      'double' => t('Double'),
      'hidden' => t('Hidden'),
      'groove' => t('Groove'),
      'ridge' => t('Ridge'),
      'inset' => t('Inset'),
      'outset' => t('Outset'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconPullOptions() {
    return [
      'none' => t('No Pull'),
      'pull-left' => t('Pull Left'),
      'pull-right' => t('Pull Right'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconDirectionOptions() {
    return [
      'normal' => t('Normal'),
      'reverse' => t('Reverse'),
      'alternate' => t('Alternate'),
      'alternate-reverse' => t('Alternate reverse'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconTimingOptions() {
    return [
      'none' => t('No Timing'),
      'ease' => t('Ease'),
      'ease-in' => t('Ease-In'),
      'ease-out' => t('Ease-Out'),
      'ease-in-out' => t('Ease-In-Out'),
      'linear' => t('Linear'),
      'step-start' => t('Step-Start'),
      'step-end' => t('Step-End'),
      'timing-custom' => t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconColorOptions() {
    return [
      'none' => t('No Color'),
      'color-1' => t('Color 1'),
      'color-2' => t('Color 2'),
      'color-3' => t('Color 3'),
      'color-4' => t('Color 4'),
      'color-5' => t('Color 5'),
      'color-6' => t('Color 6'),
      'color-custom' => t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconPositionOptions() {
    return [
      'before' => t('Before'),
      'prepend' => t('Prepend'),
      'append' => t('Append'),
      'after' => t('After'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIconPositionMarkup() {
    return '
      <div class="element-position">
        <div class="element-outside">
          <span class="element-before">' . t('Before') . '</span>
        </div>
        <div class="element-inside">
          <span class="element-prepend">' . t('Prepend') . '</span>
          <p class="element-selector">' . t('element') . '</p>
          <span class="element-append">' . t('Append') . '</span>
        </div>
        <div class="element-outside">
          <span class="element-after">' . t('After') . '</span>
        </div>
      </div>';
  }

}
