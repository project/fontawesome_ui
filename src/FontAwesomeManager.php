<?php

namespace Drupal\fontawesome_ui;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;

/**
 * Font Awesome icon manager.
 */
class FontAwesomeManager implements FontAwesomeManagerInterface {

  /**
   * The database connection used to check the selector against.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a FontAwesomeManager object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the selector
   *   against.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function isFontAwesome($icon) {
    return (bool) $this->connection->query("SELECT * FROM {fontawesome_icon} WHERE [selector] = :selector", [':selector' => $icon])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function loadFontAwesome() {
    $query = $this->connection
      ->select('fontawesome_icon', 'fa')
      ->fields('fa', ['faid', 'selector', 'options'])
      ->condition('status', 1);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function addFontAwesome($faid, $icon, $label, $comment, $changed, $status, $options) {
    $this->connection->merge('fontawesome_icon')
      ->key('faid', $faid)
      ->fields([
        'selector' => $icon,
        'label'    => $label,
        'comment'  => $comment,
        'changed'  => $changed,
        'status'   => $status,
        'options'  => $options,
      ])
      ->execute();

    return $this->connection->lastInsertId();
  }

  /**
   * {@inheritdoc}
   */
  public function removeFontAwesome($faid) {
    $this->connection->delete('fontawesome_icon')
      ->condition('faid', $faid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findAll(array $header = [], $search = '', $status = NULL) {
    $query = $this->connection
      ->select('fontawesome_icon', 'fa')
      ->extend(PagerSelectExtender::class)
      ->extend(TableSortExtender::class)
      ->orderByHeader($header)
      ->limit(50)
      ->fields('fa');

    if (!empty($search) && !empty(trim((string) $search)) && $search !== NULL) {
      $search = trim((string) $search);
      // Escape for LIKE matching.
      $search = $this->connection->escapeLike($search);
      // Replace wildcards with MySQL/PostgreSQL wildcards.
      $search = preg_replace('!\*+!', '%', $search);
      // Add selector and the label field columns.
      $group = $query->orConditionGroup()
        ->condition('selector', '%' . $search . '%', 'LIKE')
        ->condition('label', '%' . $search . '%', 'LIKE');
      // Run the query to find matching targets.
      $query->condition($group);
    }

    // Check if status is set.
    if (!is_null($status) && $status != '') {
      $query->condition('status', $status);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findById($faid) {
    return $this->connection->query("SELECT [selector], [label], [comment], [status], [options] FROM {fontawesome_icon} WHERE [faid] = :faid", [':faid' => $faid])
      ->fetchAssoc();
  }

}
