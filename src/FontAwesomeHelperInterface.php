<?php

namespace Drupal\fontawesome_ui;

/**
 * Provides helper functions for Font Awesome.
 *
 * Provides methods for converting colors, sizes, and times.
 */
interface FontAwesomeHelperInterface {

  /**
   * Converts a size from one unit to another.
   *
   * @param mixed $size
   *   The size value to be converted. It can be a string or a number.
   * @param string $from_unit
   *   The unit of the size value to be converted from.
   *   Possible values are 'px', 'rem', and 'em'.
   * @param string $to_unit
   *   The unit of the size value to be converted to.
   *   Possible values are 'px', 'rem', and 'em'.
   * @param float $base_font_size
   *   The base font size for 'em' conversion. Default is 16px.
   *
   * @return float
   *   The converted size value.
   *
   * @throws \InvalidArgumentException
   *   Thrown when an unsupported unit is provided.
   */
  public function convertSize($size, $from_unit, $to_unit, $base_font_size = 16.0);

  /**
   * Converts a time from one unit to another.
   *
   * @param mixed $time
   *   The time value to be converted. It can be a string or a number.
   * @param string $from_unit
   *   The unit of the time value to be converted from.
   *   Possible values are 'ms' and 's'.
   * @param string $to_unit
   *   The unit of the time value to be converted to.
   *   Possible values are 'ms' and 's'.
   *
   * @return float
   *   The converted time value.
   *
   * @throws \InvalidArgumentException
   *   Thrown when an unsupported unit is provided.
   */
  public function convertTime($time, $from_unit, $to_unit);

  /**
   * Converts a color from one format to another.
   *
   * @param string|array $color
   *   The color value to convert.
   * @param string $from
   *   The format to convert from. Supported formats: 'hex', 'rgb'.
   * @param string $to
   *   The format to convert to. Supported formats: 'hex', 'rgb'.
   *
   * @return array|string
   *   The converted color value.
   *
   * @throws \InvalidArgumentException
   *   If the provided color format is not supported
   *   or the color value is invalid.
   */
  public function convertColor($color, $from, $to);

  /**
   * Check if a given color is in HEX or RGB format.
   *
   * @param string $color
   *   The color string to check.
   *
   * @return string
   *   Returns 'hex' if the color is in HEX format, 'rgb'
   *   if the color is in RGB format, and 'unknown' otherwise.
   */
  public function getColorFormat(string $color);

}
