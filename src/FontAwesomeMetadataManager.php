<?php

namespace Drupal\fontawesome_ui;

use Drupal\Core\Extension\ExtensionPathResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides manager functions for Font Awesome metadata.
 */
class FontAwesomeMetadataManager implements FontAwesomeMetadataManagerInterface {

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Constructs a new FontAwesomeMetadataManager object.
   *
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(ExtensionPathResolver $extension_path_resolver) {
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCategories(string $version, string $license = 'free', string $category_name = 'all'): array {
    // Convert the major version number to an integer for processing.
    $version_major = intval($version);

    // Normalize the license to lowercase for consistency.
    $license = strtolower($license);

    // Ensure version is 5/6 and license is 'free' or 'pro'.
    if (!in_array($license, ['free', 'pro']) || ($version_major !== 5 && $version_major !== 6)) {
      return [];
    }

    // Define the path to the categories metadata file.
    $module_path = $this->extensionPathResolver->getPath('module', 'fontawesome_ui');
    $categories_path = $module_path . '/metadata/v' . $version_major . '/categories.' . $license . '.yml';

    // Verify that the required metadata files exist before proceeding.
    if (!file_exists($categories_path)) {
      return [];
    }

    // Parse the metadata files to retrieve categories data.
    $categories = Yaml::parse(file_get_contents($categories_path));

    // If a specific category is requested, filter the categories.
    if ($category_name !== 'all' && !empty($category_name)) {
      $category_name_lower = strtolower(trim($category_name));
      return array_filter($categories, fn($category) => strtolower($category['label']) === $category_name_lower);
    }

    return $categories;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons(string $version, string $license = 'free', array $styles = [], string $icon_name = ''): array {
    // Convert the major version number to an integer for processing.
    $version_major = intval($version);

    // Normalize the license to lowercase for consistency.
    $license = strtolower($license);

    // Ensure version is 5/6 and license is 'free' or 'pro'.
    if (!in_array($license, ['free', 'pro']) || ($version_major !== 5 && $version_major !== 6)) {
      return [];
    }

    // Define paths to metadata files for icon families, and sponsors.
    $module_path = $this->extensionPathResolver->getPath('module', 'fontawesome_ui');
    $sponsors_path = $module_path . '/metadata/v' . $version_major . '/sponsors.yml';
    $icon_families_path = $module_path . '/metadata/v' . $version_major . ($version_major === 6 ? '/icon-families.' : '/icons.') . $license . '.yml';

    // Verify that the required metadata files exist before proceeding.
    if (!file_exists($icon_families_path) || !file_exists($sponsors_path)) {
      return [];
    }

    // Parse the metadata files to retrieve icon family and sponsor data.
    $sponsors = Yaml::parse(file_get_contents($sponsors_path));
    $icon_families = Yaml::parse(file_get_contents($icon_families_path));

    // Flatten the sponsor icons into a single array for easy lookup.
    $sponsor_icons = array_merge(...array_column($sponsors, 'icons'));

    // Retrieve valid styles for the given version and license if not provided.
    if (empty($styles)) {
      $styles = fontawesome_ui_icon_styles($version, $license, ['family', 'style']);
    }

    // Process each icon from the icon families metadata.
    $icons = [];
    foreach ($icon_families as $icon => $details) {
      $icon_styles = [];
      $free_styles = [];
      $is_brand = FALSE;
      $is_sponsor = in_array($icon, $sponsor_icons);

      // Helper function to process styles for version 6 or 5.
      $process_styles = function (array $styles_list, string $default_family = 'classic') use (&$icon_styles, &$free_styles, &$is_brand, $styles, $license, $icon) {
        // Sort styles by the order in $styles array.
        usort($styles_list, function ($a, $b) use ($styles) {
          // Find positions of styles in the $styles array.
          $pos_a = array_search($a['style'], array_column($styles, 'style'));
          $pos_b = array_search($b['style'], array_column($styles, 'style'));

          // If positions are not found, place them at the end.
          $pos_a = $pos_a === FALSE ? PHP_INT_MAX : $pos_a;
          $pos_b = $pos_b === FALSE ? PHP_INT_MAX : $pos_b;

          // Compare positions to determine order.
          return $pos_a <=> $pos_b;
        });

        foreach ($styles_list as $style_detail) {
          // Handle version 6 or 5.
          $style = is_array($style_detail) ? $style_detail['style'] : $style_detail;
          $family = $style_detail['family'] ?? $default_family;

          // Add valid styles to the icon styles list.
          if (array_filter($styles, fn($s) => $s['style'] === $style && $s['family'] === $family)) {
            $icon_styles[] = $this->generateIconStyle($icon, $family, $style, $license);
          }

          // Mark as a brand if the style is 'brands'.
          if ($style === 'brands') {
            $is_brand = TRUE;
          }

          // Collect free styles if applicable.
          if ($license === 'free') {
            $free_styles[] = $style;
          }
        }
      };

      // Process styles based on the FontAwesome version.
      if ($version_major === 6 && isset($details['familyStylesByLicense'][$license])) {
        $process_styles($details['familyStylesByLicense'][$license]);
      }
      elseif ($version_major === 5 && isset($details['styles'])) {
        $process_styles($details['styles']);
      }

      // Add the icon metadata if it has valid styles.
      if (!empty($icon_styles)) {
        $icons[$icon] = [
          'label' => $details['label'] ?? '',
          'search_terms' => implode(' ', array_map('strtolower', array_merge(
            [$details['label']],
            [$icon],
            $details['aliases']['names'] ?? [],
            $details['search']['terms'] ?? []
          ))),
          'icons' => $icon_styles,
          'free' => $free_styles,
          'sponsor' => $is_sponsor,
          'brand' => $is_brand,
        ];

        // Return early if a specific icon name is requested and matches.
        if ($icon_name !== 'all' && !empty($icon_name) && strtolower(trim($icon_name)) === strtolower($details['label'])) {
          return [$icon => $icons[$icon]];
        }
      }
    }

    return $icons;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategorizedIcons(string $version, string $license = 'free', array $styles = [], string $category_name = 'all', string $icon_name = ''): array {
    // Convert the major version number to an integer for processing.
    $version_major = intval($version);

    // Normalize the license to lowercase for consistency.
    $license = strtolower($license);

    // Ensure version is 5/6 and license is 'free' or 'pro'.
    if (!in_array($license, ['free', 'pro']) || ($version_major !== 5 && $version_major !== 6)) {
      return [];
    }

    // Retrieve categories and icons.
    $categories = $this->getCategories($version, $license, $category_name);
    $icons = $this->getIcons($version, $license, $styles, $icon_name);

    // Map icons to their respective categories.
    $categorized_icons = [];
    foreach ($categories as $category => $details) {
      // Skip categories not matching the requested name.
      if ($category_name !== 'all' && $category_name !== $category) {
        continue;
      }

      // Filter icons belonging to this category.
      $categorized_icons[$details['label']] = array_filter(
        $icons,
        fn($icon, $icon_key) => in_array($icon_key, $details['icons']),
        ARRAY_FILTER_USE_BOTH
      );
    }

    // Identify and include uncategorized icons.
    $uncategorized_icons = array_diff_key($icons, array_flip(array_merge(...array_column($categories, 'icons'))));
    $categorized_icons['Uncategorized'] = $uncategorized_icons;

    return $categorized_icons;
  }

  /**
   * Generates the CSS class and metadata for an icon style.
   *
   * @param string $icon
   *   The name of the icon.
   * @param string $family
   *   The family of the icon (e.g., 'classic', 'sharp').
   * @param string $style
   *   The style of the icon (e.g., 'solid', 'regular').
   * @param string $license
   *   The license of the icon (e.g., 'free', 'pro').
   *
   * @return array
   *   The generated style details.
   */
  protected function generateIconStyle(string $icon, string $family, string $style, string $license): array {
    // Determine the class prefix based on the family.
    switch ($family) {
      case 'sharp':
        $class_prefix = 'fa-sharp ';
        break;

      case 'duotone':
        $class_prefix = 'fa-duotone ';
        break;

      case 'sharp-duotone':
        $class_prefix = 'fa-sharp-duotone ';
        break;

      default:
        // No prefix for 'classic' or other families.
        $class_prefix = '';
        break;
    }

    // Return the constructed style details.
    return [
      'family' => $style == 'brands' ? 'brands' : $family,
      'style' => $style,
      'class' => $class_prefix . 'fa-' . $style . ' fa-' . $icon,
      'type' => $license,
    ];
  }

}
