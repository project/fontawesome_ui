<?php

namespace Drupal\fontawesome_ui;

/**
 * Provides an interface defining a Font Awesome icon manager.
 */
interface FontAwesomeManagerInterface {

  /**
   * Returns if this Font Awesome icon selector is added.
   *
   * @param string $icon
   *   The Font Awesome icon selector to check.
   *
   * @return bool
   *   TRUE if the Font Awesome icon selector is added, FALSE otherwise.
   */
  public function isFontAwesome($icon);

  /**
   * Finds an added Font Awesome icon selector by its ID.
   *
   * @return string|false
   *   Either the added Font Awesome icon or FALSE if none exist with that ID.
   */
  public function loadFontAwesome();

  /**
   * Add a Font Awesome icon selector.
   *
   * @param int $faid
   *   The Font Awesome id for edit.
   * @param string $icon
   *   The Font Awesome icon selector to add.
   * @param string $label
   *   The label of Font Awesome icon selector.
   * @param string $comment
   *   The comment for Font Awesome icon options.
   * @param int $changed
   *   The expected modification time.
   * @param int $status
   *   The status for Font Awesome icon.
   * @param string $options
   *   The Font Awesome icon selector options.
   *
   * @return int|null|string
   *   The last insert ID of the query, if one exists.
   */
  public function addFontAwesome($faid, $icon, $label, $comment, $changed, $status, $options);

  /**
   * Remove a Font Awesome icon selector.
   *
   * @param int $faid
   *   The Font Awesome id to remove icon.
   */
  public function removeFontAwesome($faid);

  /**
   * Finds all added Font Awesome icon selector.
   *
   * @param array $header
   *   The Font Awesome header to sort selector and label.
   * @param string $search
   *   The Font Awesome search key to filter selector.
   * @param int|null $status
   *   The Font Awesome icon status to filter selector.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll(array $header, $search, $status);

  /**
   * Finds an added Font Awesome icon selector by its ID.
   *
   * @param int $faid
   *   The ID for an added Font Awesome icon selector.
   *
   * @return string|false
   *   Either the added Font Awesome icon or FALSE if none exist with that ID.
   */
  public function findById($faid);

}
