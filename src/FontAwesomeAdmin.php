<?php

namespace Drupal\fontawesome_ui;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\fontawesome_ui\Form\FontAwesomeFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays list of Font Awesome icon selectors.
 *
 * @internal
 */
class FontAwesomeAdmin extends FormBase {

  /**
   * The Font Awesome icon manager.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeManagerInterface
   */
  protected $iconManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new FontAwesomeAdmin object.
   *
   * @param \Drupal\fontawesome_ui\FontAwesomeManagerInterface $icon_manager
   *   The Font Awesome icon manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   */
  public function __construct(FontAwesomeManagerInterface $icon_manager, DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, Request $current_request) {
    $this->iconManager    = $icon_manager;
    $this->dateFormatter  = $date_formatter;
    $this->formBuilder    = $form_builder;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fontawesome.icon_manager'),
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $icon
   *   (optional) Font Awesome icon to be added to selector.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $icon = '') {
    // Attach Font Awesome overview admin library.
    $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.admin';

    $search = $this->currentRequest->query->get('search') ?? '';
    $status = $this->currentRequest->query->get('status') ?? NULL;

    /** @var \Drupal\fontawesome_ui\Form\FontAwesomeFilter $form */
    $form['fontawesome_admin_filter_form'] = $this->formBuilder->getForm(FontAwesomeFilter::class, $search, $status);
    $form['#attributes']['class'][] = 'fontawesome-ui-filter';
    $form['#attributes']['class'][] = 'views-exposed-form';

    $header = [
      [
        'data'  => $this->t('Selector'),
        'field' => 'fa.faid',
      ],
      [
        'data'  => $this->t('Label'),
        'field' => 'fa.label',
      ],
      [
        'data'  => $this->t('Status'),
        'field' => 'fa.status',
      ],
      [
        'data'  => $this->t('Updated'),
        'field' => 'fa.changed',
        'sort'  => 'desc',
      ],
      $this->t('Operations'),
    ];

    $rows = [];
    $result = $this->iconManager->findAll($header, $search, $status);
    foreach ($result as $icon) {
      $row = [];
      $row['selector'] = $icon->selector;
      $row['label'] = $icon->label;
      $status_class = $icon->status ? 'marker marker--enabled' : 'marker';
      $row['status'] = [
        'data' => [
          '#type' => 'markup',
          '#prefix' => '<span class="' . $status_class . '">',
          '#suffix' => '</span>',
          '#markup' => $icon->status ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ];
      $row['updated'] = $this->dateFormatter->format($icon->changed, 'short');
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('fontawesome.edit', ['icon' => $icon->faid]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url'   => Url::fromRoute('fontawesome.delete', ['icon' => $icon->faid]),
      ];
      $links['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'url'   => Url::fromRoute('fontawesome.duplicate', ['icon' => $icon->faid]),
      ];
      $row['operations'] = [
        'data' => [
          '#type'  => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }

    $form['fontawesome_admin_table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#empty'  => $this->t('No Font Awesome icon available. <a href=":link">Add icon</a> .', [
        ':link' => Url::fromRoute('fontawesome.add')
          ->toString(),
      ]),
      '#attributes' => ['class' => ['fontawesome-icon-list']],
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add operations to Font Awesome icon list.
  }

}
