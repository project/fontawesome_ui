<?php

namespace Drupal\fontawesome_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Color as ColorUtility;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\fontawesome_ui\FontAwesomeHelperInterface;
use Drupal\fontawesome_ui\FontAwesomeIconHelperInterface;
use Drupal\fontawesome_ui\FontAwesomeManagerInterface;
use Drupal\fontawesome_ui\FontAwesomeMetadataManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The Font Awesome add and edit icon form.
 *
 * @internal
 */
class FontAwesomeForm extends FormBase {

  /**
   * An array to store the variables.
   *
   * @var array
   */
  protected $variables = [];

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $formConfig;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Font Awesome detection and conversion helper service.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeIconHelperInterface
   */
  protected $unitHelper;

  /**
   * The Font Awesome icon helper service.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeIconHelperInterface
   */
  protected $iconHelper;

  /**
   * The Font Awesome icon manager service.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeManagerInterface
   */
  protected $iconManager;

  /**
   * The Font Awesome metadata manager service.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeMetadataManagerInterface
   */
  protected $metadataManager;

  /**
   * Constructs a new FontAwesome icon object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend service.
   * @param \Drupal\fontawesome_ui\FontAwesomeHelperInterface $unit_helper
   *   The Font Awesome units helper service.
   * @param \Drupal\fontawesome_ui\FontAwesomeIconHelperInterface $icon_helper
   *   The Font Awesome icon helper service.
   * @param \Drupal\fontawesome_ui\FontAwesomeManagerInterface $icon_manager
   *   The Font Awesome icon manager service.
   * @param \Drupal\fontawesome_ui\FontAwesomeMetadataManagerInterface $metadata_manager
   *   The Font Awesome metadata manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TimeInterface $time, RequestStack $request_stack, CacheBackendInterface $cache_backend, FontAwesomeHelperInterface $unit_helper, FontAwesomeIconHelperInterface $icon_helper, FontAwesomeManagerInterface $icon_manager, FontAwesomeMetadataManagerInterface $metadata_manager) {
    $this->config = $config_factory->get('fontawesome_ui.settings');
    $this->formConfig = $config_factory->get('fontawesome_ui.icon_form');
    $this->time = $time;
    $this->requestStack = $request_stack;
    $this->cacheBackend = $cache_backend;
    $this->unitHelper = $unit_helper;
    $this->iconHelper = $icon_helper;
    $this->iconManager = $icon_manager;
    $this->metadataManager = $metadata_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('cache.fontawesome'),
      $container->get('fontawesome.helper'),
      $container->get('fontawesome.icon_helper'),
      $container->get('fontawesome.icon_manager'),
      $container->get('fontawesome.metadata_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_icon_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $icon
   *   (optional) Font Awesome id to be passed on to
   *   \Drupal::formBuilder()->getForm() for use as the default value of the
   *   FontAwesome ID form data.
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $icon = 0) {
    // Load Font Awesome UI global configuration.
    $config = $this->config;

    // Fetch the configured Font Awesome version.
    $version = $config->get('version');

    // Get the settings page URL for Font Awesome.
    $settings_url = Url::fromRoute('fontawesome.settings')->toString();

    // Ensure the version is 5 or higher.
    if (intval($version) < 5) {
      // Display a warning message about minimum version and recommend updates.
      $this->messenger()->addWarning($this->t('You are using Font Awesome version @current_version. To use the Icon Builder, version 5 or higher is required (minimum 5.15.4). For the best experience, we recommend upgrading to version 6.x.',
        ['@current_version' => $version]
      ));

      // Redirect to the settings page for version update.
      return new RedirectResponse($settings_url);
    }

    // Check if the Font Awesome library is set to load.
    if (!$config->get('load')) {
      // Display a warning message instructing the user to enable the library.
      $this->messenger()
        ->addWarning($this->t(
          'Font Awesome is disabled. Please enable it in the <a href=":settings_url">settings</a> to ensure icons display correctly.',
          [':settings_url' => $settings_url]
        ));
    }

    // The FontAwesome UI icon form config settings.
    $formConfig = $this->formConfig;

    // Prepare Font Awesome form default values.
    $fontawesome_id = $icon;
    $icon = $this->iconManager->findById($fontawesome_id) ?? [];
    $selector = $icon['selector'] ?? '';
    $label = $icon['label'] ?? '';
    $comment = $icon['comment'] ?? '';
    $status = $icon['status'] ?? TRUE;
    $options = [];

    // Ensure $icon is an array and 'option' is set to prevent errors.
    if (is_array($icon) && isset($icon['options'])) {
      $options = unserialize($icon['options'], ['allowed_classes' => FALSE]) ?? '';
    }

    // Store Font Awesome icon id.
    $form['fontawesome_id'] = [
      '#type' => 'value',
      '#value' => $fontawesome_id,
    ];

    // Store Font Awesome size unit.
    $size_unit = $formConfig->get('units.size_unit');
    $size_unit_label = ' <span class="fontawesome-unit-flag">' . $size_unit . '</span>';
    $form['size_unit'] = [
      '#type' => 'value',
      '#value' => $options['size_unit'] ?? $size_unit,
    ];

    // Store Font Awesome time unit.
    $time_unit = $formConfig->get('units.time_unit');
    $time_unit_label = ' <span class="fontawesome-unit-flag">' . $time_unit . '</span>';
    $form['time_unit'] = [
      '#type' => 'value',
      '#value' => $options['time_unit'] ?? $time_unit,
    ];

    // Check if conversion is needed for size and time units. If the icon
    // information was previously saved and either the size or time unit has
    // been changed in the settings, then the previous unit should be converted
    // to the new unit when editing the form.
    $convert_size = $fontawesome_id != 0 && isset($options['size_unit']) && $size_unit != $options['size_unit'];
    $convert_time = $fontawesome_id != 0 && isset($options['time_unit']) && $time_unit != $options['time_unit'];

    // Store variables.
    $form['variables'] = [
      '#type' => 'value',
      '#value' => $this->variables,
    ];

    // Store Font Awesome classes after modifying form utilities.
    $form['classes'] = [
      '#type' => 'hidden',
      '#id' => 'fontawesome-classes',
      '#name' => 'classes',
      '#attributes' => [
        'id' => 'icon-classes',
        'class' => ['fontawesome__classes'],
      ],
    ];

    // Container for the selected Font Awesome icon and target element
    // selector fields, including destination position for icon placement.
    $form['main'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['icon-detail-use', 'align-stretch']],
    ];

    // Field and placeholder for icon used by Icon Picker or Icon Browser.
    $form['main']['icon'] = [
      '#title' => $this->t('Icon'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $options['icon'] ?? $formConfig->get('icon'),
      '#attributes' => ['class' => ['fontawesome__icon']],
    ];

    // Container for the target element's selector and label.
    $form['main']['element'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['element-detail-use', 'align-stretch']],
    ];

    // Selector for the target element.
    $form['main']['element']['selector'] = [
      '#title' => $this->t('Selector'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 64,
      '#maxlength' => 256,
      '#default_value' => $selector,
      '#placeholder' => $this->t('Enter a valid selector ...'),
      '#attributes' => ['class' => ['fontawesome__selector']],
    ];

    // Label for the chosen icon for this element selector.
    $form['main']['element']['label'] = [
      '#title' => $this->t('Label'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('Enter a optional label ...'),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#size' => 64,
      '#maxlength' => 64,
      '#default_value' => $label ?? '',
      '#attributes' => ['class' => ['fontawesome__label']],
    ];

    // Container for the target element's icon placement position.
    $form['main']['structure'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['position-detail-use', 'align-stretch']],
    ];

    // Field for the icon placement position.
    $form['main']['structure']['position'] = [
      '#title' => $this->t('Position'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => $this->iconHelper->getIconPositionOptions(),
      '#suffix' => $this->iconHelper->getIconPositionMarkup(),
      '#default_value' => $options['position'] ?? $formConfig->get('position'),
      '#attributes' => ['class' => ['fontawesome__position']],
    ];

    // The Font Awesome animating icon.
    $form['animate'] = [
      '#title' => $this->t('Animate'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['animate']['animation'] = [
      '#title' => $this->t('Animation'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconAnimations(),
      '#default_value' => $options['animation'] ?? $formConfig->get('animate.animation'),
      '#attributes' => ['class' => ['fontawesome__animation']],
    ];
    $form['animate']['custom'] = [
      '#title' => $this->t('Customization'),
      '#type' => 'details',
      '#open' => FALSE,
      '#states' => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['properties'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['fontawesome__properties']],
    ];
    $form['animate']['custom']['properties']['delay'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Delay time'),
      '#description' => $this->t('Set an initial delay for animation.'),
      '#default_value' => $this->getDefaultValue('delay', 'animate.delay', $convert_time, $time_unit, $options),
      '#field_suffix' => $time_unit_label,
      '#attributes' => ['class' => ['fontawesome__delay']],
    ];
    $form['animate']['custom']['properties']['duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#description' => $this->t('Set duration for animation.'),
      '#default_value' => $this->getDefaultValue('duration', 'animate.duration', $convert_time, $time_unit, $options),
      '#field_suffix' => $time_unit_label,
      '#attributes' => ['class' => ['fontawesome__duration']],
    ];
    $form['animate']['custom']['properties']['iteration_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Iteration count'),
      '#description' => $this->t('Set number of iterations for animation	.'),
      '#default_value' => $options['iteration_count'] ?? $formConfig->get('animate.iteration_count'),
      '#attributes' => ['class' => ['fontawesome__iteration']],
    ];
    $form['animate']['custom']['properties']['direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Direction'),
      '#description' => $this->t('Set direction for animation.'),
      '#options' => $this->iconHelper->getIconDirectionOptions(),
      '#default_value' => $options['direction'] ?? $formConfig->get('animate.direction'),
      '#attributes' => ['class' => ['fontawesome__direction']],
    ];
    $form['animate']['custom']['properties']['timing'] = [
      '#type' => 'select',
      '#title' => $this->t('Timing'),
      '#description' => $this->t('Set how the animation progresses through frames.'),
      '#options' => $this->iconHelper->getIconTimingOptions(),
      '#default_value' => $options['timing'] ?? $formConfig->get('animate.timing'),
      '#attributes' => ['class' => ['fontawesome__timing']],
    ];
    $form['animate']['custom']['properties']['timing_custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom timing function'),
      '#description' => $this->t('Enter a custom timing-function, e.g., cubic-bezier(0.1, 0.7, 1, 0.1) or steps(4, end).'),
      '#states' => [
        'visible' => [
          ':input[name="timing"]' => ['value' => 'timing-custom'],
        ],
      ],
    ];

    $form['animate']['beat_fade_wrapper'] = [
      '#title' => $this->t('Beat + Fade'),
      '#type' => 'details',
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          [
            ':input[name="animation"]' => ['value' => 'fa-fade'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat-fade'],
          ],
        ],
      ],
    ];
    $form['animate']['beat_fade_wrapper']['properties'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['fontawesome__properties']],
    ];
    $form['animate']['beat_fade_wrapper']['properties']['scale'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Scale'),
      '#description' => $this->t('Set max value that an icon will scale.'),
      '#default_value' => $options['scale'] ?? $formConfig->get('animate.scale'),
      '#attributes' => ['class' => ['fontawesome__scale']],
      '#states' => [
        'visible' => [
          [
            ':input[name="animation"]' => ['value' => 'fa-beat'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat-fade'],
          ],
        ],
      ],
    ];
    $form['animate']['beat_fade_wrapper']['properties']['opacity'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#min' => 0,
      '#max' => 1,
      '#title' => $this->t('Opacity'),
      '#description' => $this->t('Set lowest opacity value an icon will fade to and from.'),
      '#default_value' => $options['opacity'] ?? $formConfig->get('animate.opacity'),
      '#attributes' => ['class' => ['fontawesome__opacity']],
      '#states' => [
        'visible' => [
          [
            ':input[name="animation"]' => ['value' => 'fa-fade'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat-fade'],
          ],
        ],
      ],
    ];

    $form['animate']['bounce_wrapper'] = [
      '#title' => $this->t('Bounce'),
      '#type' => 'details',
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-bounce'],
        ],
      ],
    ];
    $form['animate']['bounce_wrapper']['properties'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome__properties',
          'bounce-utilities',
        ],
      ],
    ];
    $form['animate']['bounce_wrapper']['properties']['rebound'] = [
      '#type' => 'number',
      '#title' => $this->t('Rebound'),
      '#description' => $this->t('Set the amount of rebound an icon has when landing after the jump.'),
      '#default_value' => $this->getDefaultValue('rebound', 'animate.rebound', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__rebound']],
    ];
    $form['animate']['bounce_wrapper']['properties']['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Max height'),
      '#description' => $this->t('Set the max height an icon will jump to when bouncing.'),
      '#default_value' => $this->getDefaultValue('height', 'animate.height', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__height']],
    ];
    $form['animate']['bounce_wrapper']['properties']['start_scale_x'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Start scale x'),
      '#description' => $this->t('Set the icon’s horizontal distortion (“squish”) when starting to bounce.'),
      '#default_value' => $options['start_scale_x'] ?? $formConfig->get('animate.start_scale_x'),
      '#attributes' => ['class' => ['fontawesome__start_scale_x']],
    ];
    $form['animate']['bounce_wrapper']['properties']['start_scale_y'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Start scale y'),
      '#description' => $this->t('Set the icon’s vertical distortion (“squish”) when starting to bounce.'),
      '#default_value' => $options['start_scale_y'] ?? $formConfig->get('animate.start_scale_y'),
      '#attributes' => ['class' => ['fontawesome__start_scale_y']],
    ];
    $form['animate']['bounce_wrapper']['properties']['jump_scale_x'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Jump scale x'),
      '#description' => $this->t('Set the icon’s horizontal distortion (“squish”) at the top of the jump.'),
      '#default_value' => $options['jump_scale_x'] ?? $formConfig->get('animate.jump_scale_x'),
      '#attributes' => ['class' => ['fontawesome__jump_scale_x']],
    ];
    $form['animate']['bounce_wrapper']['properties']['jump_scale_y'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Jump scale y'),
      '#description' => $this->t('Set the icon’s vertical distortion (“squish”) at the top of the jump.'),
      '#default_value' => $options['jump_scale_y'] ?? $formConfig->get('animate.jump_scale_y'),
      '#attributes' => ['class' => ['fontawesome__jump_scale_y']],
    ];
    $form['animate']['bounce_wrapper']['properties']['land_scale_x'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Land scale x'),
      '#description' => $this->t('Set the icon’s horizontal distortion (“squish”) when landing after the jump.'),
      '#default_value' => $options['land_scale_x'] ?? $formConfig->get('animate.land_scale_x'),
      '#attributes' => ['class' => ['fontawesome__land_scale_x']],
    ];
    $form['animate']['bounce_wrapper']['properties']['land_scale_y'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Land scale y'),
      '#description' => $this->t('Set the icon’s vertical distortion (“squish”) when landing after the jump.'),
      '#default_value' => $options['land_scale_y'] ?? $formConfig->get('animate.land_scale_y'),
      '#attributes' => ['class' => ['fontawesome__land_scale_y']],
    ];

    $form['animate']['flip_wrapper'] = [
      '#title' => $this->t('Flip'),
      '#type' => 'details',
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-flip'],
        ],
      ],
    ];
    $form['animate']['flip_wrapper']['properties'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome__properties',
          'flip-utilities',
        ],
      ],
    ];
    $form['animate']['flip_wrapper']['properties']['flip_x'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flip-X'),
      '#default_value' => $options['flip_x'] ?? $formConfig->get('animate.flip_x'),
      '#attributes' => ['class' => ['fontawesome__flip_x']],
    ];
    $form['animate']['flip_wrapper']['properties']['flip_y'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flip-Y'),
      '#default_value' => $options['flip_y'] ?? $formConfig->get('animate.flip_y'),
      '#attributes' => ['class' => ['fontawesome__flip_y']],
    ];
    $form['animate']['flip_wrapper']['properties']['flip_z'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flip-Z'),
      '#default_value' => $options['flip_z'] ?? $formConfig->get('animate.flip_z'),
      '#attributes' => ['class' => ['fontawesome__flip_z']],
    ];
    $form['animate']['flip_wrapper']['properties']['flip_angle'] = [
      '#type' => 'number',
      '#title' => $this->t('Flip angle'),
      '#title_display' => 'invisible',
      '#default_value' => $options['flip_angle'] ?? $formConfig->get('animate.flip_angle'),
      '#field_suffix' => 'deg',
      '#attributes' => ['class' => ['fontawesome__flip_angle']],
    ];

    $form['animate']['spin_reverse'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Spin reverse'),
      '#default_value' => $options['spin_reverse'] ?? $formConfig->get('animate.spin_reverse'),
      '#attributes' => ['class' => ['fontawesome__spin_reverse']],
      '#states' => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-spin-pulse'],
        ],
      ],
    ];

    // The Font Awesome border + pull icon utilities.
    $form['border_pull'] = [
      '#title' => $this->t('Border + Pull'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['border_pull']['border'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Border'),
      '#default_value' => $options['border'] ?? $formConfig->get('border_pull.border'),
      '#attributes' => ['class' => ['fontawesome__border']],
    ];
    $form['border_pull']['border_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['fontawesome-wrapper', 'form-item'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="border"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['border_pull']['border_wrapper']['border_style'] = [
      '#title' => $this->t('Style'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconBorderStyleOptions(),
      '#default_value' => $options['border_style'] ?? $formConfig->get('border_pull.border_style'),
      '#attributes' => ['class' => ['fontawesome__border_style']],
    ];
    $form['border_pull']['border_wrapper']['properties'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome__properties',
          'bounce-utilities',
        ],
      ],
    ];
    $form['border_pull']['border_wrapper']['properties']['border_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Color'),
      '#title_display' => 'after',
      '#default_value' => $options['border_color'] ?? $formConfig->get('border_pull.border_color'),
      '#attributes' => ['class' => ['fontawesome__border_color']],
    ];
    $form['border_pull']['border_wrapper']['properties']['border_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#title_display' => 'after',
      '#default_value' => $this->getDefaultValue('border_width', 'border_pull.border_width', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__border_width']],
    ];
    $form['border_pull']['border_wrapper']['properties']['border_radius'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Radius'),
      '#title_display' => 'after',
      '#default_value' => $this->getDefaultValue('border_radius', 'border_pull.border_radius', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__border_radius']],
    ];
    $form['border_pull']['border_wrapper']['properties']['border_padding'] = [
      '#type' => 'number',
      '#title' => $this->t('Padding'),
      '#title_display' => 'after',
      '#default_value' => $this->getDefaultValue('border_padding', 'border_pull.border_padding', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__border_padding']],
    ];
    $form['border_pull']['pull'] = [
      '#type' => 'radios',
      '#title' => $this->t('Pull'),
      '#options' => $this->iconHelper->getIconPullOptions(),
      '#default_value' => $options['pull'] ?? $formConfig->get('border_pull.pull'),
      '#attributes' => ['class' => ['fontawesome__pull']],
    ];
    $form['border_pull']['pull_margin'] = [
      '#type' => 'number',
      '#title' => $this->t('Pull margin'),
      '#title_display' => 'after',
      '#default_value' => $this->getDefaultValue('pull_margin', 'border_pull.pull_margin', $convert_size, $size_unit, $options),
      '#field_suffix' => $size_unit_label,
      '#attributes' => ['class' => ['fontawesome__pull_margin']],
      '#states' => [
        'invisible' => [
          ':input[name="pull"]' => ['value' => 'none'],
        ],
      ],
    ];

    // The Font Awesome UI sidebar.
    $form['region'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'fontawesome_sidebar',
        'class' => ['fontawesome-ui-secondary-region'],
      ],
    ];

    // The Font Awesome Icon preview and rebuild.
    $form['region']['preview'] = [
      '#type' => 'details',
      '#title' => $this->t('Font Awesome preview'),
      '#open' => TRUE,
    ];
    $form['region']['preview']['sample'] = [
      '#type' => 'markup',
      '#markup' => '<div class="fontawesome__preview"><p class="fontawesome__sample"><i class="fab fa-brands fa-font-awesome fa-bounce"></i></p></div>',
    ];
    $form['region']['preview']['replay'] = [
      '#value' => $this->t('Rebuild'),
      '#type' => 'image_button',
      '#name' => 'image_button',
      '#src' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAAAXNSR0IArs4c6QAAIABJREFUeF7t3QmYXFWZ//Hfud0dsoBhE9xZFBUiBFLVIZCu6mYRBwF1dEBcQAcFx3FBVEZFZ1xGHRcERsVxcEEBRYj6VxFhEKS7KgGSrgqbRBFFZEYRTGRfku6+7/+pDoYtSVd13VN17z3feh4feeTe95z3815zf+muuuXECwEEEEAAAQSCE3DBdUzDCCCAAAIIICACABcBAggggAACAQoQAAIcOi0jgAACCCBAAOAaQAABBBBAIEABAkCAQ6dlBBBAAAEECABcAwgggAACCAQoQAAIcOi0jAACCCCAAAGAawABBBBAAIEABQgAAQ6dlhFAAAEEECAAcA0ggAACCCAQoAABIMCh0zICCCCAAAIEAK4BBBBAAAEEAhQgAAQ4dFpGAAEEEECAAMA1gAACCCCAQIACBIAAh07LCCCAAAIIEAC4BhBAAAEEEAhQgAAQ4NBpGQEEEEAAAQIA1wACCCCAAAIBChAAAhw6LSOAAAIIIEAA4BpAAAEEEEAgQAECQIBDp2UEEEAAAQQIAFwDCCCAAAIIBChAAAhw6LSMAAIIIIAAAYBrAAEEEEAAgQAFCAABDp2WEUAAAQQQIABwDSCAAAIIIBCgAAEgwKHTMgIIIIAAAgQArgEEEEAAAQQCFCAABDh0WkYAAQQQQIAAwDWAAAIIIIBAgAIEgACHTssIIIAAAggQALgGEEAAAQQQCFCAABDg0GkZAQQQQAABAgDXAAIIIIAAAgEKEAACHDotI4AAAgggQADgGkAAAQQQQCBAAQJAgEOnZQQQQAABBAgAXAMIIIAAAggEKEAACHDotIwAAggggAABgGsAAQQQQACBAAUIAAEOnZYRQAABBBAgAHANIIAAAgggEKAAASDAodMyAggggAACBACuAQQQQAABBAIUIAAEOHRaRgABBBBAgADANYAAAggggECAAgSAAIdOywgggAACCBAAuAYQQAABBBAIUIAAEODQaRkBBBBAAAECANcAAggggAACAQoQAAIcOi0jgAACCCBAAOAaQAABBBBAIEABAkCAQ6dlBBBAAAEECABcAwgggAACCAQoQAAIcOi0jAACCCCAAAGAawABBBBAAIEABQgAAQ6dlhFAAAEEECAAcA0ggAACCCAQoAABIMCh0zICCCCAAAIEAK4BBBBAAAEEAhQgAAQ4dFpGAAEEEECAAMA1gAACCCCAQIACBIAAh07LCCCAAAIIEAC4BhBAAAEEEAhQgAAQ4NBpGQEEEEAAAQIA1wACCCCAAAIBChAAAhw6LSOAAAIIIEAA4BpAAAEEEEAgQAECQIBDp2UEEEAAAQQIAFwDCCCAAAIIBChAAAhw6LSMAAIIIIAAAYBrAAEEEEAAgQAFCAABDp2WEUAAAQQQIABwDSCAAAIIIBCgAAEgwKHTMgIIIIAAAgQArgEEEEAAAQQCFCAABDh0WkYAAQQQQIAAwDWAAAIIIIBAgAIEgACHTssIIIAAAggQALgGEEAAAQQQCFCAABDg0GkZAQQQQAABAgDXAAIIIIAAAgEKEAACHDotI4AAAgggQADgGkAAAQQQQCBAAQJAgEOn5ZwJDF3Zq55Z2+mRaHv19G4nZ9sptu3ktN36Tt1WkvXKnJOLt370f9tC0uxHJR6SbO3kP1t0j5yZ5MYlu3/9/6Y1klYrcmsUuzWasW6NJh5eo+EDxnMmSTtpFRhcuZ9GFlyd1u1ldV8EgKxOjn2HIXDoLVvovnt2VhTtLBfvLLmd5dzOiuOdJD1dck+X09zuYNjdMrdazv1FZn+Q7PeKotvk4t9L0W26a+btWjVvXXf2xqq5ESiPfkxye6hSPCo3PaWkEQJASgbBNgIXGKptr4lovmR7ymlPyV4k2S6Snim5jP7/1GLJ/XEyGJi7WbIb5HpuUNx7o5budXfgE6f9ZgTW3/w/KmkJAaAZsNaOyegfLK01ydEIpEfAIpXr8+Rsb0l7ytz8yf+evNEH9bpd0o2T/zG7Xj3uOg0XbpacBaVAs5sWeOzm3ziGAODhWiEAeEClJAIbBA65fo7Wje2jWIvl4gFZtL+kbRHaqMB9MrdCLl6mSEu1dmyZrt7/YawCFHjizZ8A4OkSIAB4gqVsoAIHLd9O4z0HyjQg2f4yt7ecegPVaK9ts7VyUX0yEJiWamLtlVo2sP6NibzyK/DUmz8BwNO0CQCeYCkbioBFGli5jyI7WOYOlrNBSX2hdN/RPk0TiuLrZO6niqOLtHTBSn5l0NEJ+F9s4zd/AoAneQKAJ1jK5lhgw9/y7WBJh0vuWTnuNr2txbpTzl2myC5Sny7T5cV707tZdjalwKZv/gSAKfGmdwABYHpunBWawP7X76C+sdcoVuOjSCU59YRGkO5+bZ2cXaa45wKtG/uJli+6L937ZXdPENj8zZ8A4OlyIQB4gqVsDgQGbthG0doj5OxIWfQyfrSfkZmaHlEUXy5zSzSx9v/xvoGUz23qmz8BwNMICQCeYCmbUYGhm7bUxCP/IMWvlXMHcdPP6Bwf23bjKYcXyUUX6C8zL+bBRCmbZ3M3fwKAp7ERADzBUjZjAgP1giI7VrJjJLdNxnbPdpsSsMbDh5Zowr6oZQtvauoUDvIn0PzNnwDgaQoEAE+wlM2AwL7XPE0z+46W2dskLcjAjtliUgIurks9Z+kBO0/14kNJlaVOkwKt3fwJAE2ytnoYAaBVMY7PvsBQfdHkTd8m39D3ty/EyX5fdNC6gGm1pHMU21la1n9z6wU4o2WB1m/+BICWkZs7gQDQnBNHZV7AIg2uPExmH5C0OPPt0EDCAmay6ApF+qJGChclXJxyfxOY3s2fAODpCiIAeIKlbEoEGt+m9+A9r1XsPqRIL07JrthGmgVM18rZGYoe+C5feZzgoKZ/8ycAJDiGx5ciAHiCpWyXBRqf2+9d988y9045bdfl3bB8FgVMt8npq5qhr/KQoTYH2N7NnwDQJv+mTicAeIKlbJcEFq98lnomTpHprXJuiy7tgmXzJfBXmc5Q/MgZPFNgGoNt/+ZPAJgGezOnEACaUeKY9AsM1bZXrPdLerekWenfMDvMnIBpjZx9WRNrv0AQaHJ6ydz8CQBNcrd6GAGgVTGOT5fAfldtq76+d0vuJElPS9fm2E0+BewvkvuCovv/U8MHPJLPHhPoKrmbPwEggXFsrAQBwBMsZT0LHFybq7V6r5zew43fszXlNyVwu5z7pB6wb6leHIPpcQLJ3vwJAJ4uLgKAJ1jK+hKwSOXaGyX7vBTt4GsV6iLQtIDpFkX6sEaKS5o+J88HJn/zJwB4ul4IAJ5gKetBYLB2gKTTZZrvoTolEWhPwNzliifeE/Rjhv3c/AkA7V2ZmzybAOAJlrIJCpRXPFfW8ym5xnP6eSGQaoExyc5W5D6s4WLjKYPhvPzd/AkAnq4iAoAnWMomIHDI9XP0yLqTZe4DcpqZQEVKINApgb9K+oR2vPXLWnLURKcW7do6fm/+BABPgyUAeIKlbJsCpdFXyrkzJT27zUqcjkD3BBpfOuT0Vg0vvK57m/C8sv+bPwHA0wgJAJ5gKTtNgYOu2VHr+j7Pj/un6cdp6RMwjcvpK5rZd4oum/9g+jbYxo46c/MnALQxos2dSgDwBEvZaQgM1o5UrP/i0b3TsOOU9AuYfidFb1N1wRXp32wTO+zczZ8A0MQ4pnMIAWA6apyTrEB5+S6Ke/5bkV6abGGqIZA2gclvHTxP42vfo6v3b7xPIJuvzt78CQCerhICgCdYyjYjYE6l2nsmH6YizW7mDI5BICcCf5TpbaoWL85cP52/+RMAPF0kBABPsJSdQmDyd/2935TTy7FCIFgBc+dqVu/bM/PegO7c/AkAnv4PQgDwBEvZzQiUaq+S9DU5bY8TAsELmP1KTm9Qpf/aVFt07+ZPAPB0YRAAPMFSdiMC+101S30zPvPoN/ZBhAACfxMwWyvnPqpK4fOSi1MH092bPwHA0wVBAPAES9knCQytLMri82R6ETYIILAJgcnHCbs3admCP6XGqPs3fwKAp4uBAOAJlrKPEyjXTpT0eUl9uCCAwFQC9pfJXwmM9P98qiO9//t03PwJAJ4GTQDwBEtZSUNXzpRt9V8yvRkPBBBoRcBMcp9TpXBK134lkJ6bPwGglUunhWMJAC1gcWgLAkP1FyiOfyC5vVo4i0MRQOAJAnaRZrhjdHnx3o7CpOvmTwDwNHwCgCfYoMuWRxsf7TtPctsE7UDzCCQj8BtNxK/u2NcMp+/mTwBI5jp6ShUCgCfYMMuaU7n+L5J9WnJRmAZ0jYAXgftldpyq/d/3Uv1vRdN58ycAeBo6AcATbHBlG1/d+/D4d+XsFcH1TsMIdETAbPKpmSOFj0rOEl8yvTd/AkDiw15fkADgCTaoskMrnqHYXSS5YlB90ywC3RFYouj+YzV8wCOJLZ/umz8BILFBP7EQAcATbDBlF6+Yp8hdLOd2CqZnGkWg6wLuKkX2Sg0XV7e9lfTf/AkAbQ954wUIAJ5ggyhbrh8oi38g57YOol+aRCBNAma/let9uSr73DLtbWXj5k8AmPaAN38iAcATbO7LlupvlrOzeLhP1yf9sKTfr/+Pu1Om1XKNB8m4NbJ4jUxr1OPWaNzWrd/pFndP/tdsW7fhC2iGbtpS4xN92uLhSGujuZP/3vXMVGTbKbbtJG2vyO0w+c+N72+I9Uw52+XRn/rwcKduXgKx7lRPfIRGFo62vI3s3PwJAC0Pt7kTCADNOXHU4wUm/+DQv0mO66czV8aYTL+W6UY5/VLO3arYbtMW47fpikV3dmYLG1nlyAt7tOYFz1E8sYss2kVx/AI57Sm5+ZKe17V9hbfwQ5K9TpX+nzTderZu/gSApgfb2oH8Ad6aV+BHm9NA/XRFajzal5cfgYclG5XTcqlxw7cbddecVVo179G/wftZNPGqAzdso551e8u0l5z21oQWKdKLE1+HgusFTOOS+0dVC+dNSZK9mz8BYMqhTu8AAsD03AI8q/EZ/9qXJPeOAJv317Lpz4rimixaqkjLNGvuqC7Zba2/BbtYed9rnqZZPQsVu4MlDcisKOe26OKOcra0mcydqGrxS5tsLJs3fwKApyuVAOAJNldlh67sVbzVtyS9IVd9daOZxte+mquoR5fK3CWqFH7VjW2kYs3JZ0eMDUk6TNKhcto5FfvK9CYaISB6r6qFM57SRnZv/gQAT9ckAcATbG7K7nHTDG370PmK3Ktz01PnG7ldsosUR5dqdu+VG9581/l9pHvFyY+U9rxcssajpEty6kn3htO8O/uIKv2f2rDDbN/8CQCeLjUCgCfYXJQ99JYt9MB9F/J0v2lN84+SfqBISzRcWOblyW3T2lZGTtrvqm3V13e4LDpGLj6QR0tPa26fVaX4QWX/5k8AmNb4pz6JADC1UZhH7HfVLPX1NZ7ud1CYANPp2v4iRRfI6UKNLGjc9OPpVOGcJwmUVzxXil4r6XWSFuDTikB8jRQtauWMlB67RJXiUSndW2a3RQDI7Og8brxQ69OW8Q9l0eEeV8lJaYtl0S/k4nM1NrZEV+/f+Fw+L18C5frukr1J0vGStvW1DHVTJ0AA8DASAoAH1EyXbHy2+67nnyezozPdh+/NN9697/Rt9epr+kXxd76Xo/6TBIaunCnb6gjF7gQ5a3yqgFe+BQgAHuZLAPCAmt2SFqlcP1vSsdntwfPOXVyX3BfkHlii4QPGPa9G+WYEhlYWZfY+xfYPcupt5hSOyZwAAcDDyAgAHlCzWdKcSvUz5fT2bO7f564nP1p1hSJ9USOFi3yuRO02BIau3VnxxEkye4ucm9NGJU5NnwABwMNMCAAeUDNZslz7vKT3Z3Lv/jY9JukcRTpVw8Vf+1uGyokKHLR8O433vEOx3jX53QW88iBAAPAwRQKAB9TMlSzXPijpPzK3b18bNk3I6TzF9gkt7b/V1zLU9SyweOlW6p15kmK9V07rv+SIV1YFCAAeJkcA8ICaqZKDtSNl9j0+Z92YmsWS+4Em7F+1rP/mTM2RzW5aYP0zBd4tufdK2gqqTAoQADyMjQDgATUzJYdqA5rQz+U0MzN79rfRixXZBzXc/0t/S1C5qwKlWuNrjD8suRP4GuuuTmI6ixMApqM2xTkEAA+omSg5MLqrIrtainbIxH59bdLpZsV6n6rFi30tQd2UCQwsf6Gi3jMkOzRlO2M7mxYgAHi4OggAHlBTX7LxJqmxnqskvTD1e/W2Qbtbcp/V6lmnZ+6rdr2ZBFZ4sH6EzBpfmrNrYJ1nsV0CgIepEQA8oKa6ZOMBKhNbNX7sP5DqfXrbXOPJfe6/Nb7uI7p6/796W4bC2RBo/P8h3qrx6ZcPSZqdjU0HuUsCgIexEwA8oKa65EDtO4r0+lTv0dfmzBpfvXu8qv3LfC1B3YwKHFh7vsbjs6TowIx2kPdtEwA8TJgA4AE1tSVLoyfJudNSuz9/G2t8nv80zZn7UV2y21p/y1A52wLmVK4dI7nT+Z6B1E2SAOBhJAQAD6ipLFleWZbiywN89/Pyyb/1V4o3pnIubCp9Agde92yNTXyFr8FO1WgIAB7GQQDwgJq6kotXPktRXJfTM1K3N18bMo3LuU8quu9TPLPfF3LO6w6OHiNzX5b0tJx3moX2CAAepkQA8ICaqpKNr/adFV+hKCqlal8+N2P2B0XRMRopVH0uQ+0ABA5cuZPG4vPCfdNsamZMAPAwCgKAB9RUlSzXzpT0z6nak9/NLFHUc4KG97nH7zJUD0Zg6MpexVt+ROY+IqeeYPpOV6MEAA/zIAB4QE1NyfU/wjwnNfvxu5H7JXe8KoUL/C5D9WAFSisGpajx04DnBGvQvcYJAB7sCQAeUFNRcvJjTbo2iGefm25Rj72ax/im4srL9yaGattrwp0vZwfnu9HUdUcA8DASAoAH1K6XnPyR5VZLJe3b9b343oCLfyrXdww/8vcNTf0NAkde2KM7d/2UpA+g0jEBAoAHagKAB9SulyzXPyk1vvQkzy8zyX1OlcIpkovz3Cm9pVSgNHq0pK/LuTkp3WGetkUA8DBNAoAH1K6WXP8Nf8M5f7PS/XL2Wo30X9JVaxZHYGDlfLn4R3LaGQyvAgQAD7wEAA+oXSt5cG2u1uq6nP9hdIdMh6taXNk1ZxZG4PECQyueodhdJLkiMN4ECAAeaAkAHlC7VrJcO0/SG7q2vu+FY/1SpsO0rHi776Woj0BLAodcP0ePrDtfcke0dB4HNytAAGhWqoXjCAAtYKX60MbvI507P9V7bGtz8f9oYt2RWjZwf1tlOBkBXwLr3xz4xcCeu+FL88l1CQAepAkAHlA7XrLx0aTYVknu6R1fuxMLmjtXPfcdxyN9O4HNGm0LDI7+i0yfkRx/vraNuaEAASA5yw2VuEA9oHa8ZKl+jpwd0/F1O7KgnaVK8e28078j2CySlMBg/XhZ/FXJRUmVDLwOAcDDBUAA8IDa0ZLl+oHrv+Uvj3/bsDNVKb5LctZRUxZDIAmB9b+WazyJsy+JcoHXIAB4uAAIAB5QO1ayUJutOWp8ze2uHVuzcwt9VpXiBzu3HCsh4EFgsH6EYrtQTjM9VA+pJAHAw7QJAB5QO1ZysH6azE7q2HqdW+hDqhQ/07nlWAkBjwKDo4cqdj+RU6/HVfJemgDgYcIEAA+oHSk5uKJfcXR1/h74Yx9Xpf9jHTFkEQQ6IVAe/ZjkPtqJpXK8BgHAw3AJAB5QvZdc/3GjmqS9va/VyQVMp6lafF8nl2QtBLwKcPNPipcAkJTk4+oQADygei85WHu7TF/xvk5HF3DfVGXBW3nDX0fRWcynADf/JHUJAElqPlqLAOAB1WvJoWu3Vjz+m1x95r/xOf/qgjfzUT+vVw7FOynAzT9pbQJA0qKSCAAeUL2WzN0b/+L/UfTg4Tzkx+tVQ/FOCnDz96FNAPCgSgDwgOqt5FD9BZqIfynntvC2RicLO92kPi3W5cV7O7ksayHgTYCbvy9aAoAHWQKAB1RvJcu1n0o6zFv9jha2P0m2SJWF/9vRZVkMAV8C3Px9yTbqEgA86BIAPKB6KVlaeZBc44l/uXjdrygua3jhdbnohiYQ4Obv+xogAHgQJgB4QE28ZONjf3fter1M8xKv3emCpglFdoRG+i/p9NKsh4AXAW7+XlifVJQA4EGZAOABNfGS5dFjJfftxOt2o6DpFFWL/9GNpVkTgcQFuPknTrqJggQAD9IEAA+oiZZc/9CfVZJemGjdbhQz9xNVF7yKz/p3A581Exfg5p846WYKEgA8aBMAPKAmWrI8+hbJfT3Rmt0p9hvN0ELe8d8dfFZNWICbf8KgU5YjAExJ1PoBBIDWzTp3RqHWpzm6WdIunVvUw0pmDyi2RVq28CYP1SmJQGcFuPl31nv9agQAD+oEAA+oiZXMzSN/3dGqFC5IzIVCCHRLgJt/t+QJAB7kCQAeUBMpOXTlTE1sdYucnpNIvW4VcfqWRor/2K3lWReBxAS4+SdGOY1CBIBpoE11CgFgKqFu/fty7URJZ3Rr+YTWvVVrx/fR8kX3JVSPMgh0R4Cbf3fcH1uVAOBhAgQAD6htl1z/u//fSXpu27W6VcA0rigqa2TB1d3aAusikIgAN/9EGNssQgBoE3BjpxMAPKC2XXJw9BiZO6ftOl0t4D6mSuHjXd0CiyPQrgA3/3YFkzqfAJCU5OPqEAA8oLZdsly7VtLebdfpXoEViu5fzDf8dW8ArJyAADf/BBATK0EASIzysUIEAA+obZUcGj1Ysft5WzW6eXLjR/89cT/P+e/mEFi7bQFu/m0TJlyAAJAwaKMcAcADalslS6OXyLm/a6tGN082fULV4ke7uQXWRqAtAW7+bfF5OpkA4AGWAOABddolh2ovVmyrJJfNuTjdLHf/3ho+4JFpG3AiAt0U4ObfTf3NrU0A8DCZbN5oPECkomS5/g3JjkvFXlrehMWK3KCGi0tbPpUTEEiDADf/NExhU3sgAHiYDgHAA+q0Su5//Q7qGfuDnGZO6/xun2T6L1WL/9ztbbA+AtMS4OY/LbYOnkQA8IBNAPCAOq2SpfrJcva5aZ3b9ZPsbvXFu+mKfdd0fStsAIFWBbj5tyrWjeMJAB7UCQAeUKdVsjS6Ss7tPq1zu32S2btU7f9yt7fB+gi0LMDNv2WyLp1AAPAATwDwgNpyydKKQblouOXz0nCC2a/0kJuvenEsDdthDwg0LcDNv2mqFBxIAPAwBAKAB9SWS5Zr50l6Q8vnpeGEyB2q4cKladgKe0CgaQFu/k1TpeRAAoCHQRAAPKC2VHLo2q0VT/xR0uyWzkvDwS7+qUYWHpGGrbAHBJoW4ObfNFWKDiQAeBgGAcADakslB2vvluk/WzonFQdbLIv2UbVwQyq2wyYQaEaAm38zSmk8hgDgYSoEAA+oLZUcrF0n0/yWzknDwabzVS2+Pg1bYQ8INCXAzb8pppQeRADwMBgCgAfUpksOrSwqjkebPj4tB5omFNs8Leu/OS1bYh8IbFaAm3/WLxACgIcJEgA8oDZdslz7vKT3N318eg78tirFN6dnO+wEgc0IcPPPw+VBAPAwRQKAB9TmSppTqX6rnHZu7vjUHDWmXu2uXxR/l5odsREENiXAzT8v1wYBwMMkCQAeUJsqOVDbV5GuaerYdB30DVWKb03XltgNAhsR4Oafp8uCAOBhmgQAD6hNlRysnyazk5o6NjUHmUnRPFUKv0rNltgIAhsT4Oaft+uCAOBhogQAD6hTlzSncv02Sc+b+tg0HWEXqdL/ijTtiL0g8BQBbv55vCgIAB6mSgDwgDplydLoYjmXva/NtXhI1YUjU/bHAQh0S4Cbf7fkfa9LAPAgTADwgDplyYHaGYp04pTHpekAF9c1srCYpi2xFwSeIMDNP88XBAHAw3QJAB5QpyxZrv0hcz/+d/Z6jfSfP2VvHIBANwS4+XdDvZNrEgA8aBMAPKButuTiFfPUE/2y08u2ud4diu5/noYPGG+zDqcjkLwAN//kTdNXkQDgYSYEAA+omy1ZrjUe/NN4AFB2XmafVrX/w9nZMDsNRoCbfyijJgB4mDQBwAPq5gPA6OWSO6jTy05/PTNF0Qs1XPjt9GtwJgIeBLj5e0BNbUkCgIfREAA8oG6y5CHXz9HD69bIuS06uWxba8X6uZYWD2mrBicjkLQAN/+kRdNejwDgYUIEAA+omyxZHn2F5H7cySXbXsvpKI0Ul7RdhwIIJCXAzT8pySzVIQB4mBYBwAPqJkuWal+R09s7uWRba5lWa8u5z9Elu61tqw4nI5CUADf/pCSzVocA4GFiBAAPqJv+CUCt8QU6u3ZyybbWMn1J1eK726rByQgkJcDNPynJLNYhAHiYGgHAA+pGSx5Ye77Gla030jlX1kih2iki1kFg0+F59GOS+yhCwQoQADyMngDgAXWjJUv1N8vZ2Z1arv117E+qFJ8rubj9WlRAoA0B/ubfBl5uTiUAeBglAcAD6sYDQO0sOR3fqeXaXse50zVSeG/bdSiAQDsC3Pzb0cvTuQQAD9MkAHhA3WjJcu0mSXt0arm213HR/hpZcHXbdSiAwHQFuPlPVy6P5xEAPEyVAOAB9SklB27YRtHa1ZKLOrFcAmvcrkphZ8lZArUogUDrAtz8WzfL9xkEAA/zJQB4QH1KyVLtMDn9tBNLJbOGnalK/zuTqUUVBFoU4ObfIlgQhxMAPIyZAOAB9SklB2uflulDnVgqkTVMh6tavDiRWhRBoBUBbv6taIV0LAHAw7QJAB5Qn1KyXB+WbLATS7W9htla9czeXsPzHmi7FgUQaEWAm38rWqEdSwDwMHECgAfUJ5QcurJX8Vb3Sprte6lE6vPs/0QYKdKiADf/FsGCO5wA4GHkBAAPqE8oWVq5h1zc+ARANl5m71W1//RsbJZd5kKAm38uxui5CQKAB2ACgAfUJwaA0aPl3Pm+l0msfqTdNVz8dWL1KITA5gTnaEE5AAAgAElEQVS4+XN9NCdAAGjOqaWjCAAtcU3j4NLop+TcKdM4s/OnmP6savGZnV+YFYMU4OYf5Nin2TQBYJpwmzuNAOAB9QklB1dcJIsO971MIvWdvq+R4pGJ1KIIAvzNn2sgOQECQHKWGyoRADygPqFkufYHSc/zvUwi9c2dpGrhjERqUQSBTQnwN3+ujdYFCACtm015BgFgSqI2Dji4Nlfr7G7JZcPZbF9V+1e00TGnIrB5AW7+XCHTEyAATM9ts2dl48bkofGOlBysl2RW6cha7S/ysFbP2lqr5q1rvxQVENiIADd/LovpCxAApm+3yTMJAB5QN5QcrP2zTGf6XCK52m5ElcJQcvWohMDjBLj5czm0J0AAaM9vo2cTADygbig5UDtDkU70uUSCtU9VpXhygvUohcB6AW7+XAntCxAA2jd8SgUCgAfUDSXLtR9JeqXPJRKrbe4YVQvnJVaPQghw8+caSE6AAJCc5YZKBAAPqI8FgNHrJbeXzyUSq21uvqqFGxKrRyEE+Js/10ByAgSA5CwJAB4sn1qyVLtHTnM7slZ7i4xp9awteQNge4ic/TgBbv5cDskKEACS9Zysxk8APKBOlhy4YRtF6/7qq3yidWP9UkuLeyZak2LhCgyu3E8WnxQuAJ0nLuB0jUaKpyVeN/CCBABfF0CptkBOdV/lE677HVWKb0y4JuUQQAABBFIsQADwNZxy/dWS/cBX+UTrmk5RtfgfidakGAIIIIBAqgUIAL7GU669T9KpvsonW9cdrUrhgmRrUg0BBBBAIM0CBABf0ynX/lPSu32VT7RurEVaWlyeaE2KIYAAAgikWoAA4Gs8g/XzZXa0r/KJ1h3v21FXzb8r0ZoUQwABBBBItQABwNd4BmqXKdJLfZVPrK7Zg6oWt5KcJVaTQggggAACqRcgAPgaUam2Uk77+CqfWF2nmzRSfEli9SiEAAIIIJAJAQKArzGVRm+Tczv5Kp9YXdPPVC0ellg9CiGAAAIIZEKAAOBrTKXRB+TcHF/lE6vr9HWNFI9PrB6FEEAAAQQyIUAA8DGmoStnKt7qYR+lPdT8jCrFD3moS0kEEEAAgRQLEAB8DGfomuco7v1fH6U91DxZlWJGnlfgoXtKIoAAAoEKEAB8DH5g5XxF8XU+Side07njNFI4O/G6FEQAAQQQSLUAAcDHeAbrJZlVfJROvKbZq1Tt/3HidSmIAAIIIJBqAQKAj/EMjr5U5i7zUTr5mvFiVRZelXxdKiKAAAIIpFmAAOBjOuXRl0vuYh+lE69p0TxVF6xKvC4FEUAAAQRSLUAA8DGewfrfy+yHPkonXjO252tp/62J16UgAggggECqBQgAPsZTGj1azp3vo3TiNaPx52p40f8lXpeCCCCAAAKpFiAA+BhPefRYyX3bR+nEa/JFQImTUhABBBDIggABwMeUSrW3yulrPkonXnOGttblxXsTr0tBBBBAAIFUCxAAfIynXH+HZF/2UTrxmmPrZuvq/bPy1MLE26cgAgggEKoAAcDH5EujJ8m503yUTrzmjrf2aslRE4nXpSACCCCAQKoFCAA+xkMA8KFKTQQQQACBBAUIAAlibijFrwB8qFITAQQQQCBBAQJAgpgbSvEmQB+q1EQAAQQQSFCAAJAg5mM/AeBjgD5YqYkAAgggkJwAASA5y8cq8SAgH6rURAABBBBIUIAAkCDmhlI8CtiHKjURQAABBBIUIAAkiPm4XwHwZUA+XKmJAAIIIJCYAAEgMcrHFRoaPVix+7mP0onXNBtQtX9Z4nUpiAACCCCQagECgI/xDNZLMqv4KJ14TbNXqdr/48TrUhABBBBAINUCBAAf4ynV95Kz632UTr6mvUWV/m8mX5eKCCCAAAJpFiAA+JjO4pXPUk/8Rx+lE6/p7AMa6f9c4nUpiAACCCCQagECgI/xHHrLFnrw3kd8lE68prnPqVr4QOJ1KYgAAgggkGoBAoCv8ZRG75dzW/oqn2Ddb6hSfGuC9SiFAAIIIJABAQKAryGVRm+Tczv5Kp9YXdPPVC0ellg9CiGAAAIIZEKAAOBrTIMrarKo4Kt8gnVXqVKcl2A9SiGAAAIIZECAAOBrSOXa/0g6xFf5BOs+rEphjuQswZqUQgABBBBIuQABwNeASrXvyul1vsonWne8b0ddNf+uRGtSDAEEEEAg1QIEAF/jGaidoUgn+iqfaN1Yi7S0uDzRmhRDAAEEEEi1AAHA13gGa++V6Qu+yidb1x2tSuGCZGtSDQEEEEAgzQIEAF/TydI3Ajr3YY0UPu2LgroIIIAAAukTIAD4mkmptkBOdV/lE60b67taWnxDojUphgACCCCQagECgK/xDNywjaJ1f/VVPtG6sX6ppcU9E61JMQQQQACBVAsQAHyOp1S7R05zfS6RSG3TuLacu6Uu2W1tIvUoggACCCCQegECgM8RDdauk2m+zyUSqx3F+2h44XWJ1aMQAggggECqBQgAPsdTrv1I0it9LpFYbWfHaqT/3MTqUQgBBBBAINUCBACf4ymPni659/hcIsHap6pSPDnBepRCAAEEEEixAAHA53BKtX+S03/5XCKx2nFc1dKF5cTqUQgBBBBAINUCBACf4ymNLpZzS30ukVht0yNaM2uuVs1bl1hNCiGAAAIIpFaAAOBzNIuXbqWeLe6VXDaceSSwz6uB2ggggECqBLJxY0oVWYubKdV+L6edWzyrO4c7vU8jxdO6szirIoAAAgh0UoAA4Fu7VP+xnL3C9zKJ1I/th1ra/5pEalEEAQQQQCDVAgQA3+Mp1z8p2Yd9L5NI/Vh3amnxGYnUoggCCCCAQKoFCAC+x1Ouv1ay7/leJrn6bg9VCr9Krh6VEEAAAQTSKEAA8D2Vcn13yVb5Xiax+rwPIDFKCiGAAAJpFiAA+J7O0JW9mtjyHjk3x/dSidQ3d7mqhZcmUosiCCCAAAKpFSAAdGI0pdqVchrqxFJtr2G2Vj2zt9fwvAfarkUBBBBAAIHUChAAOjGaLL0RsOERuyO0tPDTTtCwBgIIIIBAdwQIAJ1wHxw9VOZ+1omlklnDzlSl/53J1KIKAggggEAaBQgAnZjK0LVbKx5fI7moE8slsMb/qlLYSXKWQC1KIIAAAgikUIAA0KmhDNRuVKSXdGq59teJF6uy8Kr261ABAQQQQCCNAgSATk2lXP+qZG/r1HJtrxPrP7W0mJWvMm67XQoggAACoQkQADo18cHRY2TunE4tl8A6d2jHW5+rJUdNJFCLEggggAACKRMgAHRqIAOjuypyv+vUcsmsEw2qsqCSTC2qIIBAxwXKtT3l9OKOrzvtBe0BjfRfMu3TObElAQJAS1xtHlyq/VZOz2+zSgdP59MAHcRmKQSSFyivuFqKFiVf2FNFp+9rpHikp+qUfZIAAaCTl0R59MuSe0cnl2xrLdMabTn32bpkt7Vt1eFkBBDovMDA8hcqin4tuez8OW86XtXi1zuPFeaK2bkw8jCfgfrhiuyibLXiXqtK4cJs7ZndIoCASvXPytm/ZEpiQjtpWfH2TO05w5slAHRyeIdcP0cPj62W08xOLtvWWnw3QFt8nIxAVwSOvLBHd+5yu+Se1ZX1p7Oo000aKWboo9LTaTJd5xAAOj2PgdplipShL9sxU6/bTb8oZuwNjJ0eLOshkCKBwfoRMvtJinY09VZMp6lafN/UB3JEUgIEgKQkm61TrjUu8FObPTwVx5l9WtX+D6diL2wCAQSmFiivuFSKXjb1gSk6wtkhGun/eYp2lPutEAA6PeLSyj3k4ps6vWyb692hB7WT6sWxNutwOgII+BYoX7ubNH5ztt78Zw+q54HtNXzAI755qP+YAAGgG1dDafQ2ObdTN5ZuY803qFL8bhvncyoCCHRCoDx6uuSy9RRPPv7XiSvjKWsQALrBPlg/TWYndWPpNtZcqUqx0Mb5nIoAAr4FJt9ovO7/5NzWvpdKtL7Z61Tt/16iNSk2pQABYEoiDwcM1Rcptqs9VPZb0ulAjRSv9LsI1RFAYNoCpfp75Oz0aZ/fjRNNj2jd+I5avui+biwf8poEgK5M35xK9VvltHNXlp/+oherUjx8+qdzJgIIeBMo1Po0227J4K8Xf6xK8VXeXCi8SQECQLcujnLt85Le363lp7eumSZsTy1bmLU3MU6vXc5CIEsC5dqbJH0rS1ue3KuzYzXSf27m9p2DDRMAujXEwRX9smhFt5af9rpmZ6vaf9y0z+dEBBDwIGBOg/UbZZrnobjPkmOKZ+yopXvd7XMRam9cgADQtSvDnMr1xsN1dunaFqazsGlcPW53DRd+O53TOQcBBDwIlGqvktP/81DZd0l+rehbeDP1CQBdxFe59hlJH+jmFqa1trlzVS0cO61zOQkBBBIWmPzb/7UyzU+4sP9yTkdppLjE/0KssDEBAkA3r4tSbYGc6t3cwrTWNk3IuT1VKfxqWudzEgIIJCdQrh8l2QXJFexUJbtb0QPP4uE/nfJ+6joEgO7Zr1+5XLtW0t7d3kbr69sFqvQf3fp5nIEAAokJNL7058+73Cjndk+sZucKfVWV4ts7txwrPVmAANDta6I0+k4596Vub6P19S2WRfuoWrih9XM5AwEEEhEYHD1G5s5JpFani7hof40syN7zUDrt5HE9AoBH3KZKH1ybq3X6k6TZTR2fpoP4quA0TYO9hCaw31Wz1Dfj15Kel7nWTbeoWnhR4zOAmdt7jjZMAEjDMEv1c+TsmDRspfU92GGq9P+s9fM4AwEE2hIo1z4i6d/bqtGtk537sEYKn+7W8qy7XoAAkIYrobyyLMUjadhKy3sw+63WzJ6nVfPWtXwuJyCAwPQEDrpmR4313iJpq+kV6OpZY5qIdtayBY2ffPLqogABoIv4jy1tTgP1VYr04lRsp9VNmDtJ1cIZrZ7G8QggME2Bcv0bkmX0gVy8gXiaU0/8NAJA4qTTLFiuNR4L3Hg8cPZeZveox+2m4eLq7G2eHSOQMYHJp4i6ayQXZWzn67cbqaTh4tJM7j1nmyYApGWgpZVPl+Lb5TQzLVtqbR92lir9b2vtHI5GAIGWBIau7FW81Wg2Pzrc6NRuUKU/ew8samlI2TmYAJCmWZVqZ8np+DRtqfm9mEnRwaoUftH8ORyJAAItCWTx634f36Bzb9NI4ayWeuZgbwIEAG+00yi8ePRF6tGqzP5oT/qNovvn82SvacyeUxCYSmBoxTM0Ef1aTnOnOjSV/77xq8JZM56jy+Y/mMr9BbgpAkDahl6u/VTSYWnbVvP7cZ9SpdD4eBIvBBBIUqBc+5GkVyZZsqO1TKepWnxfR9dksc0KEADSdoEM1g6QKbs/Rm98W6Czhar0Nx5xzAsBBJIQKNXfKGfnJlGqSzXGNKEXaFnx9i6tz7IbESAApPGyKNcaXxC0II1ba2pP5kbVc9/+Gj5gvKnjOQgBBDYtUKo9U06/lLRtdpncN1UpvCW7+8/nzgkAaZxrufZ6Sd9J49aa35N9XJX+jzV/PEcigMBGBcq1H0r6++zqmMl6XqLqglXZ7SGfOycApHGu6z/q87tMPuN7g2fjy4LsQFUXZvMJh2m8LthTeALl0bdI7uuZbjy2H2pp/2sy3UNON08ASOtgS7V3yemLad1ek/v6vdaO763li+5r8ngOQwCBvwkM1V+g2FZm9HG/j82Rb/1L7TVNAEjraA69ZQs9eG/jWd/PTesWm9qXs/M00p/RLzpqqkMOQiB5gT1umqHtH258VW523ws0qWJXqNJ/cPJAVExCgACQhKKvGoP1E2T2377Kd66ue6MqhYy/p6FzWqyEgAZqpypS9j8yZzagav8yJppOAQJAOueyfleFWp/mqPF937umeZtT7s3sQfVokYb7G+9k5oUAApsTGKwfIYt/LLls//ls+pmqxQw/0yT/l2m2L7D8z0carP+jzL6Z+VYbXxvc09uv4X3uyXwvNICAL4HytbvJJkYz+7S/DS5mcravRhY2vreAV0oFCAApHcyGbR15YY/u2vUmmV6U9q1OuT8X/1Qj/a+UXDzlsRyAQGgCh1w/Rw+NXaNIL8l8607f10jxyMz3kfMGCABZGHC5/gbJzsvCVpvY47+qUvxkE8dxCAIBCZjT4MrvyuzozDdtmpCivfjcf/onSQBI/4ykxk8B/rzrtXLaMwvb3fweG88HcK9QtXhx9nuhAwQSEiiPflhy+QjG5s5VtXBsQjKU8ShAAPCIm2jpcv3AyY/U5OP1kMwOULV/RT7aoQsE2hAojb5GThdm+FtAH9/8w+qNdtcvFvyhDRFO7ZAAAaBD0Iksk/VvA3siwh2a0CK+HCSRK4MiWRUo1RZIVpFzc7LawhP3zSPAszRHAkCWpjX5ZLD4JsnNyNK2N7lX041aNz7AkwJzMU2aaFXgwJU7aTxuPOznma2emtLj/6iZfS/SZfMfTOn+2NaTBAgAWbskBkc/J3MnZ23bmwkBw1oz62VaNW9dbnqiEQSmEhiqba9xVRXpxVMdmpl/b+4YVQt5ebNyZtjb2SgBoB29bpx7cG2u1sW/kaIdurG8pzW/o0rjTUN8PNCTL2XTJDB005aKH268n2dhmrbV5l6Wq1LYT3LWZh1O76AAAaCD2IktNVg/XmZnJVYvFYXsLFWK/8QfIKkYBpvwJbD+Gf8XSTrE1xKdr9t46E/PYo0saPw6g1eGBAgAGRrWhq02PhZ4566Nd9Bn/ItCnoTv3OkaKbw3iyNhzwhMKTD5Nd9P+55keftq3G+oUnzrlP1zQOoECACpG0mTGxpYOV9R3HjMZl+TZ2TjMNMnVC1+NBubZZcINCnQCO137HqOIr2+yTOycZhptXq0u4aLq7OxYXb5eAECQJavh3Lt85Len+UWNrp30ymqFv8jd33RUJgCk4/z3uVbMvfG3AHwxr9Mj5QAkOXxFWqzNUc3Zv7bAjc2A6d/10jx37I8HvaOwORTPO/c9RuS3pQ7DXOXq1p4ae76CqghAkDWhz1YO0DWeEJgxr86dOM/CjhTleK7eGNg1i/SQPffeMPftg+fq0hH5U7AbK1izdey/ptz11tADREA8jDswdrZMr05D608tYfJTwe8nY8I5nO6ue1qv6tmqW+LH0h2aC57dO7DGil8Ope9BdQUASAPwz5o+XYac6ty9myAx0/mO3pQ/6h6cSwP46KHnAvse83TtEXfTyQbzGWn5kbVc9/+Gj5gPJf9BdQUASAvwx6sHSlrfKFITl+xfq6ZOlKXF+/NaYe0lQeBA697tibGL5Zpfh7a2UgPDyvSAg0Xf53T/oJqiwCQp3GXa9+WlN+v4XS6ST3RYXzTWJ4u2hz1snjFPPVEP5P0vBx19cRWnE7USPGLue0vsMYIAHkaeOMRoxMPr5TTbnlq60m93KHYHaGlhXqOe6S1rAk0vq7b7Idympu1rTe/3/gXqvQfzJtymxdL+5EEgLRPqNX9lVfsL4tG5NTb6qmZOd7sAVn0Oi0t/DQze2aj+RUYrL1dpv/M3UO5Hj8x072KtRdf352vy5gAkK95ru+mVPu4nHL+GXqLZe5jqhY+yd9I8ngRZ6Cnyef6P/QlyZ2Qgd22t0Vnx2qk/9z2inB22gQIAGmbSBL7aTxzfGLLipzbL4lyqa5h+plsxhu1dK+7U71PNpcvgcbX+U5oiZyG8tXYRroxO1vV/uNy32eADRIA8jr0A2vP17iulbRVXlvc0JfpFjm9RpVi46mIvBDwKzBYL8nsfEnP9rtQKqqv0oPqV734UCp2wyYSFSAAJMqZsmKDo6+Tue+mbFd+tmP2oBT9k6qF8/wsQFUEzKlc/4BM/57r99g8NuiHNBEv1LKFNzH7fAoQAPI518e6GqidoUgn5r3Nx/W3RPGMt/ErgYAm3olWGw/bWtdzjpxe3onlUrGGc8dppHB2KvbCJrwIEAC8sKaoaKHWp1nxFYqiUop25XcrptvUo2M0XFzqdyGqByEwVP87xfE3JPesIPptNGnuXFUL+X2mSDCD3HyjBIAQLoSDrtlRY72Nz82H8DvLRydqseS+rNWzTtaqeetCGDM9Jiww+Tz/GR+V7GTJRQlXT285p+u1Rd9iXTb/wfRukp0lIUAASEIxCzVKo4vl3JW5/qzyRudgNcU9b9XSBddnYUzsMSUCgyv3Uxx/O+cP1doItv1FvT39PG0zJdeh520QADwDp6p8qfYuOYX3GE/TuJy+opl9p/C3mlRdkenbTKE2W3P0bzK9X0496dug1x2NKbJDNNw/7HUViqdGgACQmlF0aCOl+jlydkyHVkvbMr+RouNVWVBJ28bYTwoEBuqHK7KvSHpuCnbT+S04vUMjxUb/vAIRIAAEMugNbR56yxZ68J7LJFcOrfX1/ZrJ3NfVo1M0XFwdpgFdP0GgvOK5UvQFSUcGK2P6mqrF/D/RMNgBb7xxAkCIF8R+V22rGTOukulFIbb/aA64R9In9JD7surFsWAdQm586MqZmtjqZDl9UNLscCnciFbPPIQ3y4Z3BRAAwpv5+o4nnxRoV0vu6aESTPbtdLNivU/V4sVBO4TWfLn+aslOlbRLaK0/qd9VimcM8NyMMK8CAkCYc1/fdeOdzhZfIWlWyAyP/kTgUin6gKqFG4K3yDPAUG1AsfusZPvnuc3merM/qbdnf97x35xWHo8iAORxqq30VBr9BzldENTnnDfpYyZnF8vpXzW88LpWGDk25QLl+u6SfTzo3/M/cUT3SzaoSn/j+0J4BSpAAAh08E9ou1Q/Wc4+B8XfBCYfInS+1PNxVfa5BZcMCwyNvkQT7hRJRwX4sb5NDW5M5o5QtfA/GZ4sW09AgACQAGIuSpRrn5H0gVz0klQT658f8B1FdqqG+3+ZVFnqdECgVFsgp49I9kp+uvV478anYKLjVC18qwNTYImUCxAAUj6gjm6vXP+SZO/s6JqZWMxMii6VuS+ouqDxngleqRSY/La+l8rsJDm9THL8+fbkOZnerWrxS6kcH5vquAD/B+k4eZoXNKfB+jdlenOad9nlvV0nuVP1oF3Ixwe7PIm/Lb/+43xvVKT3yDQvJbtK4zY+pEqx8ZM+XghMChAAuBCeKHDkhT26Y9fvKtJR0GxGwPRnOX1b6vkG7xPo0pUysPyF6oneuj6wBv5x1qlG4PTvGin+21SH8e/DEiAAhDXv5rptfIXwlvEPZdHhzZ0Q+FEurks9Z+kBO0/14kOBa/htv/Eky4fufYVid4JcfBA/5m+K+4uqFE9s6kgOCkqAABDUuFtodvLHqk+7SM4ObuGs0A/9q+SWyNkF2uHWipYcNRE6SCL9N34qdefzByV7vcxeI+e2TqRuGEW+okrhnZKzMNqly1YECACtaIV2bOP70HtnfF9OLw+t9bb7nfwVgWvYXaiRBcskF7ddM6gCFmnw2n0lO1IWv1Zyzwqq/USatTNUKb6Xm38imLksQgDI5VgTbGqPm2Zo+0e+K9lrEqwaWqk/SrpIZpeqZ/YVGp73QGgATfXb+Cre2fZSOXeEFB8hRTs0dR4HbUzgM6oUPwQNApsTIABwfUwtMHRlryae9s2Av0Z4aqOmj7B1ki2V9VwqZ5eqUryx6VNzd6BFKtX3ltxBj/6qqcRjqZMYsvuYKoXGUw95IbBZAQIAF0iTAhapvPIrkr2tyRM4rCmB+C4pulrSUim+SnO2qeuS3dY2dWrWDmq8uXSWFijSIkmLZTpATttnrY2U7/eDqhQ/m/I9sr2UCBAAUjKIbGyj8ZyAlV+YfNAKLz8Cpkck1WRarkg3ynSjtpx7U+ZCQeNmP9vtLmkvyfaR074yFeQ00w9c4FVNE3LuRFUKZwYuQfstCBAAWsDi0EcFSrV/lWt8sQpPWuvINbH+kcS/kexGyTUeSXyrFN8mi36vavGOjuxhU4sctHw7rYueL+deIKcXyPRCOb1EZrtLbkZX9xbK4o3Q6NwbVCn8MJSW6TMZAQJAMo7hVRkcPUamr/OHfJdH3/jD33SbIvd7Wdz45MEaSaslWy1zaxS5NXLx3RrX+ucT9Lr7ND5jQr09YxvejNh4o+e2E3Mm//3EA049fVtLPVsp0lw5myvFW0tuG5meKelZivVM9Uz+93PkNLfLAqEv/1dFeqWGi0tDh6D/1gUIAK2bccbfBEqjiyX3I36PyyWBQDcE7E+y6FBVCzd0Y3XWzL4AASD7M+xuB6WVe0jxxXLaubsbYXUEQhKwG9Tb93L9Yu/GR0x5ITAtAQLAtNg46QkCQ7XtFetHk+/s5oUAAp4F3CWaYa/T5cV7PS9E+ZwLEAByPuCOtdd4iMuW+o5Mr+rYmiyEQFACZnLuMxopfIQnSwY1eG/NEgC80YZYePL72P9Fpk/JqSdEAXpGwIuA2Vo5naBK/zle6lM0SAECQJBj99z0YO0AWfw9HuXq2ZnygQg03uynv1e1f0UgDdNmhwQIAB2CDm6Z8vJdpJ7G55L3Dq53GkYgMQGryNzRXX/eQ2L9UChNAgSANE0jb3tpfKVw/LQzJTsub63RDwJ+Bcwk9yU9qPerXhzzuxbVQxUgAIQ6+U72XRp9p5y+wEODOonOWpkVMK1WZMdqpP+SzPbAxjMhQADIxJhysMmh0Zco1nckt1cOuqEFBPwImBtVT3SUhve5zc8CVEXgMQECAFdD5wQaHxWc0/hJgP6pc4uyEgIZEJj8Mh99XtH9/6rhA8YzsGO2mAMBAkAOhpi5Fkr1l8nZ2dLks+V5IRC2gNkfJHuTqgtHwoag+04LEAA6Lc566wX2v34H9Y59U9JhkCAQsMASRT0naHifewI2oPUuCRAAugTPsg2ByQcHvVvSpyXNxgSBcATiu2TuBFX7fxxOz3SaNgECQNomEuJ+Gs8MiHv+W5FeGmL79ByYgNP3Ndb3Dl01/67AOqfdlAkQAFI2kKC3M1g7UrG+wtcLB30V5Ln5O2T2LlX7f5DnJuktOwIEgOzMKoydNt4b0DN+qpwdE0bDdJl/gcZDfdnU7tcAAAkwSURBVPQ1rZ04WcsX3Zf/fukwKwIEgKxMKrR9DtQPV2RfkfTc0Fqn3xwJmG5UjztBw4VrctQVreREgACQk0Hmso1Drp+jR8Y/JLP3yWlmLnukqXwKmO5VpE/oAX2JR/nmc8R56IoAkIcp5r2HoWueo4m+T8vFb5Qc12ze553p/iyWRd/RjLGTdcWiOzPdCpvPvQB/mOZ+xDlqcKC2r6L4DClalKOuaCU/AisUuRP5cX9+Bpr3TggAeZ9w7vqzSKX6cXLxp6Roh9y1R0NZFPi9zE5RtXiB5Bpv+OOFQCYECACZGBObfIrAvtc8TTN736PYTpJzWyOEQBcE7pDcp7R65te0at66LqzPkgi0JUAAaIuPk7susHjpVuqZ+c8yfUhOc7u+HzYQgsBfJfuSJtZ+QcsG7g+hYXrMpwABIJ9zDa+rodr2MvsXmXsHjxUOb/wd6vh+yX1RUXQqz+7vkDjLeBUgAHjlpXjHBYZWPENx9CGZTuCjgx3Xz+eCpjWK9EWtW/dlXb3/X/PZJF2FKEAACHHqIfRcWvl0ufg4SSfytcMhDNxDj7HuVGRf1Qx3ui4v3uthBUoi0FUBAkBX+Vncu8B+V81S74w3SXqvnHbzvh4LZF/AdIukz2nLuefqkt3WZr8hOkBg4wIEAK6MQAQs0uDKwxTr3XJ2cCBN02bTApMP8PmFIjtLO9z6Qy05aqLpUzkQgYwKEAAyOji23YZA44FCzt4m6Sg5N6eNSpyadYHGI3udna0oOlPDhd9mvR32j0ArAgSAVrQ4Nl8CjWcJzOh9vZyOl7QgX83RzWYFXFxXHH1dPbPO0/C8B9BCIEQBAkCIU6fnpwosXjFPPVHjK4gbYWBbiPIoYHdLWiJz/61qcWUeO6QnBFoRIAC0osWx+ReY/AbCda+R3FGSvVRyM/LfdK47HJP0M5mdrYfcz/hmvlzPmuZaFCAAtAjG4QEJDF27teLxV8jZkbLoZZL6Auo+u62aJuR0zeTf9vvGv8e38mV3lOzcrwABwK8v1fMisN9V26qv73DCQEoH+vib/njf+bpq/l0p3SnbQiA1AgSA1IyCjWRG4KDl22ld9FI5d6hivUyRdszM3vO0UdOfFelSmbtUM+xSHtaTp+HSSycECACdUGaNfAusfwPh4TJ3sJwN8qsCT+Nu/C0/iq+TRZcr0k81XLhKcrGn1SiLQO4FCAC5HzENdlRg4IZtFI0dIMUlye0vaR8CwbQnMCa5UZmWylTVTKvyt/xpW3IiAk8RIABwUSDgU6BQm63Zcb9ctD4QWLyfnNva55KZrW12j5xbMXnD77Gq1o4t19X7P5zZftg4AikXIACkfEBsL28CFmmotocmor0l7SnFe8m5PSU9O2+dbr6fuPEmvWvlopWKbaVMK7W0/9awDOgWge4KEAC668/qCKwXaHzKYIu+vTQxGQb2lMUvVhTtLNmzJRdlk6nxfH39r8z9RpH9RqZfyzX+eXyVhhf9XzZ7YtcI5EeAAJCfWdJJHgX2uGmGdnjkebJ4F2kyEOyiWDvLuZ1k9nQ5215y23SldbMHJd0uZ3dI0f/J7P/koj9J8f/KotvUc99vNHzAI13ZG4sigMCUAgSAKYk4AIGUCxx5YY/++KLt1Du+nSLbThO2nZy2k0XbyVkk2SzJzVzfReP9B7GTczNkWv9FSM7WyvTQ5D+bWyv36D/LPSzZPdLkI3TvkXru0eTv6Sfu1sS6e7Rs4P6Uy7A9BBDYjAABgMsDAQQQQACBAAUIAAEOnZYRQAABBBAgAHANIIAAAgggEKAAASDAodMyAggggAACBACuAQQQQAABBAIUIAAEOHRaRgABBBBAgADANYAAAggggECAAgSAAIdOywgggAACCBAAuAYQQAABBBAIUIAAEODQaRkBBBBAAAECANcAAggggAACAQoQAAIcOi0jgAACCCBAAOAaQAABBBBAIEABAkCAQ6dlBBBAAAEECABcAwgggAACCAQoQAAIcOi0jAACCCCAAAGAawABBBBAAIEABQgAAQ6dlhFAAAEEECAAcA0ggAACCCAQoAABIMCh0zICCCCAAAIEAK4BBBBAAAEEAhQgAAQ4dFpGAAEEEECAAMA1gAACCCCAQIACBIAAh07LCCCAAAIIEAC4BhBAAAEEEAhQgAAQ4NBpGQEEEEAAAQIA1wACCCCAAAIBChAAAhw6LSOAAAIIIEAA4BpAAAEEEEAgQAECQIBDp2UEEEAAAQQIAFwDCCCAAAIIBChAAAhw6LSMAAIIIIAAAYBrAAEEEEAAgQAFCAABDp2WEUAAAQQQIABwDSCAAAIIIBCgAAEgwKHTMgIIIIAAAgQArgEEEEAAAQQCFCAABDh0WkYAAQQQQIAAwDWAAAIIIIBAgAIEgACHTssIIIAAAggQALgGEEAAAQQQCFCAABDg0GkZAQQQQAABAgDXAAIIIIAAAgEKEAACHDotI4AAAgggQADgGkAAAQQQQCBAAQJAgEOnZQQQQAABBAgAXAMIIIAAAggEKEAACHDotIwAAggggAABgGsAAQQQQACBAAUIAAEOnZYRQAABBBAgAHANIIAAAgggEKAAASDAodMyAggggAACBACuAQQQQAABBAIUIAAEOHRaRgABBBBAgADANYAAAggggECAAgSAAIdOywgggAACCBAAuAYQQAABBBAIUIAAEODQaRkBBBBAAAECANcAAggggAACAQoQAAIcOi0jgAACCCBAAOAaQAABBBBAIEABAkCAQ6dlBBBAAAEECABcAwgggAACCAQoQAAIcOi0jAACCCCAAAGAawABBBBAAIEABQgAAQ6dlhFAAAEEECAAcA0ggAACCCAQoAABIMCh0zICCCCAAAIEAK4BBBBAAAEEAhQgAAQ4dFpGAAEEEECAAMA1gAACCCCAQIACBIAAh07LCCCAAAIIEAC4BhBAAAEEEAhQgAAQ4NBpGQEEEEAAAQIA1wACCCCAAAIBChAAAhw6LSOAAAIIIEAA4BpAAAEEEEAgQAECQIBDp2UEEEAAAQQIAFwDCCCAAAIIBChAAAhw6LSMAAIIIIAAAYBrAAEEEEAAgQAFCAABDp2WEUAAAQQQIABwDSCAAAIIIBCgAAEgwKHTMgIIIIAAAgQArgEEEEAAAQQCFPj/N7+h4hJPpPIAAAAASUVORK5CYII=',
      '#attributes' => ['class' => ['fontawesome__replay']],
    ];

    // Define color options for Font Awesome icons.
    $form['region']['color_options'] = [
      '#title' => $this->t('Color'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    // Initialize the $colors array and set default color values.
    $colors = [];
    $color = 'none';
    $color_custom = '#000000';

    // Determine the color format from the provided options.
    $color_format = isset($options['color']) ? $this->unitHelper->getColorFormat($options['color']) : '';

    // Populate the $colors array with color settings
    // from the form configuration.
    for ($i = 1; $i <= 6; $i++) {
      $colors['color-' . $i] = $formConfig->get('colors.color' . $i);
    }

    // Check if the color format is 'rgb'.
    if ($color_format == 'rgb') {
      // Search for the RGB color in the predefined colors array.
      $color = array_search($options['color'], $colors);

      // If the color is not found, set it as a custom color
      // and convert it to HEX format.
      if (!$color) {
        $color = 'color-custom';
        $result = preg_replace('/\s+/', '', $options['color']);
        $color_custom = $this->unitHelper->convertColor($result, 'rgb', 'hex');
      }
    }

    // Add radio buttons for color selection.
    $form['region']['color_options']['color'] = [
      '#title' => $this->t('Color'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconColorOptions(),
      '#default_value' => $color,
      '#attributes' => ['class' => ['fontawesome__color']],
    ];

    // Add color picker for custom color selection.
    $form['region']['color_options']['color_picker'] = [
      '#title' => $this->t('Color picker'),
      '#title_display' => 'invisible',
      '#type' => 'color',
      '#default_value' => $color_custom,
      '#attributes' => ['class' => ['fontawesome__color_picker']],
    ];

    // Font Awesome icon size options configuration.
    $form['region']['size_options'] = [
      '#title' => $this->t('Size'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    // Radio buttons for selecting icon size.
    $form['region']['size_options']['size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconSizeOptions(),
      '#default_value' => $options['size'] ?? $formConfig->get('size'),
      '#attributes' => ['class' => ['fontawesome__size']],
    ];

    // Font Awesome icon flip options configuration.
    $form['region']['flip_options'] = [
      '#title' => $this->t('Flip'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    // Radio buttons for selecting icon flip direction.
    $form['region']['flip_options']['flip'] = [
      '#title' => $this->t('Flip'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconFlipOptions(),
      '#default_value' => $options['flip'] ?? $formConfig->get('flip'),
      '#attributes' => ['class' => ['fontawesome__flip']],
    ];

    // Font Awesome icon rotate options configuration.
    $form['region']['rotate_options'] = [
      '#title' => $this->t('Rotate'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    // Radio buttons for selecting icon rotation.
    $form['region']['rotate_options']['rotate'] = [
      '#title' => $this->t('Rotate'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconRotateOptions(),
      '#default_value' => $options['rotate'] ?? $formConfig->get('rotate'),
      '#attributes' => ['class' => ['fontawesome__rotate']],
    ];
    // Radio buttons for selecting icon rotation.
    $form['region']['rotate_options']['rotate_angle'] = [
      '#title' => $this->t('Rotate angle'),
      '#title_display' => 'invisible',
      '#type' => 'number',
      '#min' => -360,
      '#max' => 360,
      '#default_value' => $options['rotate_angle'] ?? $formConfig->get('rotate_angle'),
      '#field_suffix' => ' <span class="fontawesome-unit-flag">deg</span>',
      '#attributes' => ['class' => ['fontawesome__rotate_angle']],
      '#states' => [
        'visible' => [
          ':input[name="rotate"]' => ['value' => 'rotate-by'],
        ],
      ],
    ];

    // The Font Awesome fixed width icon.
    $form['fixed_width'] = [
      '#title' => $this->t('Fixed Width'),
      '#description' => $this->t('Make your icon the same width so they can easily vertically align, like in a list or navigation menu.'),
      '#type' => 'checkbox',
      '#default_value' => $options['fixed_width'] ?? $formConfig->get('fixed_width'),
      '#attributes' => ['class' => ['fontawesome__fixed_width']],
    ];

    // Textarea for describing icon settings and usage on the website.
    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment'),
      '#default_value' => $comment ?? '',
      '#description' => $this->t('Describe these animation settings and their usage on your website.'),
      '#rows' => 2,
      '#weight' => 93,
    ];

    // Close button for the sidebar.
    $close_sidebar_translation = $this->t('Close sidebar panel');
    $form['region']['sidebar_close'] = [
      '#markup' => '<a href="#close-sidebar" class="toggle-sidebar__close trigger" role="button" title="' . $close_sidebar_translation . '"><span class="visually-hidden">' . $close_sidebar_translation . '</span></a>',
    ];

    // Container for sticky form actions.
    $form['sticky'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['gin-sticky', 'gin-sticky-form-actions']],
    ];

    // Checkbox for enabling or disabling the animation.
    $form['sticky']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $status ?? FALSE,
      '#weight' => 99,
    ];

    // Sidebar toggle button.
    $hide_panel = $this->t('Hide sidebar panel');
    $form['sticky']['sidebar_toggle'] = [
      '#markup' => '<a href="#toggle-sidebar" class="toggle-sidebar__trigger trigger" role="button" title="' . $hide_panel . '" aria-controls="fontawesome_sidebar"><span class="visually-hidden">' . $hide_panel . '</span></a>',
      '#weight' => '999',
    ];

    // Overlay for the sidebar.
    $form['sidebar_overlay'] = [
      '#markup' => '<div class="toggle-sidebar__overlay trigger"></div>',
    ];

    // Action buttons container and submit button for saving the form.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#submit' => [[$this, 'submitForm']],
    ];

    if ($fontawesome_id != 0) {
      // Link button for deleting the icon form.
      $form['actions']['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#url' => Url::fromRoute('fontawesome.delete', ['icon' => $fontawesome_id]),
        '#attributes' => [
          'class' => [
            'action-link',
            'action-link--danger',
            'action-link--icon-trash',
          ],
        ],
      ];

      // Add a submit handler to redirect to
      // the list after submitting the edit form.
      $form['actions']['submit']['#submit'] = ['::submitForm', '::overview'];
    }
    else {
      // Submit button for saving and redirecting to the list in the add form.
      $form['actions']['overview'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save and go to list'),
        '#submit' => array_merge($form['actions']['submit']['#submit'], ['::overview']),
        '#weight' => 20,
      ];
    }

    // Prepare icon and category data from Font Awesome metadata files
    // for Icon Picker/Browser and Attach plugin libraries.
    // Also, include styling and scripts for the icon builder form.
    $form = $this->attachedLibrary($form, $form_state);

    return $form;
  }

  /**
   * Helper function to get the default value for a form field.
   *
   * @param string $option_key
   *   The key of the option to retrieve.
   * @param string $default_key
   *   The default configuration key.
   * @param bool $convert
   *   Whether to convert the value based on units.
   * @param string $unit
   *   The unit to convert to.
   * @param array $options
   *   The options array containing current values.
   *
   * @return mixed
   *   The default value for the form field.
   */
  protected function getDefaultValue(string $option_key, string $default_key, bool $convert, string $unit, array $options) {
    // Check for conversion.
    if ($convert) {
      if (in_array($unit, ['px', 'em', 'rem'])) {
        return $this->unitHelper->convertSize($options[$option_key], $options['size_unit'], $unit);
      }
      elseif (in_array($unit, ['s', 'ms'])) {
        return $this->unitHelper->convertTime($options[$option_key], $options['time_unit'], $unit);
      }
    }

    // Return the default value if no conversion is needed.
    return $options[$option_key] ?? $this->formConfig->get($default_key);
  }

  /**
   * Retrieves the label flag for Font Awesome UI features.
   *
   * This function returns a label flag for the given type.
   * Supported types are: 'beta', 'experimental', and 'new'.
   *
   * @param string $type
   *   The type of the label flag.
   *
   * @return string
   *   The HTML string containing the label flag.
   */
  public function getLabelFlag(string $type) {

    // Switch statement to handle different types of flags.
    switch ($type) {
      case 'beta':
        $label = '<span class="fontawesome-beta-flag">Beta</span>';
        break;

      case 'experimental':
        $label = '<span class="fontawesome-experimental-flag">Experimental</span>';
        break;

      case 'new':
        $label = '<span class="fontawesome-new-flag">New</span>';
        break;

      default:
        // Initialize the label variable.
        $label = '';
        break;
    }

    // Return the label flag.
    return $label;
  }

  /**
   * Prepares icon and category data from Font Awesome metadata files.
   *
   * This function is used for the Icon Picker/Browser and Attach plugin.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function attachedLibrary(array $form, FormStateInterface $form_state) {
    $cid = 'fontawesome_ui:font_awesome_metadata';

    // Load the FontAwesome UI global configuration and icon form settings.
    $config = $this->config;
    $formConfig = $this->formConfig;

    // Get Font Awesome configuration from global settings.
    $version = $config->get('version');
    $library = $config->get('library');
    $existed = fontawesome_ui_check_installed();
    $methods = $existed ? $config->get('method') : 'cdn';

    // Ensures and verifies the library version and license.
    $license = $config->get('license');
    $reset = _fontawesome_ui_enforce_verify_version($existed, $library, $methods, $version, $license);
    if ($reset) {
      $version = $config->get('version');
      $license = $config->get('license');
    }

    $use_fontawesome = 'https://use.fontawesome.com/releases/';
    $library_path = $use_fontawesome . 'v' . $version . '/svgs/';
    if ($existed) {
      // Get the current request.
      $request = $this->requestStack->getCurrentRequest();
      $origin_url = $request->getSchemeAndHttpHost() . $request->getBaseUrl();
      $library_path = $origin_url . '/' . fontawesome_ui_find_library() . '/svgs/';
    }

    // Try to get data from cache.
    if ($cache = $this->cacheBackend->get($cid)) {
      $categorized_icons = $cache->data;
    }
    else {
      // Retrieve categorized icons from metadata manager.
      $categorized_icons = $this->metadataManager->getCategorizedIcons($version, $license);

      // Store the processed data in cache.
      if (is_array($categorized_icons) && count($categorized_icons)) {
        $this->cacheBackend->set($cid, $categorized_icons, CacheBackendInterface::CACHE_PERMANENT);
      }
    }

    // Attach Font Awesome settings to the form.
    $form['#attached']['drupalSettings']['fontAwesome']['installed_path'] = $library_path;
    $form['#attached']['drupalSettings']['fontAwesome']['icons_source'] = $categorized_icons;

    $units = $formConfig->get('units');
    $defaults = [
      'animation' => $formConfig->get('animate.animation'),
    ];
    for ($i = 1; $i <= 6; $i++) {
      $colors['color-' . $i] = $formConfig->get('colors.color' . $i);
    }
    $form['#attached']['drupalSettings']['fontAwesome']['version'] = $version;
    $form['#attached']['drupalSettings']['fontAwesome']['license'] = $license;
    $form['#attached']['drupalSettings']['fontAwesome']['units'] = $units;
    $form['#attached']['drupalSettings']['fontAwesome']['defaults'] = $defaults;
    $form['#attached']['drupalSettings']['fontAwesome']['colors'] = $colors;

    // Attach the appropriate plugin library.
    $plugin = $formConfig->get('icon_plugin');
    $form['#attached']['drupalSettings']['fontAwesome']['icon_plugin'] = $plugin;
    if ($plugin == 'browser') {
      $form['#attached']['drupalSettings']['fontAwesome']['auto_open'] = $formConfig->get('icon_browser.auto_open');
      $form['#attached']['drupalSettings']['fontAwesome']['click_insert'] = $formConfig->get('icon_browser.click_insert');
      $form['#attached']['drupalSettings']['fontAwesome']['show_counts'] = $formConfig->get('icon_browser.show_counts');
      $form['#attached']['drupalSettings']['fontAwesome']['items_per_page'] = $formConfig->get('icon_browser.items_per_page');
      $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.icon.browser';
    }
    else {
      $form['#attached']['drupalSettings']['fontAwesome']['has_search'] = $formConfig->get('icon_picker.has_search');
      $form['#attached']['drupalSettings']['fontAwesome']['empty_icon'] = $formConfig->get('icon_picker.empty_icon');
      $form['#attached']['drupalSettings']['fontAwesome']['auto_close'] = $formConfig->get('icon_picker.auto_close');
      $form['#attached']['drupalSettings']['fontAwesome']['icons_per_page'] = $formConfig->get('icon_picker.icons_per_page');
      $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.icon.picker';
    }

    // Attach the Font Awesome UI icon builder library.
    $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.icon.styling';

    // Use 'webfonts' library for displaying a list of all icons.
    $library = 'webfonts';
    $cdn = $methods == 'cdn' ? 'cdn.' : '';

    // Attach the necessary libraries based on configuration.
    if ($config->get('file_types.all')) {
      // Attach the main Font Awesome library.
      $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library;

      // Handle additional Pro styles for version 6 and above.
      if ($license === 'pro') {
        $styles = [];

        // Include "Sharp" styles for version 6 and above.
        if (intval($version) >= 6) {
          $styles = [
            'sharp_solid',
            'sharp_regular',
            'sharp_light',
            'sharp_thin',
          ];
        }

        // Include "Sharp Duotone" style for version 6.6.0.
        if (version_compare($version, '6.6.0', '==')) {
          $styles[] = 'sharp_duotone';
        }

        // Include "Duotone" and additional "Sharp Duotone" styles for
        // version 6.7.0 and above.
        if (version_compare($version, '6.7.0', '>=')) {
          $styles = array_merge($styles, [
            'duotone_solid',
            'duotone_regular',
            'duotone_light',
            'duotone_thin',
            'sharp_duotone_solid',
            'sharp_duotone_regular',
            'sharp_duotone_light',
            'sharp_duotone_thin',
          ]);
        }

        // Attach all determined styles dynamically.
        foreach ($styles as $style) {
          $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library . '.' . $style;
        }
      }
    }
    else {
      // Attach libraries piecemeal based on selected styles.
      $styles = fontawesome_ui_icon_styles($version);
      foreach ($styles[$license] as $icon) {
        $icon_type = "file_types.$icon";
        $icon_load[$icon_type] = is_null($config->get($icon_type)) === TRUE ? TRUE : $config->get($icon_type);
        if ($icon_load[$icon_type]) {
          $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library . '.' . $icon;
        }
      }

      if ($library == 'webfonts') {
        // Attach the shim file if needed.
        if ($config->get('file_types.v5_font_face') && intval($version) > 5) {
          $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library . '.v5_font_face';
        }
        if ($config->get('file_types.v4_font_face')) {
          $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library . '.v4_font_face';
        }
      }
    }

    // Attach the v4 shim file if needed.
    if ($config->get('file_types.v4_shim')) {
      $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.' . $cdn . $library . '.v4_shim';
    }

    // Attach extended animation library.
    $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.extended.animation';

    return $form;
  }

  /**
   * Submit handler for removing animate.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function remove(array &$form, FormStateInterface $form_state) {
    $fontawesome_id = $form_state->getValue('fontawesome_id');
    $form_state->setRedirect('fontawesome.delete', ['icon' => $fontawesome_id]);
  }

  /**
   * Form submission handler for the 'overview' action.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function overview(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('fontawesome.admin');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fontawesome_id = $form_state->getValue('fontawesome_id');
    $is_new = $fontawesome_id == 0;
    $selector = trim($form_state->getValue('selector'));

    // Check if the form is for a new Font Awesome icon.
    if ($is_new) {
      // Validate if the selector already exists.
      if ($this->iconManager->isFontAwesome($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector already exists.'));
      }
    }
    else {
      // Fetch the existing Font Awesome icon by ID.
      if ($this->iconManager->findById($fontawesome_id)) {
        $animate = $this->iconManager->findById($fontawesome_id);

        // Validate if the selector has been changed and already exists.
        if ($selector != $animate['selector'] && $this->iconManager->isFontAwesome($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get all form field values.
    $values = $form_state->getValues();

    // Prepare main field data.
    $fontawesome_id = $values['fontawesome_id'];
    $label = trim($values['label']);
    $selector = trim($values['selector']);
    $comment = trim($values['comment']);
    $status = $values['status'];

    // Provide a label from selector if was empty.
    if (empty($label)) {
      $label = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    }

    $color = '';
    if ($values['color'] === 'color-custom') {
      $color = ColorUtility::hexToRgb($values['color_picker']);
      $color = sprintf("rgb(%d, %d, %d)", $color['red'], $color['green'], $color['blue']);
    }
    else {
      for ($i = 1; $i <= 6; $i++) {
        $colors['color-' . $i] = $this->formConfig->get('colors.color' . $i);
      }

      if (isset($colors[$values['color']])) {
        $color = $colors[$values['color']];
      }
    }

    // Set variables from icon form utility fields.
    $variables = [
      'classes' => $values['classes'],
      'icon' => $values['icon'],
      'position' => $values['position'],
      'fixed_width' => $values['fixed_width'],
      'animation' => $values['animation'],
      'delay' => $values['delay'],
      'duration' => $values['duration'],
      'direction' => $values['direction'],
      'iteration_count' => $values['iteration_count'],
      'timing' => $values['timing'],
      'scale' => $values['scale'],
      'opacity' => $values['opacity'],
      'rebound' => $values['rebound'],
      'height' => $values['height'],
      'start_scale_x' => $values['start_scale_x'],
      'start_scale_y' => $values['start_scale_y'],
      'jump_scale_x' => $values['jump_scale_x'],
      'jump_scale_y' => $values['jump_scale_y'],
      'land_scale_x' => $values['land_scale_x'],
      'land_scale_y' => $values['land_scale_y'],
      'flip_x' => $values['flip_x'],
      'flip_y' => $values['flip_y'],
      'flip_z' => $values['flip_z'],
      'flip_angle' => $values['flip_angle'],
      'spin_reverse' => $values['spin_reverse'],
      'border' => $values['border'],
      'border_color' => $values['border_color'],
      'border_width' => $values['border_width'],
      'border_style' => $values['border_style'],
      'border_radius' => $values['border_radius'],
      'border_padding' => $values['border_padding'],
      'pull' => $values['pull'],
      'pull_margin' => $values['pull_margin'],
      'color' => $color,
      'color_picker' => $values['color_picker'],
      'size' => $values['size'],
      'flip' => $values['flip'],
      'rotate' => $values['rotate'],
      'rotate_angle' => $values['rotate_angle'],
      'size_unit' => $values['size_unit'],
      'time_unit' => $values['time_unit'],
    ];

    // Get variables from other module.
    if (isset($values['variables']) && count($values['variables'])) {
      $variables = $variables + $values['variables'];
    }
    if (count($this->variables)) {
      $variables = array_merge($variables, $this->variables);
    }

    // Serialize options variables.
    $options = serialize($variables);

    // The Unix timestamp when the animate was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Save animate.
    $this->iconManager->addFontAwesome($fontawesome_id, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been added.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();
  }

}
