<?php

namespace Drupal\fontawesome_ui\Form;

use Drupal\Component\Utility\Color as ColorUtility;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fontawesome_ui\FontAwesomeIconHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures FontAwesome UI Builder form settings.
 */
class FontAwesomeIconFormSettings extends ConfigFormBase {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Font Awesome icon helper service.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeIconHelperInterface
   */
  protected $iconHelper;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend interface.
   * @param \Drupal\fontawesome_ui\FontAwesomeIconHelperInterface $icon_helper
   *   The Font Awesome icon helper service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ThemeHandlerInterface $theme_handler, CacheBackendInterface $cache_backend, FontAwesomeIconHelperInterface $icon_helper) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->themeHandler = $theme_handler;
    $this->cacheBackend = $cache_backend;
    $this->iconHelper = $icon_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('theme_handler'),
      $container->get('cache.fontawesome'),
      $container->get('fontawesome.icon_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_builder_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fontawesome_ui.icon_form',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // The FontAwesome UI icon form config settings.
    $formConfig = $this->config('fontawesome_ui.icon_form');

    // Store Font Awesome size unit.
    $size_unit = $formConfig->get('units.size_unit');
    $size_unit_label = ' <span class="fontawesome-unit-flag">' . $size_unit . '</span>';
    $form['size_unit'] = [
      '#type' => 'value',
      '#value' => $size_unit,
    ];

    // Store Font Awesome time unit.
    $time_unit = $formConfig->get('units.time_unit');
    $time_unit_label = ' <span class="fontawesome-unit-flag">' . $time_unit . '</span>';
    $form['time_unit'] = [
      '#type' => 'value',
      '#value' => $time_unit,
    ];

    $form['icon'] = [
      '#title' => $this->t('Icon'),
      '#description' => $this->t('Default icon field place holder.'),
      '#type' => 'textfield',
      '#default_value' => $formConfig->get('icon') ?? 'font-awesome',
    ];

    // Load method library from CDN or Locally.
    $form['fixed_width'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fixed width'),
      '#default_value' => $formConfig->get('fixed_width') ?? TRUE,
    ];

    // Load method library from CDN or Locally.
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => $this->iconHelper->getIconPositionOptions(),
      '#default_value' => $formConfig->get('position') ?? 'prepend',
    ];

    $form['animate'] = [
      '#title' => $this->t('Animate'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['animate']['animation'] = [
      '#type' => 'select',
      '#title' => $this->t('Animation'),
      '#options' => $this->iconHelper->getIconAnimations(),
      '#default_value' => $formConfig->get('animate.animation') ?? '',
    ];
    $form['animate']['custom'] = [
      '#title' => $this->t('Customization'),
      '#description' => $this->t('Add motion to your icons.'),
      '#type' => 'details',
      '#open' => FALSE,
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['delay'] = [
      '#type'          => 'number',
      '#min'           => 0,
      '#title'         => $this->t('Delay time'),
      '#default_value' => $formConfig->get('animate.delay') ?? 0,
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-delay-time']],
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['duration'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Duration'),
      '#default_value' => $formConfig->get('animate.duration'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-duration']],
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['direction'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Direction'),
      '#default_value' => $formConfig->get('animate.direction'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-direction']],
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['iteration_count'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Iteration count'),
      '#default_value' => $formConfig->get('animate.iteration_count'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-direction']],
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['timing'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Timing'),
      '#default_value' => $formConfig->get('animate.timing'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-timing']],
      '#states'        => [
        'invisible' => [
          ':input[name="animation"]' => ['value' => 'none'],
        ],
      ],
    ];
    $form['animate']['custom']['scale'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Scale'),
      '#default_value' => $formConfig->get('animate.scale'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-timing']],
      '#states'        => [
        'visible' => [
          [
            ':input[name="animation"]' => ['value' => 'fa-beat'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat-fade'],
          ],
        ],
      ],
    ];
    $form['animate']['custom']['opacity'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Opacity'),
      '#default_value' => $formConfig->get('animate.opacity'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-timing']],
      '#states'        => [
        'visible' => [
          [
            ':input[name="animation"]' => ['value' => 'fa-fade'],
          ],
          [
            ':input[name="animation"]' => ['value' => 'fa-beat-fade'],
          ],
        ],
      ],
    ];

    $form['animate']['bounce_wrapper'] = [
      '#title'         => $this->t('Bounce'),
      '#type'       => 'fieldset',
      '#attributes' => [
        'class' => ['fontawesome-wrapper', 'container-inline', 'form-item'],
      ],
      '#states'        => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-bounce'],
        ],
      ],
    ];
    $form['animate']['bounce_wrapper']['rebound'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rebound'),
      '#default_value' => $formConfig->get('animate.rebound'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['height'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Max height'),
      '#default_value' => $formConfig->get('animate.rebound'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['start_scale_x'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Start scale x'),
      '#default_value' => $formConfig->get('animate.start_scale_x'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['start_scale_y'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Start scale y'),
      '#default_value' => $formConfig->get('animate.start_scale_y'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['jump_scale_x'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Jump scale x'),
      '#default_value' => $formConfig->get('animate.jump_scale_x'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['jump_scale_y'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Jump scale y'),
      '#default_value' => $formConfig->get('animate.jump_scale_y'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['land_scale_x'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Land scale x'),
      '#default_value' => $formConfig->get('animate.land_scale_x'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['bounce_wrapper']['land_scale_y'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Land scale y'),
      '#default_value' => $formConfig->get('animate.land_scale_y'),
      '#field_suffix'  => $time_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];

    $form['animate']['flip_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['fontawesome-wrapper', 'container-inline', 'form-item'],
      ],
      '#states'        => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-flip'],
        ],
      ],
    ];
    $form['animate']['flip_wrapper']['flip_x'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Flip-X'),
      '#default_value' => $formConfig->get('animate.flip_x'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['flip_wrapper']['flip_y'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Flip-Y'),
      '#default_value' => $formConfig->get('animate.flip_y'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['flip_wrapper']['flip_z'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Flip-Z'),
      '#default_value' => $formConfig->get('animate.flip_z'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['animate']['flip_wrapper']['flip_angle'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Flip angle'),
      '#default_value' => $formConfig->get('animate.flip_angle'),
      '#field_suffix'  => 'deg',
      '#attributes'    => ['class' => ['animate-rebound']],
    ];

    $form['animate']['spin_reverse'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Spin reverse'),
      '#default_value' => $formConfig->get('animate.spin_reverse'),
      '#attributes'    => ['class' => ['animate-rebound']],
      '#states'        => [
        'visible' => [
          ':input[name="animation"]' => ['value' => 'fa-spin-pulse'],
        ],
      ],
    ];

    $form['border_pull'] = [
      '#title' => $this->t('Border + Pull'),
      '#type'  => 'details',
      '#open'  => TRUE,
    ];
    $form['border_pull']['border'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Border'),
      '#default_value' => $formConfig->get('border_pull.border'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['border_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['fontawesome-wrapper', 'container-inline', 'form-item'],
      ],
      '#states'        => [
        'visible' => [
          ':input[name="border"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['border_pull']['border_wrapper']['border_color'] = [
      '#type'          => 'color',
      '#title'         => $this->t('Border color'),
      '#default_value' => $formConfig->get('border_pull.border_color'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['border_wrapper']['border_width'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Border width'),
      '#default_value' => $formConfig->get('border_pull.border_width'),
      '#field_suffix'  => $size_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['border_wrapper']['border_style'] = [
      '#title'         => $this->t('Border style'),
      '#type'          => 'select',
      '#options' => $this->iconHelper->getIconBorderStyleOptions(),
      '#default_value' => $formConfig->get('border_pull.border_style'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['border_wrapper']['border_radius'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Border radius'),
      '#default_value' => $formConfig->get('border_pull.border_radius'),
      '#field_suffix'  => $size_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['border_wrapper']['border_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Border padding'),
      '#default_value' => $formConfig->get('border_pull.border_padding'),
      '#field_suffix'  => $size_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['pull'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Pull'),
      '#options' => $this->iconHelper->getIconPullOptions(),
      '#default_value' => $formConfig->get('border_pull.pull'),
      '#attributes'    => ['class' => ['animate-rebound']],
    ];
    $form['border_pull']['pull_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Pull margin'),
      '#default_value' => $formConfig->get('border_pull.pull_margin'),
      '#field_suffix'  => $size_unit_label,
      '#attributes'    => ['class' => ['animate-rebound']],
      '#states'        => [
        'invisible' => [
          ':input[name="pull"]' => ['value' => 'none'],
        ],
      ],
    ];

    $form['colors'] = [
      '#title' => $this->t('Colors'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['colors']['color1'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 1'),
      '#default_value' => $formConfig->get('colors.color1') ? ColorUtility::rgbToHex($formConfig->get('colors.color1')) : '#000000',
    ];
    $form['colors']['color2'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 2'),
      '#default_value' => $formConfig->get('colors.color2') ? ColorUtility::rgbToHex($formConfig->get('colors.color2')) : '#000000',
    ];
    $form['colors']['color3'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 3'),
      '#default_value' => $formConfig->get('colors.color3') ? ColorUtility::rgbToHex($formConfig->get('colors.color3')) : '#000000',
    ];
    $form['colors']['color4'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 4'),
      '#default_value' => $formConfig->get('colors.color4') ? ColorUtility::rgbToHex($formConfig->get('colors.color4')) : '#000000',
    ];
    $form['colors']['color5'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 5'),
      '#default_value' => $formConfig->get('colors.color5') ? ColorUtility::rgbToHex($formConfig->get('colors.color5')) : '#000000',
    ];
    $form['colors']['color6'] = [
      '#type' => 'color',
      '#title' => $this->t('Color 6'),
      '#default_value' => $formConfig->get('colors.color6') ? ColorUtility::rgbToHex($formConfig->get('colors.color6')) : '#000000',
    ];

    $form['sizing'] = [
      '#title' => $this->t('Sizing'),
      '#type'  => 'details',
      '#open'  => TRUE,
    ];
    $form['sizing']['sizing_options'] = [
      '#type' => 'select',
      '#title' => $this->t('Sizing options'),
      '#options' => [
        'relative' => $this->t('Relative sizing'),
        'literal' => $this->t('Literal sizing'),
      ],
      '#default_value' => $formConfig->get('sizing_options') ?? 'relative',
    ];
    $form['sizing']['size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconSizeOptions(),
      '#default_value' => $options['size'] ?? $formConfig->get('size'),
      '#required' => TRUE,
    ];

    // The Font Awesome icon flip options.
    $form['flip_options'] = [
      '#title' => $this->t('Flip'),
      '#type'  => 'details',
      '#open'  => TRUE,
    ];
    $form['flip_options']['flip'] = [
      '#title' => $this->t('Flip'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconFlipOptions(),
      '#default_value' => $options['flip'] ?? $formConfig->get('flip'),
    ];

    // The Font Awesome icon rotate options.
    $form['rotate_options'] = [
      '#title' => $this->t('Rotate'),
      '#type'  => 'details',
      '#open'  => TRUE,
    ];

    $form['rotate_options']['rotate'] = [
      '#title' => $this->t('Rotate'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $this->iconHelper->getIconRotateOptions(),
      '#default_value' => $options['rotate'] ?? $formConfig->get('rotate'),
    ];

    $form['units'] = [
      '#title' => $this->t('Units'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['units']['size_unit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Size unit'),
      '#options' => [
        'px' => $this->t('px'),
        'em' => $this->t('em'),
        'rem' => $this->t('rem'),
      ],
      '#default_value' => $formConfig->get('units.size_unit') ?? 'px',
      '#required' => TRUE,
    ];
    $form['units']['time_unit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Time unit'),
      '#options' => [
        's' => $this->t('s'),
        'ms' => $this->t('ms'),
      ],
      '#default_value' => $formConfig->get('units.time_unit') ?? 'ms',
      '#required' => TRUE,
    ];

    $form['widget'] = [
      '#title' => $this->t('Plugin'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['widget']['icon_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Display icons'),
      '#options' => [
        'picker' => $this->t('Icon picker'),
        'browser' => $this->t('Icon browser'),
      ],
      '#default_value' => $formConfig->get('icon_plugin') ?? 'browser',
    ];

    $form['icon_picker'] = [
      '#title' => $this->t('Icon picker'),
      '#type' => 'details',
      '#open' => TRUE,
      '#states'        => [
        'visible' => [
          'select[name="icon_plugin"]' => ['value' => 'picker'],
        ],
      ],
    ];
    $form['icon_picker']['has_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable search'),
      '#default_value' => $formConfig->get('icon_picker.has_search') ?? TRUE,
    ];
    $form['icon_picker']['empty_icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show empty icon'),
      '#default_value' => $formConfig->get('icon_picker.empty_icon') ?? TRUE,
    ];
    $form['icon_picker']['auto_close'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Close automatically when clicked outside'),
      '#default_value' => $formConfig->get('icon_picker.auto_close') ?? TRUE,
    ];
    $form['icon_picker']['icons_per_page'] = [
      '#type'          => 'number',
      '#min'           => 18,
      '#title'         => $this->t('Icons per page'),
      '#default_value' => $formConfig->get('icon_picker.icons_per_page') ?? 18,
    ];

    $form['icon_browser'] = [
      '#title' => $this->t('Icon browser'),
      '#type' => 'details',
      '#open' => TRUE,
      '#states'        => [
        'visible' => [
          'select[name="icon_plugin"]' => ['value' => 'browser'],
        ],
      ],
    ];
    $form['icon_browser']['auto_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto open icon browser'),
      '#default_value' => $formConfig->get('icon_browser.auto_open') ?? FALSE,
    ];
    $form['icon_browser']['click_insert'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert icon on click'),
      '#default_value' => $formConfig->get('icon_browser.click_insert') ?? TRUE,
    ];
    $form['icon_browser']['show_counts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show sidebar icon counts'),
      '#default_value' => $formConfig->get('icon_browser.show_counts') ?? TRUE,
    ];
    $form['icon_browser']['items_per_page'] = [
      '#type'          => 'number',
      '#min'           => 30,
      '#title'         => $this->t('Icons per page'),
      '#default_value' => $formConfig->get('icon_browser.items_per_page') ?? 36,
    ];

    // The FontAwesome UI settings library.
    $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.settings';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated Font Awesome settings.
    $config = $this->config('fontawesome_ui.icon_form');
    $config
      ->set('icon', (string) $values['icon'])
      ->set('fixed_width', isset($values['fixed_width']) && $values['fixed_width'] !== 0 ?? FALSE)
      ->set('position', $values['position'])
      ->set('animate.animation', (string) $values['animation'])
      ->set('animate.delay', $values['delay'])
      ->set('animate.direction', $values['direction'])
      ->set('animate.duration', $values['duration'])
      ->set('animate.iteration_count', $values['iteration_count'])
      ->set('animate.timing', $values['timing'])
      ->set('animate.scale', $values['scale'])
      ->set('animate.opacity', $values['opacity'])
      ->set('animate.rebound', $values['rebound'])
      ->set('animate.height', $values['height'])
      ->set('animate.start_scale_x', $values['start_scale_x'])
      ->set('animate.start_scale_y', $values['start_scale_y'])
      ->set('animate.jump_scale_x', $values['jump_scale_x'])
      ->set('animate.jump_scale_y', $values['jump_scale_y'])
      ->set('animate.land_scale_x', $values['land_scale_x'])
      ->set('animate.land_scale_y', $values['land_scale_y'])
      ->set('animate.flip_x', $values['flip_x'])
      ->set('animate.flip_y', $values['flip_y'])
      ->set('animate.flip_z', $values['flip_z'])
      ->set('animate.flip_angle', $values['flip_angle'])
      ->set('border_pull.border', $values['border'])
      ->set('border_pull.border_color', $values['border_color'])
      ->set('border_pull.border_padding', $values['border_padding'])
      ->set('border_pull.border_radius', $values['border_radius'])
      ->set('border_pull.border_style', $values['border_style'])
      ->set('border_pull.border_width', $values['border_width'])
      ->set('border_pull.pull', $values['pull'])
      ->set('border_pull.pull_margin', $values['pull_margin'])
      ->set('build.size', $values['size'])
      ->set('build.flip', $values['flip'])
      ->set('build.rotate', $values['rotate'])
      ->set('sizing_options', $values['sizing_options'])
      ->set('size_unit', $values['size_unit'])
      ->set('time_unit', $values['time_unit'])
      ->set('icon_plugin', $values['icon_plugin'])
      ->set('icon_picker.has_search', $values['has_search'])
      ->set('icon_picker.empty_icon', $values['empty_icon'])
      ->set('icon_picker.auto_close', $values['auto_close'])
      ->set('icon_picker.icons_per_page', $values['icons_per_page'])
      ->set('icon_browser.auto_open', $values['auto_open'])
      ->set('icon_browser.click_insert', $values['click_insert'])
      ->set('icon_browser.show_counts', $values['show_counts'])
      ->set('icon_browser.items_per_page', $values['items_per_page']);

    for ($i = 1; $i <= 6; $i++) {
      $color = ColorUtility::hexToRgb($values['color' . $i]);
      $rgbColor = sprintf("rgb(%d, %d, %d)", $color['red'], $color['green'], $color['blue']);
      $config->set('colors.color' . $i, $rgbColor);
    }

    $config->save();

    // Flush caches so the updated config can be checked.
    $cid = 'fontawesome_ui:font_awesome_metadata';
    $this->cacheBackend->delete($cid);
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
