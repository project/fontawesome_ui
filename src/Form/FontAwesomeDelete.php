<?php

namespace Drupal\fontawesome_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\fontawesome_ui\FontAwesomeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove Font Awesome icon.
 *
 * @internal
 */
class FontAwesomeDelete extends ConfirmFormBase {

  /**
   * The Font Awesome icon.
   *
   * @var int
   */
  protected $icon;

  /**
   * The Font Awesome icon manager.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeManagerInterface
   */
  protected $iconManager;

  /**
   * Constructs a new FontAwesomeDelete object.
   *
   * @param \Drupal\fontawesome_ui\FontAwesomeManagerInterface $icon_manager
   *   The Font Awesome icon manager.
   */
  public function __construct(FontAwesomeManagerInterface $icon_manager) {
    $this->iconManager = $icon_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fontawesome.icon_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %selector from Font Awesome icon selectors?', ['%selector' => $this->icon['selector']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int|string $icon
   *   The Font Awesome record ID to remove icon.
   */
  public function buildForm(array $form, FormStateInterface $form_state, int|string $icon = 0) {
    if (!$this->icon = $this->iconManager->findById($icon)) {
      throw new NotFoundHttpException();
    }
    $form['fontawesome_id'] = [
      '#type'  => 'value',
      '#value' => $icon,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $icon_id = $form_state->getValue('fontawesome_id');
    $this->iconManager->removefontAwesome($icon_id);
    $this->logger('user')
      ->notice('Deleted %selector', ['%selector' => $this->icon['selector']]);
    $this->messenger()
      ->addStatus($this->t('The Font Awesome icon selector %selector was deleted.', ['%selector' => $this->icon['selector']]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('fontawesome.admin');
  }

}
