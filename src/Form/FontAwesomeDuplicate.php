<?php

namespace Drupal\fontawesome_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fontawesome_ui\FontAwesomeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to duplicate Font Awesome icon.
 *
 * @internal
 */
class FontAwesomeDuplicate extends FormBase {

  /**
   * The Font Awesome icon.
   *
   * @var int
   */
  protected $icon;

  /**
   * The Font Awesome icon manager.
   *
   * @var \Drupal\fontawesome_ui\FontAwesomeManagerInterface
   */
  protected $iconManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new FontAwesomeDuplicate object.
   *
   * @param \Drupal\fontawesome_ui\FontAwesomeManagerInterface $icon_manager
   *   The Font Awesome icon manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(FontAwesomeManagerInterface $icon_manager, TimeInterface $time) {
    $this->iconManager = $icon_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fontawesome.icon_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_duplicate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $icon
   *   The Font Awesome record ID to remove icon.
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $icon = 0) {
    $form['fontawesome_id'] = [
      '#type'  => 'value',
      '#value' => $icon,
    ];

    // New selector to duplicate icon.
    $form['selector'] = [
      '#title'         => $this->t('Selector'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => '',
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Be sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#button_type' => 'primary',
      '#value'       => $this->t('Duplicate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $faid     = $form_state->getValue('fontawesome_id');
    $is_new   = $faid == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->iconManager->isfontAwesome($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector is already exists.'));
      }
    }
    else {
      if ($this->iconManager->findById($faid)) {
        $icon = $this->iconManager->findById($faid);

        if ($selector != $icon['selector'] && $this->iconManager->isfontAwesome($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * Form submission handler for the 'duplicate' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $faid     = $values['fontawesome_id'];
    $selector = trim($values['selector']);
    $label    = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    $status   = 1;

    $icon    = $this->iconManager->findById($faid);
    $comment = $icon['comment'];
    $options = $icon['options'];

    // The Unix timestamp when the Font Awesome icon was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Save Font Awesome icon.
    $new_faid = $this->iconManager->addfontAwesome(0, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been duplicated.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    // Redirect to duplicated FontAwesome icon edit form.
    $form_state->setRedirect('fontawesome.edit', ['icon' => $new_faid]);
  }

}
