<?php

namespace Drupal\fontawesome_ui\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\fontawesome_ui\Constants\FontAwesomeConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures FontAwesome UI settings.
 */
class FontAwesomeSettings extends ConfigFormBase {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ThemeHandlerInterface $theme_handler, CacheBackendInterface $cache_backend) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->themeHandler = $theme_handler;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('theme_handler'),
      $container->get('cache.fontawesome'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fontawesome_ui.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // The Font Awesome latest version constant.
    $latest_version = FontAwesomeConstants::LATEST_VERSION;

    // The latest Font Awesome download link.
    $download_link = FontAwesomeConstants::DOWNLOAD_LINK;

    // Flags for Font Awesome version.
    $free_label = ' <span class="fontawesome-free-flag">Free</span>';
    $pro_label = ' <span class="fontawesome-pro-flag">Pro</span>';
    $pro_only_label = ' <span class="fontawesome-free-flag">Pro Only</span>';

    // The FontAwesome UI config settings.
    $config = $this->config('fontawesome_ui.settings');

    // Get Font Awesome library type.
    $library = $config->get('library');

    // Handle load Font Awesome library.
    $form['load'] = [
      '#title' => $this->t('Load'),
      '#description' => $this->t("If enabled, this module will attempt to load the Font Awesome for your site. To prevent loading it twice, leave this option disabled if you're including the assets manually or through another module or theme."),
      '#type' => 'checkbox',
      '#default_value' => $config->get('load'),
      '#weight' => -90,
    ];

    // Font Awesome Stable version releases.
    $form['options_wrapper'] = [
      '#type' => 'container',
      '#weight' => -60,
      '#attributes' => [
        'class' => [
          'fontawesome-options-wrapper',
          'form-item',
        ],
      ],
    ];

    // Show warning missing library and lock on cdn method.
    $assets = fontawesome_ui_check_installed();
    $method = $config->get('method');
    $method_lock_change = FALSE;
    if ($assets === FALSE) {
      $method = 'cdn';
      $method_lock_change = TRUE;
      $method_warning = $this->t('You cannot set local due to the Font Awesome library is missing. Please <a href=":downloadLink" rel="external" target="_blank">Download</a> and extract to "/libraries/fontawesome" directory.', [
        ':downloadLink' => $download_link,
      ]);

      // Display warning message off.
      $form['hide'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide warning'),
        '#default_value' => $config->get('hide') ?? FALSE,
        '#description' => $this->t("If you want to use the CDN without installing the local library, you can turn off the warning."),
      ];

      $form['method_warning'] = [
        '#type' => 'container',
        '#markup' => '<div class="library-status-report">' . $method_warning . '</div>',
        '#prefix' => '<div id="method-warning">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [
            ':input[name="load"]' => ['checked' => TRUE],
          ],
          'invisible' => [
            ':input[name="hide"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $version = !empty($form_state->getValue('version')) ? $form_state->getValue('version') : (!empty($config->get('version')) ? $config->get('version') : $latest_version);
    }
    else {
      $version = ($method == 'cdn') ? (!empty($form_state->getValue('version')) ? $form_state->getValue('version') : $config->get('version')) : fontawesome_ui_detect_version($library, $assets);
    }

    // Font Awesome Stable version releases.
    $form['method_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome-method-wrapper',
          'container-inline',
          'form-item',
        ],
      ],
    ];

    // Load method library from CDN or Locally.
    $form['method_wrapper']['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method'),
      '#options' => [
        'local' => $this->t('Local'),
        'cdn' => $this->t('CDN'),
      ],
      '#default_value' => $method,
      '#disabled' => $method_lock_change,
      '#prefix' => '<div id="method-options">',
      '#suffix' => '</div>',
    ];
    // Load method library from CDN or Locally.
    $form['method_wrapper']['cdn'] = [
      '#type' => 'select',
      '#title' => $this->t('Provider'),
      '#options' => [
        'fontawesome' => $this->t('Font Awesome'),
        'cdnjs' => $this->t('cdnjs'),
        'jsdelivr' => $this->t('jsDelivr'),
      ],
      '#default_value' => $config->get('cdn'),
      '#prefix' => '<div id="cdn-options">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          'select[name="method"]' => ['value' => 'cdn'],
        ],
      ],
    ];
    $form['method_wrapper']['method_description'] = [
      '#type' => 'item',
      '#description' => $this->t('These settings control how the library is loaded. You can choose to load it from a "CDN" (external source) or from the "Local" and "Composer" (internal library) installations.'),
    ];
    $form['method_wrapper']['integrity'] = [
      '#title' => $this->t('Integrity Value'),
      '#description' => $this->t('Enter an optional integrity value to verify the CDN. Font Awesome will provide this value along with the CDN URL. Using this integrity value ensures that the browser checks the integrity of the CDN content, preventing any unauthorized or unexpected modifications.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('integrity'),
      '#states' => [
        'enabled' => [
          'select[name="cdn"]' => ['value' => 'fontawesome'],
        ],
        'visible' => [
          ':input[name="method"]' => ['value' => 'cdn'],
        ],
      ],
    ];
    $form['method_wrapper']['cdn_notice'] = [
      '#type' => 'item',
      '#description' => $this->t("<strong>Notice:</strong> Use reliable CDN providers to integrate Font Awesome into your project for fast and efficient delivery: cdnjs supports all versions including older ones, jsDelivr is known for its performance, and the Font Awesome CDN ensures the latest version and auto-updates."),
      '#states' => [
        'visible' => [
          'select[name="method"]' => ['value' => 'cdn'],
        ],
        'enabled' => [
          ':input[name="method"]' => ['value' => 'cdn'],
        ],
      ],
    ];

    // Font Awesome Stable version releases.
    $form['version_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome-version-wrapper',
          'container-inline',
          'form-item',
        ],
      ],
      '#states' => [
        'disabled' => [
          'select[name="method"]' => ['value' => 'local'],
        ],
      ],
    ];

    // Get Font Awesome Stable versions.
    $versions = fontawesome_ui_version_options();
    $in_versions = in_array($version, _fontawesome_ui_array_flatten($versions));
    $form['version_wrapper']['version'] = [
      '#type' => 'select',
      '#options' => $versions,
      '#title' => $this->t('Version'),
      '#default_value' => $in_versions ? $version : 'other',
      '#needs_validation' => FALSE,
      '#validated' => TRUE,
      '#prefix' => '<div id="version-options">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => '::filesAjaxCallback',
        'wrapper' => 'edit-file-types',
        'method' => 'replace',
        'event' => 'change',
        'effect' => 'fade',
        'progress' => ['type' => 'throbber'],
      ],
    ];

    // Get Font Awesome latest stable releases.
    $releases = fontawesome_ui_version_releases();
    $in_releases = in_array($version, _fontawesome_ui_array_flatten($releases));
    $form['version_wrapper']['release'] = [
      '#type' => 'select',
      '#options' => $in_releases ? $releases : $releases + ['' => $version . ' (' . $this->t('Not stable') . ')'],
      '#default_value' => $in_releases ? $version : '',
      '#title' => $this->t('Stable releases'),
      '#attributes' => ['class' => ['bootstrap-version-release']],
      '#needs_validation' => FALSE,
      '#validated' => TRUE,
      '#prefix' => '<div id="release-options">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => '::filesAjaxCallback',
        'wrapper' => 'edit-file-types',
        'method' => 'replace',
        'event' => 'change',
        'effect' => 'fade',
        'progress' => ['type' => 'throbber'],
      ],
      '#states' => [
        'visible' => [
          'select[name="version"]' => ['value' => 'other'],
        ],
      ],
    ];

    // Get Font Awesome license.
    $license = fontawesome_ui_detect_license($library, $assets);
    $license_label = $license == 'pro' ? $pro_label : $free_label;
    $form['version_wrapper']['assets'] = [
      '#type' => 'item',
      '#field_suffix' => $license_label,
      '#states' => [
        'visible' => [
          'select[name="method"]' => ['value' => 'local'],
        ],
      ],
    ];
    $form['version_wrapper']['version_description'] = [
      '#type' => 'item',
      '#description' => $this->t('Choose the latest stable major version that has been released. Select "Other" option to choose a specific release from older minor versions. The installed local library will automatically detect the version.'),
    ];
    $form['version_wrapper']['release_notice'] = [
      '#type' => 'item',
      '#description' => $this->t('Notice: Major versions (e.g., v5.x) introduce significant changes, while minor versions (e.g., v5.2.x) focus on bug fixes and improvements. Choosing the latest minor release within a major version ensures stability with up-to-date enhancements without major changes.'),
      '#states' => [
        'visible' => [
          'select[name="version"]' => ['value' => 'other'],
        ],
        'enabled' => [
          ':input[name="version"]' => ['value' => 'other'],
        ],
      ],
    ];

    // Production or minimized version.
    $build_states = [];
    if ($method == 'local' && ($assets === FALSE || $assets === 'js' || $assets === 'css')) {
      $build_states['invisible']['select[name="method"]'] = ['value' => 'local'];
    }
    $form['build'] = [
      '#type' => 'details',
      '#title' => $this->t('Build variant'),
      '#open' => !$config->get('build.variant'),
      '#disabled' => $method == 'local' && $assets !== TRUE,
      '#states' => $build_states,
    ];
    $variant_states = [];
    if ($method == 'cdn') {
      $variant_states['disabled']['select[name="cdn"]'] = ['value' => 'fontawesome'];
    }
    $form['build']['variant'] = [
      '#title' => $this->t('Choose production or development library.'),
      '#description' => $this->t('These settings work with both local libraries and CDN methods.'),
      '#type' => 'radios',
      '#options' => [
        $this->t('Use non-minimized library (Development)'),
        $this->t('Use minimized library (Deployment)'),
      ],
      '#default_value' => $config->get('build.variant'),
      '#states' => $variant_states,
    ];

    // Production or minimized version.
    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Font Awesome'),
      '#open' => intval($version) > 4,
      '#disabled' => !(intval($version) > 4),
    ];

    // Font Awesome library type.
    $form['options']['library'] = [
      '#title' => $this->t('Library type'),
      '#description'   => $this->t('Choose the type of FontAwesome library to use. "SVG + JS" offers better performance and customization options, while "Web Fonts" provides broader browser compatibility.'),
      '#type' => 'select',
      '#options' => [
        'svg' => $this->t('SVG with JS'),
        'webfonts' => $this->t('Web Fonts with CSS'),
      ],
      '#default_value' => $config->get('library'),
    ];

    // Font Awesome icon tag.
    $form['options']['tag'] = [
      '#title' => $this->t('Icon tag'),
      '#description' => $this->t('Font Awesome works with any HTML element. By default, it uses the &lt;i&gt; tag, but you can choose another, like &lt;span&gt;. Changing this setting will alter how icons are inserted and requires clearing the site cache.'),
      '#type' => 'select',
      '#options' => [
        'i' => $this->t('&lt;i&gt;'),
        'span' => $this->t('&lt;span&gt;'),
      ],
      '#default_value' => empty($config->get('tag')) ? 'i' : $config->get('tag'),
    ];

    // Font Awesome CSS Pseudo-elements.
    $form['options']['pseudo'] = [
      '#title' => $this->t('CSS Pseudo-elements'),
      '#description' => $this->t("Use CSS pseudo-elements to add icons without modifying HTML. Font Awesome uses <code>::before</code> for this purpose. It's always available with Webfonts, but enabling it for SVG with JS can slow down your site. Refer to the @pseudoElementsLink for more info.", [
        '@pseudoElementsLink' => Link::fromTextAndUrl($this->t('Font Awesome guide to pseudo-elements'), Url::fromUri('https://docs.fontawesome.com/web/setup/upgrade/pseudo-elements'))->toString(),
      ]),
      '#type' => 'checkbox',
      '#default_value' => $config->get('pseudo'),
    ];

    $form['options']['validation'] = [
      '#title' => $this->t('Icon validation'),
      '#description' => $this->t("Disable this option to skip icon name validation, which is useful for custom icons from Font Awesome's hosted kits."),
      '#type' => 'checkbox',
      '#default_value' => $config->get('validation'),
    ];

    // Enable right-to-left languages.
    $form['options']['rtl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Support RTL'),
      '#default_value' => $config->get('rtl'),
      '#description' => $this->t('Enable right-to-left text support in Font Awesome for languages such as Persian/Arabic across your layouts, components and utilities.'),
    ];

    // Vertical tabs for usability.
    $form['usability'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Usability'),
      '#weight' => 99,
    ];

    // Per-file usability.
    $form['file_types'] = [
      '#type' => 'details',
      '#title' => $this->t('Files'),
      '#attributes' => [
        'class' => [
          'details--settings',
          'b-tooltip',
        ],
      ],
      '#group' => 'usability',
      '#open' => TRUE,
    ];

    // Font Awesome files' wrapper.
    $form['file_types']['files_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'fontawesome-file-wrapper',
          'form-item',
        ],
      ],
    ];

    // States for Font Awesome icons based on version.
    $file_states = [
      ':input[name="all"]' => ['checked' => FALSE],
    ];

    // Get the triggering element's version or use the default version.
    $triggerdElement = $form_state->getTriggeringElement();
    if (isset($triggerdElement['#name']) && ($triggerdElement['#name'] == 'version' || $triggerdElement['#name'] == 'release')) {
      $current_version = $triggerdElement['#value'] != 'other' ? $triggerdElement['#value'] : $latest_version;
    }
    else {
      $current_version = $version;
    }

    // Add an "All styles" checkbox to toggle all Font Awesome styles.
    // Note: Versions below 5 only use fontawesome.css without style variants.
    $form['file_types']['all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('All styles'),
      '#default_value' => intval($current_version) > 4 ? $config->get('file_types.all') : TRUE,
      '#description' => $this->t('If you don’t need the full Font Awesome icon set or old version compatibilities, disable "All" to select specific icon styles like solid, reducing file size and only loading necessary icons.'),
      '#disabled' => !(intval($current_version) > 4),
    ];

    // Create a container for per-style options when "All styles" is off.
    if (intval($current_version) > 4) {
      $form['file_types']['all_files'] = [
        '#type' => 'container',
        '#title' => $this->t('Particle files'),
        '#prefix' => '<div id="all-file">',
        '#suffix' => '</div>',
        '#states' => [
          'invisible' => [
            ':input[name="all"]' => ['checked' => TRUE],
          ],
        ],
      ];

      // Font Awesome icon styles (Free).
      $free_styles = fontawesome_ui_icon_styles($version, 'free', ['title', 'desc']);
      foreach ($free_styles as $style => $detail) {
        $file_states[':input[name="' . $style . '"]'] = ['checked' => FALSE];
        $form['file_types']['all_files'][$style] = [
          '#title' => $this->t('Use @icon-style', ['@icon-style' => $detail['title']]),
          '#description'   => $this->t('Use the `%icon-style` style for Font Awesome icons: @icon-description', [
            '%icon-style' => $detail['title'],
            '@icon-description' => $detail['desc'],
          ]),
          '#type' => 'checkbox',
          '#default_value' => $config->get('file_types.' . $style),
        ];
      }

      // Font Awesome icon styles (Pro Only).
      $pro_styles = fontawesome_ui_icon_styles($version, 'pro', ['title', 'desc'], [], TRUE);
      $pro_only = $license != 'pro' ? $pro_only_label : '';
      foreach ($pro_styles as $style => $detail) {
        $file_states[':input[name="' . $style . '"]'] = ['checked' => FALSE];
        $form['file_types']['all_files'][$style] = [
          '#title' => $this->t('Use @icon-style', ['@icon-style' => $detail['title']]) . $pro_only,
          '#description' => $this->t('Use the `%icon-style` style for Font Awesome icons: @icon-description', [
            '%icon-style' => $detail['title'],
            '@icon-description' => $detail['desc'],
          ]),
          '#type' => 'checkbox',
          '#default_value' => $license == 'free' ? FALSE : $config->get('file_types.' . $style),
          '#disabled' => $license != 'pro',
        ];
      }

      // In v6, add options for v5/v4 font face CSS for backward compatibility.
      if (intval($current_version) > 5) {
        // Compatibility v5 Font Face file.
        $file_states[':input[name="v5_font_face"]'] = ['checked' => FALSE];
        $form['file_types']['all_files']['v5_font_face'] = [
          '#title' => $this->t('Use v5 font face'),
          '#description' => $this->t('If your site previously used Font Awesome 5 classes, include the `v5-font-face.css` file for compatibility.'),
          '#type' => 'checkbox',
          '#default_value' => $config->get('file_types.v5_font_face'),
        ];

        // Compatibility v4 Font Face file.
        $file_states[':input[name="v4_font_face"]'] = ['checked' => FALSE];
        $form['file_types']['all_files']['v4_font_face'] = [
          '#title' => $this->t('Use v4 font face'),
          '#description' => $this->t('If your site previously used Font Awesome 4 classes, include the `v4-font-face.css` file for compatibility.'),
          '#type' => 'checkbox',
          '#default_value' => $config->get('file_types.v4_font_face'),
        ];
      }

      // Version 4 Backwards Compatibility.
      $file_states[':input[name="v4_shims"]'] = ['checked' => FALSE];
      $form['file_types']['v4_shims'] = [
        '#title' => $this->t('Version 4 shims file'),
        '#description' => $this->t('Include the `v4-shims` for translates old class names to version 6. Check this option if you want to automatically translate old class names to the new version. Note: You can use `v4-font-face` to ensure old icons display correctly without changing class names.'),
        '#type' => 'checkbox',
        '#default_value' => $config->get('file_types.v4_shims'),
      ];
    }

    // Adds an option to load a custom Font Awesome file.
    $file_states[':input[name="custom"]'] = ['checked' => FALSE];
    $form['file_types']['custom'] = [
      '#title' => $this->t('Custom icon file'),
      '#description' => $this->t('Enable this option to include a custom Font Awesome file that contains your chosen icon definitions, (e.g., custom-icons.js or custom-icons.css).'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('file_types.custom'),
    ];

    // Warning for disabled all files.
    $file_warning = $this->t('If all icon styles are disabled, the library cannot load. Therefore, after saving settings, the load option will be disabled and revert to the default state.');
    $form['file_types']['file_warning'] = [
      '#type' => 'container',
      '#markup' => '<div class="file-types-report">' . $file_warning . '</div>',
      '#prefix' => '<div id="file-warning">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [$file_states],
      ],
      '#weight' => -9,
    ];

    // Get list of all installed themes.
    $themes = $this->themeHandler->listInfo();
    $active_themes = [];
    foreach ($themes as $key => $theme) {
      if ($theme->status) {
        $active_themes[$key] = $theme->info['name'];
      }
    }

    // Per-theme usability.
    $form['theme_groups'] = [
      '#type' => 'details',
      '#title' => $this->t('Themes'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group' => 'usability',
      '#open' => TRUE,
    ];
    $form['theme_groups']['themes'] = [
      '#type' => 'select',
      '#title' => $this->t('Installed themes'),
      '#description' => $this->t("Specify themes by selecting their names. The list of themes to which library loading will be restricted."),
      '#options' => $active_themes,
      '#multiple' => TRUE,
      '#default_value' => $config->get('theme_groups.themes'),
    ];
    $form['theme_groups']['theme_negate'] = [
      '#type' => 'radios',
      '#title' => $this->t('Activate on specific themes'),
      '#title_display' => 'invisible',
      '#options' => [
        $this->t('All themes except those selected'),
        $this->t('Only the selected themes'),
      ],
      '#default_value' => $config->get('theme_groups.negate'),
    ];

    // Per-path usability.
    $form['request_path'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group' => 'usability',
    ];
    $form['request_path']['pages'] = [
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _fontawesome_ui_array_to_string($config->get('request_path.pages')),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front' => '<front>',
      ]),
    ];
    $form['request_path']['page_negate'] = [
      '#type' => 'radios',
      '#title' => $this->t('Load Font Awesome on specific pages'),
      '#title_display' => 'invisible',
      '#options' => [
        $this->t('All pages except those listed'),
        $this->t('Only for the listed pages'),
      ],
      '#default_value' => $config->get('request_path.negate'),
    ];

    // The FontAwesome UI settings library.
    $form['#attached']['library'][] = 'fontawesome_ui/fontawesome.settings';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function filesAjaxCallback(array &$form, FormStateInterface $form_state) {
    // Skip if the method is set to "local" as version changes are not allowed.
    if ($form_state->getValue('method') == 'local') {
      return;
    }

    // Save the selected or custom version in module configuration.
    $version = $form_state->getValue('version') != 'other' ? $form_state->getValue('version') : $form_state->getValue('release');
    $this->config('fontawesome_ui.settings')
      ->set('version', $version)
      ->save();

    // Mark the form for rebuilding.
    $form_state->setRebuild(TRUE);

    // Update the file types section dynamically via AJAX.
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#all-file', $form['file_types']['all_files']));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve form input values.
    $values = $form_state->getValues();

    // Set the default variant from form values or detected library.
    $variant = $values['variant'];

    // Force detected version from installed library.
    $suffix = fontawesome_ui_check_installed($values['library']);
    if ($suffix && $values['method'] == 'local') {
      $license = fontawesome_ui_detect_license($values['library'], $suffix);
      $version = fontawesome_ui_detect_version();
      $variant = $suffix === TRUE ? $variant : 1;
    }
    else {
      $license = 'free';
      $version = $values['version'] == 'other' ? $values['release'] : $values['version'];
      $variant = $values['method'] === 'cdn' && $values['cdn'] === 'fontawesome' ? 1 : $variant;
    }

    // Check if no icon style enabled to load.
    $particles = FALSE;
    $styles = fontawesome_ui_icon_styles($version, 'pro');
    if ($values['all'] == 0) {
      if (is_array($styles) && count($styles)) {
        foreach ($styles as $style => $title) {
          if (!empty($values[$style])) {
            $particles = TRUE;
            break;
          }
        }
      }

      // Turn off bootstrap library load instead and reset files to default.
      if (!$particles) {
        $values['all'] = 1;
        $values['load'] = 0;
      }
    }

    // Save the updated Font Awesome settings.
    $config = $this->config('fontawesome_ui.settings');
    $config
      ->set('load', $values['load'])
      ->set('hide', isset($values['hide']) && $values['hide'] !== 0 ?? FALSE)
      ->set('method', $values['method'])
      ->set('cdn', intval($version) > 4 ? $values['cdn'] : 'cdnjs')
      ->set('integrity', (string) $values['integrity'])
      ->set('version', $version)
      ->set('license', $license)
      ->set('build.variant', $variant)
      ->set('library', $values['library'])
      ->set('tag', $values['tag'])
      ->set('pseudo', $values['pseudo'])
      ->set('validation', $values['validation'])
      ->set('rtl', $values['rtl'])
      ->set('file_types.all', intval($version) > 4 ? $values['all'] : TRUE);

    // Check if the version is higher than Font Awesome 4,
    // due to seperated icon styles in version 6 and 5.
    if (intval($version) > 4) {
      // Set file type from selected version icon styles.
      if (count($styles)) {
        foreach ($styles as $style => $title) {
          $config->set("file_types.$style", isset($values[$style]) && $values[$style] ?? FALSE);
        }
      }

      // Save v5/v4 font face settings for compatibility (version > 5).
      if (intval($version) > 5) {
        $config->set('file_types.v5_font_face', isset($values['v5_font_face']) && $values['v5_font_face'] ?? FALSE);
        $config->set('file_types.v4_font_face', isset($values['v4_font_face']) && $values['v4_font_face'] ?? FALSE);
      }

      // Version 4 shims Backwards Compatibility.
      $config->set('file_types.v4_shims', isset($values['v4_shims']) && $values['v4_shims'] ?? FALSE);

      // Set custom icon file.
      $config->set('file_types.custom', isset($values['custom']) && $values['custom'] ?? FALSE);
    }

    // Save theme and page settings for Font Awesome configuration.
    $config->set('theme_groups.negate', $values['theme_negate'])
      ->set('theme_groups.themes', $values['themes'])
      ->set('request_path.negate', $values['page_negate'])
      ->set('request_path.pages', _fontawesome_ui_string_to_array($values['pages']))
      ->save();

    // Flush caches so the updated config can be checked.
    $cid = 'fontawesome_ui:font_awesome_metadata';
    $this->cacheBackend->delete($cid);
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
