<?php

namespace Drupal\fontawesome_ui\Constants;

/**
 * Defines constants for the FontAwesome UI module.
 */
class FontAwesomeConstants {
  /**
   * The latest version of Font Awesome.
   */
  const LATEST_VERSION = '6.7.2';

  /**
   * The Font Awesome download url.
   */
  const DOWNLOAD_LINK = 'https://github.com/FortAwesome/Font-Awesome/archive/6.x.zip';

}
