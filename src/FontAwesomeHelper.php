<?php

namespace Drupal\fontawesome_ui;

use Drupal\Component\Utility\Color as ColorUtility;

/**
 * Provides conversion helper functions for Font Awesome.
 */
class FontAwesomeHelper implements FontAwesomeHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function convertSize($size, $from_unit, $to_unit, $base_font_size = 16.0) {
    // Convert the size to a float.
    $size = floatval($size);

    // Define conversion factors.
    // Assuming 1rem = 16px and 1em = base_font_size.
    $conversion_factors = [
      'px_to_rem' => 1 / 16,
      'rem_to_px' => 16,
      'px_to_em' => 1 / $base_font_size,
      'em_to_px' => $base_font_size,
      'rem_to_em' => 16 / $base_font_size,
      'em_to_rem' => $base_font_size / 16,
    ];

    // Determine the conversion factor based on the units.
    if ($from_unit === 'px' && $to_unit === 'rem') {
      $conversion_factor = $conversion_factors['px_to_rem'];
    }
    elseif ($from_unit === 'rem' && $to_unit === 'px') {
      $conversion_factor = $conversion_factors['rem_to_px'];
    }
    elseif ($from_unit === 'px' && $to_unit === 'em') {
      $conversion_factor = $conversion_factors['px_to_em'];
    }
    elseif ($from_unit === 'em' && $to_unit === 'px') {
      $conversion_factor = $conversion_factors['em_to_px'];
    }
    elseif ($from_unit === 'rem' && $to_unit === 'em') {
      $conversion_factor = $conversion_factors['rem_to_em'];
    }
    elseif ($from_unit === 'em' && $to_unit === 'rem') {
      $conversion_factor = $conversion_factors['em_to_rem'];
    }
    else {
      throw new \InvalidArgumentException('Unsupported unit conversion.');
    }

    // Perform the conversion and round to 3 decimal places.
    return round($size * $conversion_factor, 3);
  }

  /**
   * {@inheritdoc}
   */
  public function convertTime($time, $from_unit, $to_unit) {
    // Convert the time to a float.
    $time = floatval($time);

    // Define conversion factors.
    // Assuming 1 second = 1000 milliseconds.
    $conversion_factors = [
      'ms_to_s' => 1 / 1000,
      's_to_ms' => 1000,
    ];

    // Determine the conversion factor based on the units.
    if ($from_unit === 'ms' && $to_unit === 's') {
      $conversion_factor = $conversion_factors['ms_to_s'];
    }
    elseif ($from_unit === 's' && $to_unit === 'ms') {
      $conversion_factor = $conversion_factors['s_to_ms'];
    }
    else {
      throw new \InvalidArgumentException('Unsupported unit conversion.');
    }

    // Perform the conversion.
    return $time * $conversion_factor;
  }

  /**
   * {@inheritdoc}
   */
  public function convertColor($color, $from, $to) {
    // Ensure the input formats are supported.
    $supported_formats = ['hex', 'rgb'];
    if (!in_array($from, $supported_formats) || !in_array($to, $supported_formats)) {
      throw new \InvalidArgumentException('Unsupported color format conversion.');
    }

    // Convert RGB string to RGB array if necessary.
    if ($from === 'rgb' && is_string($color) && preg_match('/^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/', $color, $matches)) {
      $color = [
        'red' => (int) $matches[1],
        'green' => (int) $matches[2],
        'blue' => (int) $matches[3],
      ];
    }

    // Convert hex to RGB.
    if ($from === 'hex' && $to === 'rgb') {
      // Ensure the color is a valid hex string.
      if (!is_string($color) || !preg_match('/^#?[a-fA-F0-9]{6}$/', $color)) {
        throw new \InvalidArgumentException('Invalid hex color format.');
      }

      // Remove the leading '#' if present.
      $color = ltrim($color, '#');
      // Convert the hex color to RGB using a utility function.
      return ColorUtility::hexToRgb($color);
    }
    // Convert RGB to hex.
    elseif ($from === 'rgb' && $to === 'hex') {
      // Ensure the color is a valid RGB array.
      if (!is_array($color) || !isset($color['red']) || !isset($color['green']) || !isset($color['blue'])) {
        throw new \InvalidArgumentException('Invalid RGB color format.');
      }

      // Convert the RGB color to hex using a utility function.
      return ColorUtility::rgbToHex([
        'r' => $color['red'],
        'g' => $color['green'],
        'b' => $color['blue'],
      ]);
    }

    // If the from and to formats are the same, return the color as is.
    return $color;
  }

  /**
   * {@inheritdoc}
   */
  public function getColorFormat(string $color) {
    // Check if color is in HEX format.
    if (preg_match('/^#(?:[0-9a-fA-F]{3}){1,2}$/', $color)) {
      return 'hex';
    }

    // Check if color is in RGB format.
    if (preg_match('/^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/', $color)) {
      return 'rgb';
    }

    // If neither HEX nor RGB format, return 'unknown'.
    return 'unknown';
  }

}
