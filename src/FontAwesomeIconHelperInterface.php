<?php

namespace Drupal\fontawesome_ui;

/**
 * Interface for FontAwesomeIconHelper.
 */
interface FontAwesomeIconHelperInterface {

  /**
   * Provides a list of Font Awesome icon animations.
   *
   * @return array
   *   An associative array of icon animations.
   */
  public static function getIconAnimations();

  /**
   * Provides a list of icon size options.
   *
   * @return array
   *   An associative array of icon size options.
   */
  public static function getIconSizeOptions();

  /**
   * Provides a list of icon flip options.
   *
   * @return array
   *   An associative array of icon flip options.
   */
  public static function getIconFlipOptions();

  /**
   * Provides a list of icon rotate options.
   *
   * @return array
   *   An associative array of icon rotate options.
   */
  public static function getIconRotateOptions();

  /**
   * Provides a list of icon border style options.
   *
   * @return array
   *   An associative array of icon border style options.
   */
  public static function getIconBorderStyleOptions();

  /**
   * Provides a list of icon pull options.
   *
   * @return array
   *   An associative array of icon pull options.
   */
  public static function getIconPullOptions();

  /**
   * Provides a list of icon animation direction options.
   *
   * @return array
   *   An associative array of icon direction options.
   */
  public static function getIconDirectionOptions();

  /**
   * Provides a list of icon animation timing function options.
   *
   * @return array
   *   An associative array of icon timing options.
   */
  public static function getIconTimingOptions();

  /**
   * Helper function to generate color options.
   *
   * @return array
   *   An associative array of icon color options.
   */
  public static function getIconColorOptions();

  /**
   * Provides a list of icon positions.
   *
   * @return array
   *   An associative array of icon positions.
   */
  public static function getIconPositionOptions();

  /**
   * Generates the markup for the positioning field.
   *
   * @return string
   *   The HTML markup for the positioning field.
   */
  public static function getIconPositionMarkup();

}
