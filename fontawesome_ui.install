<?php

/**
 * @file
 * Install, uninstall and update functions for the FontAwesome UI module.
 */

use Drupal\Core\Link;
use Drupal\Core\Url;

define('FONTAWESOME_UI_DOWNLOAD_URL', 'https://github.com/FortAwesome/Font-Awesome/archive/6.x.zip');

/**
 * Implements hook_requirements().
 */
function fontawesome_ui_requirements($phase) {
  $requirements = [];

  if ($phase != 'runtime') {
    return $requirements;
  }

  // Load Font Awesome settings.
  $fontawesome_config = \Drupal::configFactory()->get('fontawesome_ui.settings');

  // Get Font Awesome library type.
  $fontawesome_library = $fontawesome_config->get('library');

  // Check if Font Awesome is installed and get load method.
  $fontawesome_method = $fontawesome_config->get('method');
  $fontawesome_exists = fontawesome_ui_check_installed($fontawesome_library);

  // Check if the user has suppressed the warning in FontAwesome UI settings.
  $hide_warning = $fontawesome_config->get('hide');

  // Get the Font Awesome using library type info.
  $library_type = $fontawesome_library == 'svg' ? t('SVG with JS') : t('Webfonts with CSS');

  // Show the status of the library in the status report section.
  if ($fontawesome_exists) {
    // Detect using Font Awesome version.
    $version = fontawesome_ui_detect_version($fontawesome_library, $fontawesome_exists);

    // Get the Font Awesome using version and license info.
    $license = fontawesome_ui_detect_license($fontawesome_library, $fontawesome_exists);

    // Ensures and verifies the library version and license.
    $reset = _fontawesome_ui_enforce_verify_version($fontawesome_exists, $fontawesome_library, $fontawesome_method, $version, $license);
    if ($reset) {
      $version = $fontawesome_config->get('version');
      $license = $fontawesome_config->get('license');
    }

    $status = t('Installed Version: <em>(%license @version)</em> %library_type', [
      '@version' => $version,
      '%license' => strtoupper($license),
      '%library_type' => $library_type,
    ]);

    // First check if we're using everything.
    if ($fontawesome_config->get('file_types.all')) {
      // Attach the main library.
      $file_loaded = [
        t('All Font Awesome icons loaded.'),
      ];
    }
    // Else we attach the libraries piecemeal.
    else {
      $file_loaded = [];
      if ($fontawesome_config->get('file_types.solid')) {
        $file_loaded[] = t('Solid icons loaded');
      }
      if ($fontawesome_config->get('file_types.regular')) {
        $file_loaded[] = t('Regular icons loaded');
      }
      if ($fontawesome_config->get('file_types.light')) {
        $file_loaded[] = t('Light icons loaded');
      }
      if ($fontawesome_config->get('file_types.brands')) {
        $file_loaded[] = t('Brands icons loaded');
      }
    }

    $description = [
      '#theme' => 'item_list',
      '#items' => $file_loaded,
      '#title' => '',
      '#list_type' => 'ul',
      '#attributes' => [],
    ];
  }
  else {
    // Get using Font Awesome version from settings.
    $version = $fontawesome_config->get('version');
    $description = t('The Font Awesome library is using <strong>CDN</strong> and is not installed in your local libraries.<br>You can <a href="@downloadUrl" rel="external" target="_blank">download</a> and extract to "/libraries/fontawesome". Please see the FontAwesome UI module README file for more details.', [
      '@downloadUrl' => FONTAWESOME_UI_DOWNLOAD_URL,
    ]);
    // Returns TRUE for the library if the library
    // warning was hidden when using the CDN method.
    if ($fontawesome_exists = $hide_warning) {
      $status = t('Not installed - CDN Version: <em>(FREE @version) %library_type</em>', [
        '@version' => $version,
        '%library_type' => $library_type,
      ]);
    }
    else {
      $status = t('Not installed');
    }
  }

  // The Font Awesome library title.
  $title = t('Font Awesome');

  // Check if loading Font Awesome is enabled in the settings
  // to prepare information for requirements status report.
  if ($fontawesome_config->get('load')) {
    $requirements['fontawesome_ui'] = [
      'title' => $title,
      'value' => $status,
      'severity' => $fontawesome_exists ? REQUIREMENT_OK : REQUIREMENT_ERROR,
      'description' => $description,
    ];
  }
  else {
    $requirements['fontawesome_ui'] = [
      'title' => $title,
      'value' => t('Manually mode'),
      'severity' => REQUIREMENT_OK,
      'description' => t('The Font Awesome library is currently not loaded according to the configuration. If this is mistake, please visit @settings page and refer to the module guide. If another module or theme manually loads the library, this setting is appropriate.', [
        '@settings' => Link::createFromRoute(t('FontAwesome settings'), 'fontawesome.settings')->toString(),
      ]),
    ];
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function fontawesome_ui_schema() {
  $schema['fontawesome_icon'] = [
    'description' => 'Stores FontAwesome icon selectors.',
    'fields' => [
      'faid' => [
        'description' => 'Primary Key: unique ID for FontAwesome icon selectors.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ],
      'selector' => [
        'description' => 'FontAwesome icon selector.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'label' => [
        'description' => 'Label of icon.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'comment' => [
        'description' => 'Comment for icon.',
        'type'        => 'text',
        'size'        => 'big',
        'not null'    => TRUE,
      ],
      'changed' => [
        'description' => 'Timestamp when the icon was most recently modified.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'default'     => 0,
        'size'        => 'big',
      ],
      'status' => [
        'description' => 'Boolean indicating whether the icon is enabled.',
        'type'        => 'int',
        'not null'    => FALSE,
        'default'     => 0,
        'size'        => 'tiny',
      ],
      'options' => [
        'type'        => 'blob',
        'not null'    => TRUE,
        'size'        => 'big',
        'description' => 'The options data in serialized form.',
      ],
    ],
    'indexes' => [
      'label'    => ['label'],
      'selector' => ['selector'],
      'changed'  => ['changed'],
    ],
    'primary key' => ['faid'],
  ];
  return $schema;
}

/**
 * Implements hook_install().
 */
function fontawesome_ui_install() {
  // Check for Font Awesome library installation.
  $fontawesome_exists = fontawesome_ui_check_installed();
  if ($fontawesome_exists === FALSE) {
    \Drupal::messenger()->addWarning(
      t('The <em>FontAwesome UI</em> module requires the Font Awesome icon library.<br>
               Currently, the Font Awesome library is loaded via <strong>CDN</strong> and is not available in your local libraries.<br>
               Please <a href=":downloadUrl" rel="external" target="_blank">Download</a> the desired version and unzip into <strong>/libraries/fontawesome</strong> directory.',
        [
          ':downloadUrl' => FONTAWESOME_UI_DOWNLOAD_URL,
        ]
      )
    );
  }
  // Add Font Awesome settings link status.
  \Drupal::messenger()->addStatus(t('Thanks for installing FontAwesome UI module.<br>Check Font Awesome <a href=":settings">global settings</a> for more configuration.', [
    ':settings' => Url::fromRoute('fontawesome.settings')->toString(),
  ]));
}

/**
 * Update version_type to license in settings.
 */
function fontawesome_ui_update_9001() {
  // Load the current configuration.
  $config = \Drupal::configFactory()->getEditable('fontawesome_ui.settings');

  // Check if the old key exists and migrate the value to the new key.
  if ($config->get('version_type') !== NULL) {
    $license_value = $config->get('version_type');
    $config->set('license', $license_value);
    $config->clear('version_type');
    $config->save();
  }
}
