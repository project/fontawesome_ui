# Font Awesome UI Module Documentation

## Introduction

Integrates Font Awesome, the leading icon library and toolkit on the Internet,
providing access to the largest open-source web components library.

## Features

- **Support for All Stable Versions:** Ensures compatibility with all stable
    versions of Font Awesome.
- **Flexible Loading Options:** Choose between loading the library via CDN or
    from local libraries, supporting both compiled and source files.
- **RTL Support:** Provides right-to-left (RTL) language support for languages
    like Persian, Farsi, and Arabic.
- **Composer Installation:** Allows separate installation of the module and
    the library via Composer.
- **Theme and URL Restrictions:** Restrict the loading of the library
    by theme and URL.
- **Minification Options:** Choose between minified and non-minified
    versions of the library.
- **File Loading Restrictions:** Control the loading of JavaScript
    and CSS files.
- **Customizability:** Offers various settings for advanced customization.
- **User-Friendly:** Easy to use with a straightforward interface.

## Requirements

Download the desired version of the Font Awesome Library from GitHub:

- [Font Awesome Library](https://github.com/FortAwesome/Font-Awesome/archive/6.x.zip)

## Installation

1. **Download the FontAwesome UI Module:**
  - [FontAwesome UI Module](https://www.drupal.org/project/fontawesome_ui)

2. **Extract and Place in Contributed Modules Directory:**
  - `/modules/contrib/fontawesome_ui`

3. **Create a Libraries Directory:**
  - `/libraries`

4. **Create a 'fontawesome' Directory Inside Libraries:**
  - `/libraries/fontawesome`

5. **Download Font Awesome Icon library:**
  - [Font Awesome (Latest)](https://github.com/FortAwesome/Font-Awesome/archive/6.x.zip)

6. **Place Files in the /libraries/fontawesome Directory:**
  - For compiled CSS and JS files:
    - `/libraries/fontawesome/css/all.min.css`
    - `/libraries/fontawesome/js/all.min.js`

7. **Enable the FontAwesome UI Module:**

## Usage

### Introduction to Using the Font Awesome UI Module

The Font Awesome UI module simplifies and enhances the experience
of using Font Awesome with Drupal sites. Key features include:

- **Support for All Versions:** Supports all versions of Font Awesome.
- **Loading Options:** Utilize the CDN option if you prefer not to download
    and locally load the Font Awesome library.
- **Development Mode:** Allows the use of non-minimized for debugging purposes.
- **Custom Settings:** Configure restrictions for loading Font Awesome files,
    specify themes, and set page paths as needed.
- **RTL Support:** Ensure that one of the RTL languages is enabled on your
    site to support languages like Persian and Arabic.

### How Does It Work?

1. **Enable the "FontAwesome UI" Module:**
  - Follow the installation instructions above.

2. **Go to FontAwesome UI Settings:**
  - `/admin/config/user-interface/fontawesome`

3. **Change Default Settings:**
  - Use SVG + JS or  support or Web Fonts with CSS.
  - Restrict load on specific themes as needed.

4. **Enjoy Seamless Integration:**
  - Benefit from the seamless and flexible integration
    of Font Awesome with your Drupal site.

## Documentation

For detailed documentation, refer to the official Font Awesome documentation:

- [Font Awesome Documentation](https://fontawesome.com/start)

## Maintainers

Current module maintainer:

- **Mahyar Sabeti:** [Mahyar Sabeti on Drupal.org](https://www.drupal.org/u/mahyarsbt)

## Additional Information

Font Awesome is the Internet's leading icon library and toolkit,
used by millions of designers, developers, and content creators.
For more information, visit:

- [Font Awesome GitHub Repository](https://github.com/FortAwesome/Font-Awesome)
- [Font Awesome Download Link](https://github.com/FortAwesome/Font-Awesome/archive/6.x.zip)
- [Font Awesome Website](https://fontawesome.com/)

If you need further information or assistance, feel free to ask!
