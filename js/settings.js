/**
 * @file
 * Contains definition of the behaviour Font Awesome settings.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.fontAwesomeSettings = {
    attach: function (context, settings) {

      // The drupalSetSummary method required for this behavior is not
      // available on the Font Awesome settings page, so we need to make
      // sure this behavior is processed only if drupalSetSummary is defined.
      if (typeof $.fn.drupalSetSummary !== 'undefined') {
        $(once('fontawesome-file-types', '[data-drupal-selector="edit-file-types"]'),
        ).drupalSetSummary((context) => {
          const values = [];
          let $all_files = $(context).find('input[name="all"]');

          if (!$all_files.prop('checked')) {
            values.push(
              Drupal.t('Restricted to certain icon styles'),
            );
          } else {
            values.push(
              Drupal.t('Not restricted'),
            );
          }

          return values.join(',<br>');
        });

        $(once('fontawesome-theme-groups', '[data-drupal-selector="edit-theme-groups"]'),
        ).drupalSetSummary((context) => {
          const $themes = $(context).find(
            'select[name="themes\[\]"]',
          );
          if (!$themes.length || !$themes[0].value) {
            return Drupal.t('Not restricted');
          }

          return Drupal.t('Restricted to !theme', {'!theme': $themes.val()});
        });

        $(once('fontawesome-request-path', '[data-drupal-selector="edit-request-path"]'),
        ).drupalSetSummary((context) => {
          const $pages = $(context).find(
            'textarea[name="pages"]',
          );
          if (!$pages.length || !$pages[0].value) {
            return Drupal.t('Not restricted');
          }

          return Drupal.t('Restricted to certain pages');
        });
      }

    }
  };

})(jQuery, Drupal, drupalSettings, once);
