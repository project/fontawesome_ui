/**
 *  jQuery Font Awesome UI - Icon Browser.
 *
 * A versatile icon picker leveraging Font Awesome icons and jQuery,
 * specifically designed for the Drupal Font Awesome UI module.
 *
 * This plugin has been comprehensively rewritten and optimized
 * to fully support the Font Awesome 6 library, ensuring a seamless
 * and efficient user experience.
 *
 *  https://www.drupal.org/project/fontawesome_ui
 *
 *  Made by Mahyar Sabeti:
 *
 * {@link https://www.drupal.org/u/mahyarsbt}
 *
 *  Licensed under MIT License.
 */
(function ($) {
  /**
   * Initializes the FontAwesome icon browser.
   *
   * This plugin creates a modal browser for selecting FontAwesome icons.
   *
   * @param {Object} options
   *   Configuration options for the icon browser.
   * @param {Object} options.source
   *   The source of icons to populate the browser.
   * @param {Object} options.messages
   *   Customizable browser UI messages, such as labels and placeholders.
   * @param {string} [options.idSuffix]
   *   Suffix to uniquely identify modal elements.
   * @param {boolean} [options.allowEmpty=false]
   *   Whether to allow empty icon selection.
   * @param {boolean} [options.autoLoad=false]
   *   Whether the browser should auto-open upon initialization.
   * @param {string} [options.selectedIcon]
   *   The icon class pre-selected when the browser loads.
   *
   * @return {jQuery}
   *   The jQuery collection this plugin was initialized on.
   */
  $.fn.fontAwesomeIconBrowser = function (options) {
    // Default configuration options.
    const defaults = {
      source: {},
      messages: {
        icon_browser: 'Icon Browser',
        close_label: 'Close',
        search_placeholder: 'Search ...',
        insert_label: 'Insert'
      },
      idSuffix: '',
      allowEmpty: false,
      autoLoad: false,
      selectedIcon: '',
      clickInsert: false,
      showCount: true,
      itemsPerPage: 36,
      version: '6.6.0',
      license: 'free'
    };

    // Merge user options with defaults.
    const settings = $.extend(true, {}, defaults, options);

    // Initialize the browser instance.
    const browser = new FontAwesomeIconBrowser(settings);

    // Use `this.getStyleAlias` from the `FontAwesomeIconBrowser` instance.
    if (settings.selectedIcon) {
      const sanitizedIcon = settings.selectedIcon.replace(/</g, '&lt;').replace(/>/g, '&gt;');
      settings.selectedIcon = browser.getStyleAlias(sanitizedIcon);
    }

    // Automatically open the modal if `autoLoad` is enabled.
    if (settings.autoLoad) {
      $(document).ready(function () {
        browser.openModal();
      });
    }

    // Initialize the plugin for each matched element.
    return this.each(function () {
      const $input = $(this);
      $input.css({
        visibility: 'hidden',
        position: 'relative',
        zIndex: '-1',
        display: 'inline-block',
        height: '40px',
        width: '102px',
        padding: '0px',
        margin: '0px -102px 0px 0px',
        border: '0px none',
        verticalAlign: 'top',
        float: 'none'
      });

      // Create the icon selector.
      const $iconSelector = $(`
        <div class="icons-selector faui-darkgrey" style="position: relative;" data-faui-origin="${$input.attr('id')}">
          <div class="selector" data-faui-origin="${$input.attr('id')}">
            <span class="selected-icon"><i class="${settings.selectedIcon}"></i></span>
            <span class="selector-button">
              <i class="faui-icon-down-dir"></i>
            </span>
          </div>
        </div>
      `);

      // Add the selector to the DOM and bind event listeners.
      $input.before($iconSelector);
      $iconSelector.find('.selector').click(function (event) {
        event.preventDefault(); // Prevent default action to avoid XSS or other unintended actions.
        browser.openModal();
      });

      browser.$input = $input;
      browser.$iconSelector = $iconSelector;
    });
  };

  /**
   * FontAwesomeIconBrowser class.
   *
   * Handles the core logic for the FontAwesome icon browser.
   */
  class FontAwesomeIconBrowser {
    /**
     * Constructor for FontAwesomeIconBrowser.
     *
     * @param {Object} settings
     *   Configuration settings for the browser.
     */
    constructor(settings) {
      this.settings = settings;
      this.currentPage = 1;
      this.iconsPerPage = settings.itemsPerPage;
      this.totalPages = 1;
      this.source = settings.source;
      this.allowEmpty = settings.allowEmpty;
      this.modalElement = null;
      this.sidebarTabs = null;
      this.iconContainer = null;
      this.footerElement = null;
      this.searchInput = null;
      this.searchQuery = '';
      this.activeCategory = 'all_categories';
      this.activeStyle = 'all';
      this.activeFamily = 'all';
      this.activeType = 'all';
      this.hasProIcons = this.checkForProIcons();
      this.hasSharpIcons = this.checkForSharpIcons();
      this.version = settings.version;
      this.license = settings.license;
      this.init();
    }

    // Define style aliases as a static property for reuse across methods.
    static styleAliases = new Map([
      ['fa-sharp fa-solid', 'fass'],
      ['fa-sharp fa-regular', 'fasr'],
      ['fa-sharp fa-light', 'fasl'],
      ['fa-sharp fa-thin', 'fast'],
      ['fa-duotone', 'fad'],
      ['fa-duotone fa-solid', 'fad'],
      ['fa-duotone fa-regular', 'fadr'],
      ['fa-duotone fa-light', 'fadl'],
      ['fa-duotone fa-thin', 'fadt'],
      ['fa-sharp fa-duotone', 'fasd'],
      ['fa-sharp-duotone fa-solid', 'fasds'],
      ['fa-sharp-duotone fa-regular', 'fasdr'],
      ['fa-sharp-duotone fa-light', 'fasdl'],
      ['fa-sharp-duotone fa-thin', 'fasdt'],
      ['fa-solid', 'fas'],
      ['fa-regular', 'far'],
      ['fa-light', 'fal'],
      ['fa-thin', 'fat'],
      ['fa-duotone', 'fad'],
      ['fa-brands', 'fab']
    ]);

    /**
     * Initializes the FontAwesome icon browser.
     *
     * This method sets up the modal, event listeners, and visual components.
     */
    init() {
      this.createModal();
      this.attachEventListeners();
      this.renderSidebarTabs();
      this.renderIcons();
      this.toggleElementsVisibility();
    }

    /**
     * Retrieves all unique styles from the icon source.
     *
     * This function efficiently collects unique styles by using a Set
     * for deduplication and then converts it to an array for use.
     *
     * @return {Array}
     *   An array of unique styles found in the source data.
     */
    getAllStyles() {
      // Use a Set for unique style collection to avoid duplicates.
      const stylesSet = new Set();

      // Iterate through the source data structure to gather all styles.
      Object.values(this.source).forEach(category => {
        Object.values(category).forEach(iconData => {
          iconData.icons.forEach(icon => {
            stylesSet.add(icon.style);
          });
        });
      });

      // Convert Set to Array for return value.
      return Array.from(stylesSet);
    }

    /**
     * Resolves the alias for a given class string.
     *
     * This method matches the class string with predefined style aliases,
     * returning the alias combined with any unmatched classes. It uses
     * a sorted list of aliases for specificity in matching.
     *
     * @param {string} classString
     *   The class string to resolve (e.g., "fa-solid fa-sharp").
     *
     * @return {string}
     *   Combines the resolved alias with unmatched class strings or
     *   returns an empty string if no valid alias is found.
     */
    getStyleAlias(classString) {
      // Early exit for invalid or empty input.
      if (!classString) return '';

      // Split the class string into individual classes for processing.
      const classes = classString.split(' ');

      // Initialize variables for alias and remaining classes.
      let styleAlias = '';
      const remainingClasses = [...classes];

      // Sort aliases by specificity, longest first for exact matches.
      const sortedAliases = [...FontAwesomeIconBrowser.styleAliases.entries()]
        .sort((a, b) => b[0].split(' ').length - a[0].split(' ').length);

      // Find the most specific alias match.
      for (const [aliasKey, aliasValue] of sortedAliases) {
        const aliasClasses = aliasKey.split(' ');
        // Check if all parts of the alias are present in the input.
        if (aliasClasses.every(cls => remainingClasses.includes(cls))) {
          // Assign the matching alias.
          styleAlias = aliasValue;
          // Remove matched classes from remaining classes.
          aliasClasses.forEach(cls => {
            const index = remainingClasses.indexOf(cls);
            if (index !== -1) {
              remainingClasses.splice(index, 1);
            }
          });
          break; // Stop after finding the most specific match.
        }
      }

      // Combine alias with remaining classes, filtering out empty strings.
      return [styleAlias, ...remainingClasses].filter(Boolean).join(' ');
    }

    /**
     * Creates the modal for the FontAwesome icon browser.
     *
     * This method orchestrates the creation of the modal by delegating tasks
     * to smaller, focused functions for better modularity and performance.
     */
    createModal() {
      // Construct the modal's basic structure.
      const modalStructure = this.createModalStructure().trim();
      // Create the header with title and close button.
      const modalHeader = this.createModalHeader().trim();
      // Build the body containing sidebar, search, and icon display.
      const modalBody = this.createModalBody().trim();
      // Generate the footer with pagination controls.
      const modalFooter = this.createModalFooter().trim();

      // Append modal parts to body at once for performance.
      this.modalElement = $(modalStructure)
        .find('.faui-modal--content')
        .append(modalHeader)
        .append(modalBody)
        .append(modalFooter)
        .end()
        .appendTo('body');

      // Cache element refs for quick access.
      this.sidebarTabs = this.modalElement.find('.faui-modal--sidebar-tabs');
      this.iconContainer = this.modalElement.find('.faui-modal--icon-preview');
      this.footerElement = this.modalElement.find('.pagination .page-info');
      this.searchInput = this.modalElement.find('.faui-modal--icon-search input');
      this.insertButton = this.modalElement.find('.faui-insert-icon-button');
      this.statsElement = this.modalElement.find('.faui-stats .icons-results-summary-count h1');
      this.currentPageElement = this.modalElement.find('.faui-stats .current-page');
      this.totalPagesElement = this.modalElement.find('.faui-stats .total-pages');
    }

    /**
     * Creates the basic structure of the modal.
     *
     * @return {string}
     *   HTML string representing the modal's outer structure.
     */
    createModalStructure() {
      // Parse the version string into numbers for class generation.
      const [major, minor] = this.version.split('.').map(Number);
      const versionClass = `faui-v${major}-${minor}`;
      const versionMajor = `faui-v${major}`;
      // Determine if it's a pro or free version for class naming.
      const typeClass = this.license === 'pro' ? 'faui-pro' : 'faui-free';
      // Check if Duotone is supported based on version and license.
      const supportsDuotone = this.version.startsWith('6') && major >= 6 && minor >= 7 && this.license === 'pro';

      // Return the basic modal structure with appropriate classes.
      return `
        <div class="faui-modal ${versionClass} ${versionMajor} ${typeClass} ${supportsDuotone ? 'extended-families' : ''}" id="faui-modal${this.settings.idSuffix}" style="display: none;">
          <div class="faui-modal--content"></div>
        </div>
      `;
    }

    /**
     * Constructs the modal header with close button and title.
     *
     * @return {string}
     *   HTML string for the modal header.
     */
    createModalHeader() {
      // Choose the correct close icon based on Font Awesome version.
      const closeIconClass = this.version.startsWith('6') ? 'fa-xmark' : 'fa-times';
      return `
        <header class="faui-modal--header">
          <div class="faui-modal--header-logo-area">
            <i class="far fa-folder-open"></i>
            <span class="faui-modal--header-logo-title">${this.settings.messages.icon_browser}</span>
          </div>
          <div class="faui-modal--header-close-btn">
            <i class="fas ${closeIconClass}"></i>
          </div>
        </header>
      `;
    }

    /**
     * Builds the modal body including sidebar, search, and icon display areas.
     *
     * @return {string}
     *   HTML string for the modal body.
     */
    createModalBody() {
      // Choose the correct search icon based on Font Awesome version.
      const searchIconClass = this.version.startsWith('6')
        ? 'fa-solid fa-magnifying-glass fa-flip-horizontal fa-2x fa-fw'
        : 'fas fa-search';
      // Check for Duotone support.
      const supportsDuotone = this.version.startsWith('6') && parseFloat(this.version) >= 6.7 && this.license === 'pro';

      // Define available font families based on version and license.
      const fontFamilies = [
        { family: 'classic', icon: 'fas fa-icons', label: 'Classic' },
        ...(supportsDuotone ? [{ family: 'duotone', icon: 'fa-duotone fa-icons', label: 'Duotone' }] : []),
        { family: 'sharp', icon: 'fa-sharp fa-solid fa-icons', label: 'Sharp' },
        ...(supportsDuotone ? [{ family: 'sharp-duotone', icon: 'fa-sharp-duotone fa-icons', label: 'Sharp Duotone' }] : []),
        { family: 'brands', icon: 'fab fa-font-awesome-flag', label: 'Brands' },
      ];

      // Generate buttons for each font family.
      const familyButtons = fontFamilies.map(family =>
        `<button class="icons-facet-tab" data-family="${family.family}">
            <i class="${family.icon} fa-fw fa-2x"></i>
            <span>${family.label}</span>
        </button>`
      ).join('');

      // Add a toggle for switching between Free and Pro icons if Pro license.
      const typeToggle = this.license === 'pro'
        ? `<div class="ais-ToggleRefinement">
            <button class="icons-facet-tab" data-type="free">
              <i class="fa fa-bolt fa-fw fa-2x"></i>
              <span>Free</span>
            </button>
          </div>`
        : '';

      // Return the constructed body HTML.
      return `
        <div class="faui-modal--body claro-details__wrapper--accordion-item">
          <div class="faui-modal--sidebar">
            <div class="faui-modal--sidebar-tabs"></div>
          </div>
          <div class="faui-modal--icon-preview-wrap list-scrollbar scrollbar-2x">
            <header class="search-sticky">
              <div class="faui-icon-families">
                <div class="icons-facets-group-families">
                  ${familyButtons}
                </div>
                ${typeToggle}
              </div>
              <div class="search-sticky__items${supportsDuotone ? '' : ' layout-container'}">
                <div class="search-sticky__items__inner faui-modal--icon-search">
                  <i class="${searchIconClass}"></i>
                  <input name="" value="" placeholder="${this.settings.messages.search_placeholder}">
                </div>
              </div>
            </header>
            <div class="faui-modal--icon-preview-inner">
              <div class="search-sticky-shadow search-sticky--border"></div>
              <div class="faui-stats">
                <div class="icons-results-summary display-flex flex-row flex-wrap">
                  <div class="icons-results-summary-count">
                    <h1 class="h4 text-nowrap">0 Icons</h1>
                  </div>
                  <div class="icons-results-summary-pages">
                    <p class="page-info muted text-nowrap">
                      <span class="sr-only">Currently on </span>
                      <span class="text-capitalize">page</span>
                      <span class="current-page">1</span> of <span class="total-pages">1</span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="faui-modal--icon-preview"></div>
            </div>
          </div>
        </div>
      `;
    }

    /**
     * Generates the modal footer with pagination and insert button.
     *
     * @return {string}
     *   HTML string for the modal footer.
     */
    createModalFooter() {
      // Construct footer with or without insert button based on settings.
      return `
        <footer class="faui-modal--footer">
          <span class="page-info"></span>
          <nav class="pagination pager" role="navigation" aria-labelledby="pagination-heading">
            <ul class="page-buttons pager__items js-pager__items ${!this.settings.clickInsert ? 'pager__items--insert' : ''}"></ul>
          </nav>
          ${!this.settings.clickInsert ? `<button class="faui-insert-icon-button button js-form-submit form-submit" disabled>${this.settings.messages.insert_label}</button>` : ''}
        </footer>
      `;
    }

    /**
     * Attaches event listeners for browser interactions.
     *
     * This method sets up all necessary event listeners for user interactions
     * within the modal, ensuring responsiveness and functionality.
     */
    attachEventListeners() {
      // Close modal when the close button is clicked.
      this.modalElement.find('.faui-modal--header-close-btn i').on('click', this.closeModal.bind(this));

      // Use debounce for search input to avoid excessive re-rendering.
      this.searchInput.on('input', this.debounce(this.handleSearch.bind(this), 300));

      // Handle sidebar tab interactions for style and category changes.
      this.sidebarTabs.on('click', '.faui-modal--sidebar-tab-item', (event) => {
        const $target = $(event.currentTarget);
        const libraryId = $target.data('library-id');
        const libraryStyle = $target.data('library-style');

        if (libraryStyle) {
          // Update active style and reflect in UI.
          this.activeStyle = libraryStyle;
          this.sidebarTabs.find('.faui-modal--sidebar-tab-item[data-library-style]')
            .removeClass('style-active');
          $target.addClass('style-active');
        } else {
          // Update active category and reflect in UI.
          this.activeCategory = libraryId;
          this.sidebarTabs.find('.faui-modal--sidebar-tab-item[data-library-id]')
            .removeClass('category-active');
          $target.addClass('category-active');
        }

        // Reset to the first page when filters change.
        this.currentPage = 1;
        this.renderIcons();
        this.updateSidebarCounts();
      });

      // Manage icon selection.
      this.iconContainer.on('click', '.faui-icon-item', (event) => {
        const $target = $(event.currentTarget);
        // Deselect all icons before selecting the new one.
        this.iconContainer.find('.faui-icon-item').removeClass('icon-active');
        $target.addClass('icon-active');
        this.settings.selectedIcon = $target.data('filter');

        if (this.settings.clickInsert) {
          // Insert icon directly if clickInsert is true.
          this.insertIcon();
        } else {
          // Enable/disable the insert button.
          this.updateInsertButtonState();
          // Rebind click handler to ensure it's the latest state.
          this.insertButton.off('click').on('click', this.insertIcon.bind(this));
        }
      });

      // Handle family and type selections.
      this.modalElement.find('.faui-icon-families').on('click', '.icons-facet-tab', (event) => {
        const $target = $(event.currentTarget);
        const family = $target.data('family');
        const type = $target.data('type');

        // Toggle active class for visual feedback.
        $target.toggleClass('facet-active');

        if (family) {
          this.activeFamily = family;
        }
        if (type) {
          this.activeType = this.activeType === type ? 'all' : type;
        }

        // Log changes for debugging or user feedback.
        console.log("Clicked Family:", family);
        console.log("Active Family after click:", this.activeFamily);

        // Reset pagination after filter change.
        this.currentPage = 1;
        this.renderIcons();
        this.updateSidebarCounts();
      });

      // Ensure DOM is fully loaded before adding scroll event.
      $(document).ready(() => {
        const $scrollMain = this.modalElement.find('.faui-modal--icon-preview-wrap');
        const $searchSticky = this.modalElement.find('.search-sticky');
        const $searchBorder = this.modalElement.find('.search-sticky-shadow');

        // Handle sticky header visibility on scroll.
        $scrollMain.on('scroll', () => {
          if ($scrollMain.scrollTop() >= 50) {
            $searchSticky.addClass('search-sticky--is-sticky');
            $searchBorder.removeClass('search-sticky--border');
          } else {
            $searchSticky.removeClass('search-sticky--is-sticky');
            $searchBorder.addClass('search-sticky--border');
          }
        });
      });
    }

    /**
     * Debounces a function to limit its invocation rate.
     *
     * Ensures the given function is executed only once after the specified
     * delay, even if called repeatedly in quick succession.
     *
     * @param {Function} func
     *   The function to debounce.
     * @param {number} wait
     *   The number of milliseconds to wait before invoking the function.
     *
     * @return {Function}
     *   A debounced version of the provided function.
     */
    debounce(func, wait) {
      let timeout;
      return function (...args) {
        const context = this;
        const later = () => {
          timeout = null;
          func.apply(context, args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
      };
    }

    /**
     * Opens the icon browser modal.
     *
     * Displays the modal and highlights the currently selected icon.
     */
    openModal() {
      this.modalElement.show();
      this.setIconActive(); // Highlight the currently selected icon.
    }

    /**
     * Closes the icon browser modal.
     *
     * Hides the modal element from view.
     */
    closeModal() {
      this.modalElement.hide();
    }

    /**
     * Handles the search input for filtering icons.
     *
     * Updates the search query, resets pagination to the first page,
     * and refreshes displayed icons and sidebar counts after a debounce delay.
     *
     * @param {Event} event
     *   The input event triggered by typing in the search field.
     */
    handleSearch(event) {
      // Sanitize input and convert to lowercase for case-insensitive search.
      let query = $(event.target).val().trim().toLowerCase();
      query = query.replace(/[^a-zA-Z0-9\s]/g, '');

      if (this.searchQuery !== query) {
        this.searchQuery = query;
        this.debounce(() => {
          this.currentPage = 1; // Reset to first page for new search.
          this.renderIcons();
          this.updateSidebarCounts();
        }, 300)(); // Invoke debounce with 300ms delay.
      }
    }

    /**
     * Filters icons based on active filters and search query.
     *
     * This method aggregates all icons from the source, applies various
     * filters based on user selections, and returns a filtered list of icons.
     *
     * @return {Array}
     *   A filtered array of icons matching the active filters and query.
     */
    filterIcons() {
      // Start with all icons filtered by sidebar selections.
      let icons = this.filterIconsBySidebar();

      // Apply additional style filter if a specific style is active.
      if (this.activeStyle !== 'all') {
        icons = icons.filter(icon => icon.style === this.activeStyle);
      }

      return icons;
    }

    /**
     * Renders the sidebar tabs for styles and categories.
     *
     * This method dynamically creates tabs for each style and category,
     * updating counts if enabled. It ensures optimal DOM manipulation by
     * appending elements in one go.
     */
    renderSidebarTabs() {
      // Clear previous tabs to avoid duplicates or outdated information.
      this.sidebarTabs.empty();

      // Total icon count for "All styles".
      const totalIcons = this.filterIcons().length;

      // Render style tabs.
      this.renderStyleTabs(totalIcons);

      // Render category tabs.
      this.renderCategoryTabs();
    }

    /**
     * Renders style tabs in the sidebar.
     *
     * @param {number} totalIcons
     *   The total number of icons for the "All styles" tab.
     */
    renderStyleTabs(totalIcons) {
      this.sidebarTabs.append(`
        <h3 class="h6-muted-text-uppercase">Style</h3>
        <ul class="icons-facets-group-styles"></ul>
      `);

      // Initialize the style group container for dynamic style appending.
      const $styleGroup = this.sidebarTabs.find('.icons-facets-group-styles');
      // Add "All styles" tab with total count.
      $styleGroup.append(`
        <li class="faui-modal--sidebar-tab-item style-active" data-library-style="all">
          <span style="flex: 1 1 auto;">
            <i class="fas fa-grid-2 fa-record-vinyl"></i>All styles
          </span>
          ${this.settings.showCount ? `<span class="icons-facet-count margin-left-sm">${totalIcons}</span>` : ''}
        </li>
      `);

      // Iterate over styles to create tabs for each one.
      this.getAllStyles().forEach(style => {
        const count = this.filterIcons().filter(icon => icon.style === style).length;
        // Only render tabs for styles with icons and exclude 'brands'.
        if (count > 0 && style !== 'brands') {
          const alias = this.getStyleAlias(`fa-${style}`);
          const name = style.charAt(0).toUpperCase() + style.slice(1);
          const icon = this.getStyleIcon(style);

          $styleGroup.append(`
            <li class="faui-modal--sidebar-tab-item" data-library-style="${style}">
              <span style="flex: 1 1 auto;">
                <i class="${alias} fa-${icon}"></i>${name}
              </span>
              ${this.settings.showCount ? `<span class="icons-facet-count margin-left-sm">${count}</span>` : ''}
            </li>
          `);
        }
      });
    }

    /**
     * Gets the appropriate icon for the style based on version and license.
     *
     * @param {string} style
     *   The style to get the icon for.
     * @return {string}
     *   The icon class suitable for the given style.
     */
    getStyleIcon(style) {
      // Define base icons for styles.
      const base = {
        'solid': 'circle',
        'regular': this.license === 'pro' ? 'adjust' : 'circle',
        'light': 'circle',
        'thin': 'circle',
        'duotone': 'adjust'
      };

      // Add version-specific overrides for Font Awesome 6.
      const overrides = this.version.startsWith('6') ? {
        'regular': this.license === 'pro' ? 'circle-half-stroke' : 'circle',
        'light': 'circle-half-stroke',
        'thin': 'circle-half-stroke',
        'duotone': 'circle-half'
      } : {};

      // Return the appropriate icon or default to 'circle'.
      return base[style] || overrides[style] || 'circle';
    }

    /**
     * Renders category tabs in the sidebar.
     */
    renderCategoryTabs() {
      this.sidebarTabs.append(`
        <h3 class="h6-muted-text-uppercase">Categories</h3>
        <div class="fa-ui--refinement-list list-scrollbar">
          <ul class="icons-facets-group-categories"></ul>
        </div>
      `);

      // Set up the categories container to dynamically append categories.
      const $categoryGroup = this.sidebarTabs.find('.icons-facets-group-categories');
      // Add "All Categories" tab.
      $categoryGroup.append(`
        <li class="faui-modal--sidebar-tab-item category-active" data-library-id="all_categories">
          <span class="icons-facet-affordance position-relative margin-right-sm">
            <i class="fas fa-layer-group fa-fw"></i>
          </span>
          <span style="flex: 1 1 auto;">All Categories</span>
        </li>
      `);

      const categoryIcons = this.getCategoryIcons();

      // Iterate through categories to create their tabs.
      Object.keys(this.source).forEach(category => {
        if (category !== 'Brands') {
          const count = this.filterIcons().filter(icon =>
            // Filter icons by category, family, and type.
            Object.keys(this.source[category]).includes(icon.name) &&
            (this.activeFamily === 'all' || icon.family === this.activeFamily) &&
            (this.activeType === 'all' || icon.type === this.activeType)
          ).length;

          const iconClass = categoryIcons[category] || 'fa-wand-magic-sparkles';

          $categoryGroup.append(`
            <li class="faui-modal--sidebar-tab-item" data-library-id="${category}">
              <span class="icons-facet-affordance position-relative margin-right-sm">
                <i class="fas ${iconClass} fa-fw"></i>
              </span>
              <span style="flex: 1 1 auto;">${category}</span>
              ${this.settings.showCount ? `<span class="icons-facet-count margin-left-sm">${count}</span>` : ''}
            </li>
          `);
        }
      });

      // Update counts if they are not shown.
      if (!this.settings.showCount) {
        this.updateSidebarCounts();
      }
    }

    /**
     * Gets category icons based on the current version of Font Awesome.
     *
     * @return {Object}
     *   A map of category names to their corresponding icon classes.
     */
    getCategoryIcons() {
      // Define the base category icons for all versions.
      let base = {
        'Accessibility' : 'fa-universal-access',
        'Alert' : 'fa-bell',
        'Alphabet' : 'fa-font',
        'Animals' : 'fa-paw',
        'Arrows': this.license === 'pro' ? 'fa-arrow-alt-square-up' : 'fa-caret-square-up',
        'Astronomy' : 'fa-meteor',
        'Audio & Video' : 'fa-play-circle',
        'Automotive' : 'fa-car',
        'Autumn': this.license === 'pro' ? 'fa-pumpkin' : 'fa-leaf',
        'Beverage' : 'fa-coffee',
        'Buildings' : 'fa-building',
        'Business' : 'fa-briefcase',
        'Camping' : 'fa-campground',
        'Charity' : this.license === 'pro' ? 'fa-hands-heart' : 'fa-hand-holding-heart',
        'Chat' : this.license === 'pro' ? 'fa-comments-alt' : 'fa-comments',
        'Chess' : 'fa-chess',
        'Charts + Diagrams' : 'fa-chart-line',
        'Childhood' : 'fa-ice-cream',
        'Clothing' : 'fa-tshirt',
        'Clothing + Fashion' : 'fa-shirt',
        'Code' : 'fa-code',
        'Coding' : 'fa-code',
        'Communication' : 'fa-comment-dots',
        'Computers' : this.license === 'pro' ? 'fa-computer-classic' : 'fa-desktop',
        'Connectivity' : 'fa-signal',
        'Construction' : this.license === 'pro' ? 'fa-digging' : 'fa-paint-roller',
        'Currency' : 'fa-money-bill',
        'Date & Time' : 'fa-clock',
        'Design' : 'fa-palette',
        'Devices + Hardware' : 'fa-computer',
        'Disaster + Crisis' : 'fa-burst',
        'Editing' : 'fa-pencil',
        'Editors' : 'fa-pen',
        'Education' : 'fa-graduation-cap',
        'Emoji' : 'fa-grin-wink',
        'Energy' : 'fa-bolt',
        'Files' : 'fa-file',
        'Film + Video' : 'fa-film',
        'Finance' : this.license === 'pro' ? 'fa-sack-dollar' : 'fa-chart-line',
        'Fitness' : this.license === 'pro' ? 'fa-watch-fitness' : 'fa-hiking',
        'Food' : this.license === 'pro' ? 'fa-cheeseburger' : 'fa-hamburger',
        'Food + Beverage' : 'fa-burger',
        'Fruits & Vegetables' : 'fa-apple-alt',
        'Fruits + Vegetables' : 'fa-apple-whole',
        'Games' : this.license === 'pro' ? 'fa-joystick' : 'fa-gamepad',
        'Gaming' : 'fa-gamepad',
        'Tabletop Gaming' : 'fa-dice-d20',
        'Genders' : 'fa-venus-mars',
        'Halloween' : 'fa-ghost',
        'Hands' : 'fa-hand-paper',
        'Health' : 'fa-heartbeat',
        'Holiday' : this.license === 'pro' ? 'fa-hat-santa' : 'fa-glass-cheers',
        'Holidays' : 'fa-champagne-glasses',
        'Hotel' : 'fa-spa',
        'Household' : 'fa-bath',
        'Humanitarian' : 'fa-parachute-box',
        'Images' : 'fa-images',
        'Interfaces' : 'fa-clipboard',
        'Logistics' : 'fa-shipping-fast',
        'Maps' : 'fa-map',
        'Maritime' : 'fa-anchor',
        'Marketing' : 'fa-bullseye',
        'Mathematics' : 'fa-calculator',
        'Media Playback' : 'fa-forward',
        'Medical' : 'fa-stethoscope',
        'Medical + Health' : 'fa-stethoscope',
        'Money' : 'fa-money-bill-wave',
        'Moving' : 'fa-dolly',
        'Music' : 'fa-music',
        'Music + Audio' : 'fa-music',
        'Nature' : 'fa-tree',
        'Numbers' : 'fa-9',
        'Objects' : 'fa-cut',
        'Payments & Shopping' : 'fa-receipt',
        'Pharmacy' : 'fa-prescription',
        'Photos + Images' : 'fa-image',
        'Political' : 'fa-check-double',
        'Punctuation + Symbols' : 'fa-quote-left',
        'Religion' : 'fa-pray',
        'Science' : 'fa-microscope',
        'Science Fiction' : 'fa-rocket',
        'Security' : this.license === 'pro' ? 'fa-shield' : 'fa-shield-alt',
        'Shapes' : 'fa-shapes',
        'Shopping' : 'fa-shopping-bag',
        'Social' : 'fa-hashtag',
        'Spinners' : 'fa-sync-alt',
        'Sports' : 'fa-running',
        'Sports + Fitness' : 'fa-heart-pulse',
        'Spring' : 'fa-seedling',
        'Status' : 'fa-signal',
        'Summer' : 'fa-sun',
        'Text Formatting' : 'fa-text-height',
        'Time' : 'fa-clock',
        'Toggle' : 'fa-toggle-on',
        'Transportation' : 'fa-truck-plane',
        'Travel' : 'fa-map-marked',
        'Travel + Hotel' : 'fa-plane-departure',
        'Users & People' : 'fa-users',
        'Users + People' : 'fa-children',
        'Vehicles' : this.license === 'pro' ? 'fa-car-bus' : 'fa-bus-alt',
        'Weather' : 'fa-cloud-sun',
        'Winter' :  this.license === 'pro' ? 'fa-snowflakes' : 'fa-snowplow',
        'Writing' : 'fa-keyboard',
        'Sponsors' : this.license === 'pro' ? 'fa-usd-circle' : 'fa-donate',
        'Uncategorized' : 'fa-question-circle',
      };

      // Define version 6 specific overrides for category icons.
      let overrides = {
        'Arrows': 'fa-square-up-right',
        'Charity' : 'fa-hands-holding-child',
        'Construction' : 'fa-person-digging',
        'Emoji' : 'fa-face-grin-wink',
        'Hands' : 'fa-hand',
        'Logistics' : 'fa-truck-fast',
        'Religion' : 'fa-person-praying',
        'Shopping' : 'fa-basket-shopping',
        'Spinners' : 'fa-arrows-rotate',
        'Sponsors' : 'fa-circle-dollar-to-slot',
        'Uncategorized' : 'fa-circle-question',
      };

      return this.version.startsWith('6') ? { ...base, ...overrides } : base;
    }

    /**
     * Renders the icons in the main display area.
     *
     * This method applies filters, then renders icons for the current page
     * while optimizing DOM manipulation by batch updating.
     */
    renderIcons() {
      const icons = this.filterIcons();
      this.iconContainer.empty();

      // Add "None" option if allowed and on the first page.
      if (this.allowEmpty && this.currentPage === 1) {
        this.iconContainer.append(`
          <div class="faui-icon-item" data-library-id="fa-regular" data-filter=""
            data-library-name="fontAwesome" data-font-style="">
            <div class="faui-icon-item-inner">
              <i class="fa-regular fa- faui-icon-none"> </i>
              <div class="faui-icon-item-name" title="None">None</div>
            </div>
          </div>
        `);
      }

      // Calculate the range of icons to display.
      const start = (this.currentPage - 1) * this.iconsPerPage;
      const end = start + this.iconsPerPage;
      const iconsToRender = icons.slice(start, end);

      // Create HTML for icons in batch to minimize DOM manipulation.
      const iconsHTML = iconsToRender.map(icon => {
        const classes = this.getStyleAlias(icon.class);
        const name = icon.name.replace(/^fa-/, '');
        return `
          <div class="faui-icon-item" data-library-id="${icon.style}"
            data-filter="${this.escapeHtml(classes)}" data-library-name="fontAwesome"
            data-font-style="${icon.style}">
            <div class="faui-icon-item-inner">
              <i class="${this.escapeHtml(classes)}"></i>
              <div class="faui-icon-item-name" title="${this.escapeHtml(name)}">
                ${this.escapeHtml(name)}
              </div>
            </div>
          </div>
        `;
      }).join('');

      this.iconContainer.append(iconsHTML);

      // Update pagination, button state, and stats after rendering icons.
      this.totalPages = Math.ceil(icons.length / this.iconsPerPage);
      this.updatePagination();
      this.updateInsertButtonState();
      this.updateStats(icons.length);
    }

    /**
     * Updates the selected icon in the UI.
     */
    updateSelectedIcon() {
      // Remove existing selected icon.
      this.$iconSelector.find('.selected-icon').remove();
      // Prepend the new selected icon.
      this.$iconSelector.find('.selector').prepend(`
        <span class="selected-icon">
          <i class="${this.settings.selectedIcon}"></i>
        </span>
      `);
      // Update input value and trigger change event.
      this.$input.val(this.settings.selectedIcon).trigger('change');
    }

    /**
     * Sets the active icon in the icon browser.
     */
    setIconActive() {
      if (this.settings.selectedIcon) {
        this.iconContainer.find('.faui-icon-item').each((index, item) => {
          const $item = $(item);
          // Check if the current icon matches the selected one.
          if ($item.data('filter') === this.settings.selectedIcon) {
            $item.addClass('icon-active');
          }
        });
      }
      // Update the insert button state after setting active icon.
      this.updateInsertButtonState();
    }

    /**
     * Updates the state of the insert button.
     */
    updateInsertButtonState() {
      // Check if any icon is currently selected.
      const isActive = this.iconContainer.find('.faui-icon-item.icon-active').length > 0;
      this.insertButton.prop('disabled', !isActive);
    }

    /**
     * Inserts the selected icon into the input field and closes the modal.
     */
    insertIcon() {
      // Find the active icon element.
      const $activeIcon = this.iconContainer.find('.faui-icon-item.icon-active');
      // Update settings with the selected icon's class.
      this.settings.selectedIcon = $activeIcon.data('filter');
      this.updateSelectedIcon();
      this.closeModal();
    }

    /**
     * Checks if the source includes Pro icons.
     *
     * @return {boolean}
     *   True if Pro icons are found, false otherwise.
     */
    checkForProIcons() {
      // Use some() for early return to optimize for large datasets.
      return Object.values(this.source).some(category =>
        Object.values(category).some(iconData =>
          iconData.icons.some(icon => icon.type === 'pro')
        )
      );
    }

    /**
     * Checks if the source includes Sharp icons.
     *
     * @return {boolean}
     *   True if Sharp icons are found, false otherwise.
     */
    checkForSharpIcons() {
      // Use some() for early return to optimize for large datasets.
      return Object.values(this.source).some(category =>
        Object.values(category).some(iconData =>
          iconData.icons.some(icon => icon.family.includes('sharp'))
        )
      );
    }

    /**
     * Toggles visibility of UI elements based on settings and availability.
     *
     * Hides elements related to Pro and Sharp icons if they are not available.
     */
    toggleElementsVisibility() {
      // Hide pro icon toggle if no pro icons are available.
      if (!this.hasProIcons) {
        this.modalElement.find('.ais-ToggleRefinement').hide();
      }
      // Hide sharp family buttons if no sharp icons are available.
      if (!this.hasSharpIcons) {
        this.modalElement.find('.icons-facets-group-families button[data-family="sharp"]').hide();
        this.modalElement.find('.icons-facets-group-families button[data-family="sharp-duotone"]').hide();
      }
    }

    /**
     * Navigates to the previous page in the icon browser.
     *
     * @param {Event} event
     *   The click event triggered by the user.
     */
    prevPage(event) {
      event.preventDefault();
      // Ensure we're not on the first page before decrementing.
      if (this.currentPage > 1) {
        this.currentPage--;
        this.renderIcons();
        this.updatePagination(); // Update pagination to reflect the new page state.
        this.updateStats(this.filterIconsBySidebar().length);
      }
    }

    /**
     * Navigates to the next page in the icon browser.
     *
     * @param {Event} event
     *   The click event triggered by the user.
     */
    nextPage(event) {
      event.preventDefault();
      // Ensure we're not on the last page before incrementing.
      if (this.currentPage < this.totalPages) {
        this.currentPage++;
        this.renderIcons();
        this.updatePagination(); // Update pagination to reflect the new page state.
        this.updateStats(this.filterIconsBySidebar().length);
      }
    }

    /**
     * Updates the pagination controls in the UI.
     */
    updatePagination() {
      // Clear previous pagination to prevent DOM clutter.
      const paginationContainer = this.modalElement.find('.pagination .page-buttons').empty();

      // Calculate pagination display logic.
      this.calculatePaginationDisplay(paginationContainer);

      // Add 'Previous' and 'Next' buttons.
      this.addNavigationButtons(paginationContainer);

      // Toggle visibility of navigation buttons based on current page.
      this.toggleNavigationButtons();

      // Bind click events after rendering pagination.
      this.bindPaginationEvents();
    }

    /**
     * Calculates the pagination display including ellipsis and page numbers.
     *
     * @param {jQuery} paginationContainer
     *   The jQuery object representing the pagination container.
     */
    calculatePaginationDisplay(paginationContainer) {
      const pagesToShow = 6; // Number of pagination links to display.
      const halfPagesToShow = Math.floor(pagesToShow / 2);

      // Determine the range of pages to display.
      let startPage = Math.max(1, this.currentPage - halfPagesToShow);
      let endPage = Math.min(this.totalPages, this.currentPage + halfPagesToShow);

      // Adjust the range if near the start or end of the total pages.
      if (this.currentPage <= halfPagesToShow) {
        endPage = Math.min(this.totalPages, pagesToShow);
      }
      if (this.currentPage + halfPagesToShow >= this.totalPages) {
        startPage = Math.max(1, this.totalPages - pagesToShow + 1);
      }

      // Update footer with current page information.
      this.modalElement.find('.faui-modal--footer .page-info')
        .text(`Page ${this.currentPage} of ${this.totalPages}`);

      // Add page numbers and ellipsis.
      this.addPageNumbersAndEllipsis(paginationContainer, startPage, endPage);
    }

    /**
     * Adds page numbers and ellipses to pagination.
     *
     * @param {jQuery} paginationContainer
     *   The jQuery object where pagination will be added.
     * @param {number} startPage
     *   The starting page number to display.
     * @param {number} endPage
     *   The ending page number to display.
     */
    addPageNumbersAndEllipsis(paginationContainer, startPage, endPage) {
      // Add first page and ellipsis if necessary.
      this.addFirstPageAndEllipsis(paginationContainer, startPage);

      // Add page numbers within the calculated range.
      for (let i = startPage; i <= endPage; i++) {
        const activeClass = i === this.currentPage ? 'pager__item--active' : '';
        paginationContainer.append(`
          <li class="pager__item ${activeClass} pager__item--number">
            <a href="#" class="number-page pager__link ${activeClass ? 'is-active' : ''}">${i}</a>
          </li>
        `);
      }

      // Add last page and ellipsis if necessary.
      this.addLastPageAndEllipsis(paginationContainer, endPage);
    }

    /**
     * Adds 'Previous' and 'Next' navigation buttons.
     *
     * @param {jQuery} paginationContainer
     *   The jQuery object where buttons will be added.
     */
    addNavigationButtons(paginationContainer) {
      paginationContainer.prepend(`
        <li class="pager__item pager__item--action pager__item--previous">
          <a href="#" title="Go to previous page" rel="prev"
             aria-label="Previous Page" class="prev-page pager__link
             pager__link--action-link">Prev</a>
        </li>
      `).append(`
        <li class="pager__item pager__item--action pager__item--next">
          <a href="#" title="Go to next page" rel="next"
             aria-label="Next Page" class="next-page pager__link
             pager__link--action-link">Next</a>
        </li>
      `);
    }

    /**
     * Adds first page and ellipsis if the start page isn't 1.
     *
     * @param {jQuery} paginationContainer
     *   The jQuery object where elements will be added.
     * @param {number} startPage
     *   The starting page number.
     */
    addFirstPageAndEllipsis(paginationContainer, startPage) {
      if (startPage > 1) {
        paginationContainer.append(`
          <li class="pager__item pager__item--number">
            <a href="#" class="number-page pager__link">1</a>
          </li>
        `);
        if (startPage > 2) {
          paginationContainer.append('<li class="pager__item pager__item--ellipsis">…</li>');
        }
      }
    }

    /**
     * Adds last page and ellipsis if the end page isn't the total pages.
     *
     * @param {jQuery} paginationContainer
     *   The jQuery object where elements will be added.
     * @param {number} endPage
     *   The ending page number.
     */
    addLastPageAndEllipsis(paginationContainer, endPage) {
      if (endPage < this.totalPages) {
        if (endPage < this.totalPages - 1) {
          paginationContainer.append('<li class="pager__item pager__item--ellipsis">…</li>');
        }
        paginationContainer.append(`
          <li class="pager__item pager__item--number">
            <a href="#" class="number-page pager__link">${this.totalPages}</a>
          </li>
        `);
      }
    }

    /**
     * // Toggle 'Prev'/'Next' visibility by current page.
     */
    toggleNavigationButtons() {
      this.modalElement.find('.pagination .prev-page').toggle(this.currentPage !== 1);
      this.modalElement.find('.pagination .next-page').toggle(this.currentPage !== this.totalPages);
    }

    /**
     * Binds click events for pagination controls.
     */
    bindPaginationEvents() {
      // Add event listeners to pagination buttons.
      this.modalElement.find('.pagination .page-buttons a.number-page').on('click', (event) => {
        event.preventDefault();
        this.currentPage = parseInt($(event.currentTarget).text());
        this.renderIcons();
        this.updatePagination(); // Update pagination to reflect the new page state.
        this.updateStats(this.filterIconsBySidebar().length);
      });

      // Add event listeners to "Previous" and "Next" buttons.
      this.modalElement.find('.pagination .prev-page').on('click', this.prevPage.bind(this));
      this.modalElement.find('.pagination .next-page').on('click', this.nextPage.bind(this));
    }

    /**
     * Updates the total icon count and page details in the UI.
     *
     * @param {number} totalIcons
     *   The total number of icons available after filtering.
     */
    updateStats(totalIcons) {
      // Use jQuery text method for better performance and security.
      this.statsElement.text(`${totalIcons} Icons`);
      this.currentPageElement.text(this.currentPage);
      this.totalPagesElement.text(this.totalPages);
    }

    /**
     * Updates counts and visibility of sidebar filters.
     *
     * This method updates sidebar filter counts, like styles and categories,
     * hiding filters with no matching icons dynamically.
     */
    updateSidebarCounts() {
      // Update the total icon count for "All styles".
      this.updateTotalIconCount();

      // Update counts and visibility for styles.
      this.updateStyleCounts();

      // Update counts and visibility for categories.
      this.updateCategoryCounts();
    }

    /**
     * Updates the total icon count for "All styles".
     */
    updateTotalIconCount() {
      const totalIcons = this.filterIcons().length;
      this.sidebarTabs
        .find('[data-library-style="all"] .icons-facet-count')
        .text(totalIcons)
        .show();
    }

    /**
     * Updates counts and visibility for styles.
     */
    updateStyleCounts() {
      const styles = this.getAllStyles();
      styles.forEach((style) => {
        // Count icons for the current style.
        const styleCount = this.filterIconsBySidebar()
          .filter((icon) => icon.style === style).length;

        const styleTab = this.sidebarTabs.find(`[data-library-style="${style}"]`);
        if (styleCount === 0 || style === 'brands') {
          // Hide tabs without matching or brand styles (handled separately).
          styleTab.hide();
        } else {
          styleTab.show();
          if (this.settings.showCount) {
            styleTab.find('.icons-facet-count').text(styleCount);
          }
        }
      });
    }

    /**
     * Updates counts and visibility for categories.
     */
    updateCategoryCounts() {
      Object.keys(this.source).forEach((category) => {
        if (category !== 'Brands') {
          // Count icons for the current category with applied filters.
          const categoryCount = this.filterIconsBySidebar()
            .filter((icon) => {
              return (
                Object.keys(this.source[category]).includes(icon.name) &&
                (this.activeFamily === 'all' || icon.family === this.activeFamily) &&
                (this.activeType === 'all' || icon.type === this.activeType)
              );
            }).length;

          const categoryTab = this.sidebarTabs.find(`[data-library-id="${category}"]`);
          if (categoryCount === 0) {
            // Hide tabs with no matching icons.
            categoryTab.hide();
          } else {
            categoryTab.show();
            if (this.settings.showCount) {
              categoryTab.find('.icons-facet-count').text(categoryCount);
            }
          }
        }
      });
    }

    /**
     * Filters icons based on active filters and search query.
     *
     * This method aggregates icons from the source, applies various filters
     * based on user selections, and returns a filtered list of icons.
     *
     * @return {Array}
     *   A filtered list of icons based on sidebar selections and search
     *   input.
     */
    filterIconsBySidebar() {
      // Step 1: Aggregate all icons into a flat list with metadata.
      const uniqueIconsMap = this.aggregateIcons();
      let icons = Array.from(uniqueIconsMap.values());

      // Apply all filters in one pass for better performance.
      return icons.filter((icon) => this.applyFilters(icon));
    }

    /**
     * Aggregates icons from the source data into a unique Map.
     *
     * @return {Map}
     *   A Map of unique icons with their metadata.
     */
    aggregateIcons() {
      const uniqueIconsMap = new Map();
      Object.keys(this.source).forEach((category) => {
        Object.entries(this.source[category]).forEach(([iconName, iconData]) => {
          iconData.icons.forEach((icon) => {
            // Generate a unique key using icon name, style, and family.
            const iconKey = `${iconName}-${icon.style}-${icon.family}`;

            // Check if the icon isn't already in the map to avoid duplicates.
            if (!uniqueIconsMap.has(iconKey)) {
              // Add the icon to the map with all relevant metadata.
              uniqueIconsMap.set(iconKey, {
                ...icon,
                name: iconName,
                label: iconData.label,
                search_terms: iconData.search_terms,
                free: iconData.free,
                category: category,
              });
            }
          });
        });
      });
      return uniqueIconsMap;
    }

    /**
     * Applies all relevant filters to an icon.
     *
     * @param {Object} icon
     *   The icon object to filter.
     *
     * @return {boolean}
     *   True if the icon passes all filter conditions, false otherwise.
     */
    applyFilters(icon) {
      // Check if the icon matches all current filter criteria.
      return this.filterByFamily(icon) &&
        this.filterByStyle(icon) &&
        this.filterByType(icon) &&
        this.filterByCategory(icon) &&
        this.filterBySearch(icon);
    }

    /**
     * Filters by family selection.
     *
     * @param {Object} icon
     *   The icon to check.
     *
     * @return {boolean}
     *   True if the icon matches the family filter, false otherwise.
     */
    filterByFamily(icon) {
      if (this.activeFamily === 'all') {
        return true;
      }

      // Gather all active families from the UI.
      const activeFamilies = this.modalElement
        .find('.icons-facet-tab.facet-active[data-family]')
        .map((_, elem) => $(elem).data('family'))
        .get();

      // Verify if the icon's family is active or no families are selected.
      return activeFamilies.length === 0 || activeFamilies.includes(icon.family);
    }

    /**
     * Filters by style selection.
     *
     * @param {Object} icon
     *   The icon to check.
     *
     * @return {boolean}
     *   True if the icon matches the style filter, false otherwise.
     */
    filterByStyle(icon) {
      if (this.activeStyle === 'all') {
        return true;
      }

      // Gather all active styles from the UI.
      const activeStyles = this.modalElement
        .find('.faui-modal--sidebar-tab-item.style-active[data-library-style]')
        .map((_, elem) => $(elem).data('library-style'))
        .get();

      // Verify if the icon's style is active or no styles are selected.
      return activeStyles.length === 0 || activeStyles.includes(icon.style);
    }

    /**
     * Filters by type (Free/Pro).
     *
     * @param {Object} icon
     *   The icon to check.
     *
     * @return {boolean}
     *   True if the icon matches the type filter, false otherwise.
     */
    filterByType(icon) {
      if (this.activeType === 'all') {
        return true;
      }

      if (this.activeType === 'free') {
        // For 'free', allow classic family with free style or a brand icon.
        return (icon.family === 'classic' && icon.free.includes(icon.style)) ||
          icon.style === 'brands';
      }

      // For 'pro', directly check if the icon type is 'pro'.
      return icon.type === this.activeType;
    }

    /**
     * Filters by category selection.
     *
     * @param {Object} icon
     *   The icon to check.
     *
     * @return {boolean}
     *   True if the icon matches the category filter, false otherwise.
     */
    filterByCategory(icon) {
      // Simply compare the icon's category with the active category selection.
      return this.activeCategory === 'all_categories' ||
        icon.category === this.activeCategory;
    }

    /**
     * Filters by search query.
     *
     * @param {Object} icon
     *   The icon to check.
     *
     * @return {boolean}
     *   True if the icon matches the search query, false otherwise.
     */
    filterBySearch(icon) {
      if (!this.searchQuery) {
        return true;
      }

      // Convert query and terms to lowercase for case-insensitive match.
      const query = this.searchQuery.toLowerCase();
      return icon.search_terms.toLowerCase().includes(query);
    }

    /**
     * Escapes HTML special characters to prevent XSS attacks.
     *
     * This function replaces special characters with their corresponding
     * HTML entities to ensure safe output in the browser.
     *
     * @param {string} unsafe
     *   The untrusted string that may contain HTML special characters.
     *
     * @returns {string}
     *   The escaped string with HTML entities.
     */
    escapeHtml(unsafe) {
      return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
    }

  }
})(jQuery);
