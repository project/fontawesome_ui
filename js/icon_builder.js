/**
 * @file
 * Contains definition of the behaviour Font Awesome UI icon builder form.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  const version = drupalSettings.fontAwesome.version;
  const license = drupalSettings.fontAwesome.license;
  const units = drupalSettings.fontAwesome.units;
  const colors = drupalSettings.fontAwesome.colors;

  Drupal.behaviors.fontAwesomeBuilderInit = {
    attach: function (context, settings) {

      if (once('icon-picker', '#edit-icon', context).length) {
        Drupal.fontAwesomeBuilderElementAdjustments(context);

        // Define and initialize the variables.
        const library_url = drupalSettings.fontAwesome.installed_path;
        const icon_source = drupalSettings.fontAwesome.icons_source;
        const icon_plugin = drupalSettings.fontAwesome.icon_plugin;
        const icon_sample = $('.fontawesome__sample', context);
        const defaults = drupalSettings.fontAwesome.defaults;

        const fields = [
          'icon', 'position', 'fixed_width', 'animation', 'delay', 'duration', 'direction', 'iteration',
          'timing', 'scale', 'opacity', 'rebound', 'height', 'start_scale_x', 'start_scale_y',
          'jump_scale_x', 'jump_scale_y', 'land_scale_x', 'land_scale_y', 'flip_x', 'flip_y',
          'flip_z', 'flip_angle', 'spin_reverse', 'border', 'border_color', 'border_width',
          'border_style', 'border_radius', 'border_padding', 'pull', 'pull_margin', 'color',
          'color_picker', 'size', 'flip', 'rotate', 'rotate_angle'
        ];

        let default_icon = $('.fontawesome__icon', context).val();

        if (icon_plugin === 'browser') {
          const auto_open = drupalSettings.fontAwesome.auto_open;
          const click_insert = drupalSettings.fontAwesome.click_insert;
          const show_counts = drupalSettings.fontAwesome.show_counts;
          const items_per_page = drupalSettings.fontAwesome.items_per_page;

          $('#edit-icon').fontAwesomeIconBrowser({
            source: icon_source,
            autoLoad: auto_open,
            clickInsert: click_insert,
            showCount: show_counts,
            itemsPerPage: items_per_page,
            selectedIcon: default_icon,
            messages: {
              icon_browser: 'Icon Browser',
              close_label: 'Close',
              search_placeholder: 'Search ...',
            },
            version: version,
            license: license,
          }).on('change', function () {
            let storeClasses = $('.fontawesome__classes', context)
              , selectedIcon = $(this).val();

            Drupal.fontAwesomeBuilderIcon(selectedIcon, icon_sample, library_url, storeClasses, fields, defaults);
          });
        }
        else {
          const has_search = drupalSettings.fontAwesome.has_search;
          const empty_icon = drupalSettings.fontAwesome.empty_icon;
          const auto_close = drupalSettings.fontAwesome.auto_close;
          const icons_per_page = drupalSettings.fontAwesome.icons_per_page;

          $('#edit-icon').fontAwesomeIconPicker({
            'source': icon_source,
            'hasSearch': has_search,
            'autoClose': auto_close,
            'emptyIcon': empty_icon,
            'iconsPerPage': icons_per_page,
            'defaultIcon': default_icon,
          }).on('change', function () {
            let storeClasses = $('.fontawesome__classes', context)
              , selectedIcon = $(this).val();

            if ( selectedIcon == '' ) {
              $('#edit-icon').val(default_icon);
              Drupal.fontAwesomeBuilderIcon(default_icon, icon_sample, library_url, storeClasses, fields, defaults);
            } else {
              Drupal.fontAwesomeBuilderIcon(selectedIcon, icon_sample, library_url, storeClasses, fields, defaults);
            }
          });
        }

        // Attach change events to form fields for rebuilding the icon.
        fields.forEach(function (field) {
          // Check if the current field should be skipped.
          if (field === 'icon') {
            return; // Skip the current iteration.
          }

          const id = `#edit-${field.replace(/_/g, '-')}`;
          const className = `.fontawesome__${field}`;

          $(once('fontAwesomeBuilderEvent', id, context)).on('change', function () {
            setTimeout(function () {
              Drupal.fontAwesomeBuilderRebuild(icon_sample, fields, defaults);
            }, 9);
          });

          $(once('fontAwesomeBuilderEvent', className, context)).on('change', function () {
            setTimeout(function () {
              Drupal.fontAwesomeBuilderRebuild(icon_sample, fields, defaults);
            }, 9);
          });
        });

        // Trigger the preview rebuild on clicking the replay button.
        $(once('fontawesome__replay', '.fontawesome__replay', context)).on('click', function (event) {
          $('#edit-icon').trigger('change');
          event.preventDefault();
        }).trigger('click');

        // Handle custom color selection.
        $('#edit-color input[type="radio"]').on('click', function () {
          let selectedColor = $(this).val();
          let $colorPicker = $('.form-item--color-picker');

          if (selectedColor === 'color-custom') {
            // Hide the custom color option list item.
            let customParent = $(this).closest('li');
            customParent.hide();

            // Show the color picker form item.
            $colorPicker.show();

            // Focus and click the color picker input to open it.
            $('#edit-color-picker').focus().click();
          } else {
            // Hide the color picker form item when another option is selected.
            $colorPicker.hide();

            // Show the custom color option if it exists.
            $('#edit-color input[type="radio"]').each(function () {
              if ($(this).val() === 'color-custom') {
                $(this).closest('li').show();
              }
            });
          }
        });

        // Handle custom rotation selection.
        $('#edit-rotate input[type="radio"]').on('click', function () {
          let selectedRotate = $(this).val();

          if (selectedRotate === 'rotate-by') {
            let customParent = $(this).closest('li');
            customParent.hide();
            $('#edit-rotate-angle').focus();
          } else {
            $('#edit-rotate input[type="radio"]').each(function () {
              if ($(this).val() === 'rotate-by') {
                $(this).closest('li').show();
              }
            });
          }
        });

        // Attach change event to flip radio buttons.
        $('input[name="flip"]').on('change', function () {
          const flipValue = $('input[name="flip"]:checked').val();
          const rotateValue = $('input[name="rotate"]:checked').val();

          // If flip is not 'none' and rotate is not 'none', set rotate to 'none'.
          if (flipValue !== 'none' && rotateValue !== 'none') {
            $('input[name="rotate"][value="none"]').prop('checked', true);
          }
        });

        // Attach change event to rotate radio buttons.
        $('input[name="rotate"]').on('change', function () {
          const flipValue = $('input[name="flip"]:checked').val();
          const rotateValue = $('input[name="rotate"]:checked').val();

          // If rotate is not 'none' and flip is not 'none', set it to 'none'.
          if (rotateValue !== 'none' && flipValue !== 'none') {
            $('input[name="flip"][value="none"]').prop('checked', true);
          }
        });

        // Handle border radius input and update the suffix.
        document.addEventListener('DOMContentLoaded', function () {
          const borderRadiusInput = document.querySelector('#edit-border-radius');
          const suffixSpan = document.querySelector('.js-form-item-border-radius .form-item__suffix');

          borderRadiusInput.addEventListener('input', function () {
            const value = borderRadiusInput.value;
            const pctRegex = /^\d+ *%$/;

            suffixSpan.textContent = pctRegex.test(value) ? 'pct' : 'px';
          });
        });
      }

    }
  };

  /**
   * Extracts and returns the basic information of a Font Awesome icon.
   *
   * @param {string} selectedIcon
   *   The class of the selected icon.
   *
   * @returns {object}
   *   An object containing the style, name, alias, and path of the icon.
   *   Returns an empty object if the selected icon does not match
   *   the expected pattern.
   */
  Drupal.fontAwesomeBuilderBasic = function (selectedIcon) {
    // Create a Map from styleAliases for faster and more efficient access.
    const styleAliases = new Map([
      ['fa-sharp fa-solid', 'fass'],
      ['fa-sharp fa-regular', 'fasr'],
      ['fa-sharp fa-light', 'fasl'],
      ['fa-sharp fa-thin', 'fast'],
      ['fa-sharp fa-duotone fa-solid', 'fasds'],
      ['fa-sharp-duotone fa-solid', 'fasds'],
      ['fa-solid', 'fas'],
      ['fa-regular', 'far'],
      ['fa-light', 'fal'],
      ['fa-thin', 'fat'],
      ['fa-duotone', 'fad'],
      ['fa-brands', 'fab']
    ]);

    // Create a reverse map to convert abbreviations back to full names.
    const reverseStyleAliases = new Map(Array.from(styleAliases).map(([full, short]) => [short, full]));

    // Split the selectedIcon into individual classes.
    const classes = selectedIcon.split(' ');

    // Replace abbreviations with full style names.
    const expandedClasses = classes.map(cls => reverseStyleAliases.get(cls) || cls);

    // Find the style class and icon class
    let style = '';
    let iconClass = '';

    // Determine the style and icon class by matching
    // against styleAliases and 'fa-' prefix.
    expandedClasses.forEach(cls => {
      if (styleAliases.has(cls)) {
        style = cls;
      } else if (cls.startsWith('fa-')) {
        iconClass = cls;
      }
    });

    // If we found both style and icon class,
    // map the style to its alias.
    if (style && iconClass) {
      const alias = styleAliases.get(style);

      // Build the style path by removing "fa-"
      // and replacing spaces with hyphens.
      let stylePath = style.replace(/fa-/g, '').replace(/\s+/g, '-');

      // Construct the SVG path.
      const svgPath = `${stylePath}/${iconClass.replace(/^fa-/, '')}.svg`;

      return {
        'style': stylePath,
        'name': iconClass.replace(/^fa-/, ''),
        'alias': alias,
        'path': svgPath,
      };
    }

    // Return an empty object if the pattern does not match.
    return {};
  };

  /**
   * Applies the selected Font Awesome icon and converts it to SVG for preview.
   *
   * @param {string} selectedIcon
   *   The class of the selected icon.
   * @param {object} iconToChange
   *   The object representing the element where the icon will be changed.
   * @param {string} library_url
   *   The URL of the Font Awesome icon library.
   * @param {object} storeClasses
   *   The object representing the input element to store the icon classes.
   * @param {array} fields
   *   The list of all icon field utilities.
   * @param {object} defaults
   *   The default values of the icon field utilities.
   */
  Drupal.fontAwesomeBuilderIcon = function (selectedIcon, iconToChange, library_url, storeClasses, fields, defaults) {
    // Ensure the selected icon is valid.
    if (!selectedIcon) {
      console.error('Invalid selected icon.');
      return;
    }

    // Get the basic information about the selected icon.
    let basic = Drupal.fontAwesomeBuilderBasic(selectedIcon);

    // Check if the basic information is valid.
    if (basic && Object.keys(basic).length > 0) {
      // Construct the svgPath based on the style and name.
      let svgPath = basic.path;

      // Check if the svgPath is valid.
      if (svgPath !== 'Invalid input') {
        /**
         * Load the SVG icon from the library URL.
         *
         * @param {object} iconToChange
         *   The object representing the element where the icon
         *   will be changed.
         * @param {string} library_url
         *   The URL of the Font Awesome icon library.
         * @param {string} svgPath
         *   The path to the SVG icon.
         * @param {object} basic
         *   The basic information about the selected icon.
         * @param {array} fields
         *   The list of all icon field utilities.
         * @param {object} defaults
         *   The default values of the icon field utilities.
         * @param {number} retries
         *   The number of retry attempts in case of failure.
         */
        async function loadSvgIcon(iconToChange, library_url, svgPath, basic, fields, defaults, retries = 3) {
          // Load the SVG icon.
          const loadSvg = () => {
            return new Promise((resolve, reject) => {
              iconToChange.load(library_url + svgPath, function (response, status, xhr) {
                if (status === "success") {
                  // Find the loaded SVG element.
                  const svgElement = iconToChange.find('svg');
                  if (svgElement.length) {
                    // Add the necessary classes to the SVG element.
                    svgElement.addClass(`svg-inline--fa fa-${basic.name}`);
                    // Set the required attributes for accessibility and Font Awesome compatibility.
                    svgElement.attr({
                      'aria-hidden': 'true',
                      'focusable': 'false',
                      'data-prefix': basic.alias,
                      'data-icon': basic.name,
                      'role': 'img'
                    });
                    resolve();
                  } else {
                    reject(new Error('SVG element not found.'));
                  }
                } else {
                  reject(new Error(`Failed to load SVG: ${xhr.statusText}`));
                }
              });
            });
          };

          // Attempt to load the SVG icon with retries.
          for (let i = 0; i < retries; i++) {
            try {
              await loadSvg();
              console.log('SVG loaded successfully.');
              Drupal.fontAwesomeBuilderRebuild(iconToChange, fields, defaults);
              break;
            } catch (error) {
              console.error(error.message);
              if (i < retries - 1) {
                console.log(`Retrying... (${i + 1}/${retries})`);
                await new Promise(resolve => setTimeout(resolve, 2000));
              } else {
                console.error('Failed to load SVG after several attempts.');
              }
            }
          }
        }

        // Call the function to load the SVG icon.
        loadSvgIcon(iconToChange, library_url, svgPath, basic, fields, defaults);

        // Add a success class and remove any error class from the icon container.
        iconToChange.addClass('text-primary').removeClass('text-danger');
        // Store the selected icon's class in the input element.
        storeClasses.val(selectedIcon);
      }
    } else {
      // Log an error if the basic information is invalid.
      console.error('Invalid basic icon information.');
    }
  };

  /**
   * Rebuilds the Font Awesome icon sample with the provided options.
   *
   * @param {string} icon_sample
   *   The CSS selector of the icon sample element.
   * @param {array} fields
   *   The list of all icon field utilities.
   * @param {object} defaults
   *   The default values of the icon field utilities.
   */
  Drupal.fontAwesomeBuilderRebuild = function (icon_sample, fields, defaults) {
    const options = fields.reduce((acc, field) => {
      const id = `#edit-${field.replace(/_/g, '-')}`;
      const className = `.fontawesome__${field}`;
      acc[field] = Drupal.fontAwesomeBuilderGetValue(id, className, defaults[field]);
      return acc;
    }, {});

    options.selector = icon_sample;

    Drupal.fontAwesomeBuilderPreview(options);
  };

  /**
   * Rebuilds the Font Awesome icon sample with the provided options.
   *
   * @param {object} options
   *   The options to customize the Font Awesome icon.
   */
  Drupal.fontAwesomeBuilderPreview = function (options) {
    // Define units for size and time.
    const sizeUnit = units.size_unit;
    const timeUnit = units.time_unit;

    // Find the SVG element within the selector.
    const svgSelector = options.selector.find('svg');

    // Extract basic icon information using the helper function.
    let basic = Drupal.fontAwesomeBuilderBasic(options.icon);

    // Initialize classes and styles arrays.
    let classes = ['svg-inline--fa', `fa-${basic.name}`];
    let styles = [];

    // Add class for fixed width if enabled.
    if (options.fixed_width) {
      classes.push('fa-fw');
    }

    // Add animation classes and styles if animation is not 'none'.
    if (options.animation !== 'none') {
      classes.push(options.animation);

      let delay = Number(options.delay);
      if (delay && delay !== 0) {
        styles.push(`--fa-animation-delay: ${options.delay}${timeUnit}`);
      }
      if (options.duration && options.duration !== 0) {
        styles.push(`--fa-animation-duration: ${options.duration}${timeUnit}`);
      }
      if (options.direction && options.direction !== 'normal') {
        styles.push(`--fa-animation-direction: ${options.direction}`);
      }
      if (options.timing && options.timing !== 'none') {
        styles.push(`--fa-animation-timing: ${options.timing}`);
      }
      if ((options.animation === 'fa-beat' || options.animation === 'fa-beat-fade') && options.scale && options.scale !== 0) {
        styles.push(`--${options.animation}-scale: ${options.scale}`);
      }
      if ((options.animation === 'fa-fade' || options.animation === 'fa-beat-fade') && options.opacity && options.opacity !== 0) {
        styles.push(`--${options.animation}-opacity: ${options.opacity}`);
      }
      if (options.animation === 'fa-bounce') {
        if (options.height && options.height !== 0) {
          styles.push(`--${options.animation}-height: ${options.height}`);
        }
        ['start_scale_x', 'start_scale_y', 'jump_scale_x', 'jump_scale_y', 'land_scale_x', 'land_scale_y'].forEach(scale => {
          if (options[scale] && options[scale] !== 0) {
            styles.push(`--${options.animation}-${scale.replace('_', '-')}: ${options[scale]}`);
          }
        });
      }
      if (options.animation === 'fa-flip') {
        ['flip_x', 'flip_y', 'flip_z', 'flip_angle'].forEach(flip => {
          if (options[flip] && options[flip] !== 0) {
            const unit = flip === 'flip_angle' ? 'deg' : '';
            styles.push(`--${options.animation}-${flip.replace('_', '-')}: ${options[flip]}${unit}`);
          }
        });
      }
      // Check if the animation is one of the spin types.
      if (['fa-spin', 'fa-spin-reverse', 'fa-spin-pulse'].includes(options.animation)) {
        // Add the base spin class.
        classes.push('fa-spin');

        // If the animation is spin-reverse or if spin_reverse option is true.
        if (options.animation === 'fa-spin-reverse' || options.spin_reverse) {
          // Add the spin-pulse class if the animation is spin-pulse.
          if (options.animation === 'fa-spin-pulse') {
            classes.push('fa-spin-pulse');
          }
          // Add the spin-reverse class.
          classes.push('fa-spin-reverse');
        }
      }
    }

    // Add border classes and styles if border is enabled.
    if (options.border) {
      classes.push('fa-border');

      if (options.border_color) {
        styles.push(`--fa-border-color: ${options.border_color}`);
      }
      if (options.border_width && options.border_width !== 0) {
        styles.push(`--fa-border-width: ${options.border_width}${sizeUnit}`);
      }
      if (options.border_style && options.border_style !== 'none') {
        styles.push(`--fa-border-style: ${options.border_style}`);
      } else {
        // Explicitly set border style to 'none' if specified.
        styles.push('border-style: none;');
      }
      if (options.border_radius && options.border_radius !== 0) {
        const pctRegex = /^\d+ *%$/;
        let radiusUnit = pctRegex.test(options.border_radius) ? '' : 'px';
        styles.push(`--fa-border-radius: ${options.border_radius}${radiusUnit}`);
      }
      if (options.border_padding && options.border_padding !== 0) {
        styles.push(`--fa-border-padding: ${options.border_padding}${sizeUnit}`);
      }
    }

    // Add pull classes and styles if pull is not 'none'.
    if (options.pull !== 'none') {
      classes.push(`fa-${options.pull}`);

      if (options.pull_margin !== 0 || options.pull_margin) {
        styles.push(`--fa-pull-margin: ${options.pull_margin}em`);
      }
    }

    // Add color styles if color is not 'none'.
    if (options.color !== 'none') {
      const color = options.color === 'color-custom' ? options.color_picker : colors[options.color];
      styles.push(`color: ${color}`);
    }

    // Add size class if size is not 'none'.
    if (options.size !== 'none') {
      classes.push(`fa-${options.size}`);
    }

    // Add flip class if flip is not 'none'.
    if (options.flip !== 'none') {
      classes.push(`fa-${options.flip}`);
    }

    // Add rotate classes and styles if rotate is not 'none'.
    if (options.rotate !== 'none') {
      if (options.rotate === 'rotate-by') {
        styles.push(`--fa-rotate-angle: ${options.rotate_angle}deg`);
        // Check if 'fa-rotate-by' is not already in the classes, then add it.
        if (!classes.includes('fa-rotate-by')) {
          classes.push('fa-rotate-by');
        }
      } else {
        // Check if 'fa-rotate-by' exists in the classes array and remove it.
        classes = classes.filter(cls => cls !== 'fa-rotate-by');
        classes.push(`fa-${options.rotate}`);
      }
    }

    // Clear existing classes on the SVG element.
    svgSelector.removeClass();

    // Add the new classes to the SVG element.
    svgSelector.addClass(classes.join(' '));

    // Add the new styles to the SVG element if any.
    if (styles.length > 0) {
      svgSelector.attr('style', styles.join('; '));
    }

    // Apply the color to the SVG paths if the color is not 'none'.
    if (options.color !== 'none' && svgSelector.is('svg')) {
      const fillColor = options.color === 'color-custom' ? options.color_picker : colors[options.color];
      svgSelector.find('path').attr('fill', fillColor);
    }
  };

  /**
   * Returns the value from either the primary selector,
   * secondary selector, or a default value.
   *
   * @param {string} primarySelector
   *   Primary element selector.
   * @param {string} secondarySelector.
   *   Secondary element selector.
   * @param {string} defaultValue.
   *   Default value if neither selector has a value.
   *
   * @return {string}
   *   The value from the primary or secondary selector, or the default value.
   */
  Drupal.fontAwesomeBuilderGetValue = function (primarySelector, secondarySelector, defaultValue) {
    let value;

    if ($(primarySelector).find('input[type="radio"]').length > 0) {
      // For radio buttons inside a list.
      value = $(primarySelector).find('input[type="radio"]:checked').val();
    } else if ($(primarySelector).is('input[type="checkbox"]')) {
      // For checkboxes.
      value = $(primarySelector).is(':checked');
    } else {
      // For other input types (text, number, color) and select elements.
      value = $(primarySelector).val();
    }

    return value;
  };

  /**
   * Adjusts form elements by moving specific elements and updating attributes.
   *
   * @param {object} context
   *   The context within which the elements are adjusted.
   */
  Drupal.fontAwesomeBuilderElementAdjustments = function (context) {
    // Process forms with 'ginEditForm' once.
    once('ginEditForm', '.region-content form', context).forEach(form => {
      const sticky = context.querySelector('.gin-sticky');
      const newParent = context.querySelector('.region-sticky__items__inner');

      // Move the 'gin-sticky' element to the new
      // parent if it doesn't already exist there.
      if (sticky !== null && newParent && newParent.querySelectorAll('.gin-sticky').length === 0) {
        newParent.appendChild(sticky);

        // Attach form elements to the main form.
        const actionButtons = newParent.querySelectorAll('button, input, select, textarea');

        // Update the 'form' attribute and 'id' of each form element.
        if (actionButtons.length > 0) {
          actionButtons.forEach((el) => {
            el.setAttribute('form', form.getAttribute('id'));
            el.setAttribute('id', el.getAttribute('id') + '--gin-edit-form');
          });
        }
      }
    });

    // Transfer custom color input from a div to a list item within a form.
    once('customColorTransfer', '#edit-color--wrapper', context);

    // Find the custom color div.
    const customColorDiv = context.querySelector('.form-item--color-picker');

    $(customColorDiv).hide();

    // Find the color form list (ul).
    const colorFormList = context.querySelector('#edit-color');

    // If both the custom color div and the color
    // form list exist, proceed with the transfer.
    if (customColorDiv !== null && colorFormList !== null) {
      // Create a new li element.
      const customColorLi = document.createElement('li');
      // Get the class and style attributes, handling potential null values.
      const className = customColorDiv.getAttribute('class') || '';
      const style = customColorDiv.getAttribute('style') || '';

      customColorLi.className = className;
      customColorLi.style = style;

      // Transfer the input and label from the div to the new li.
      const customColorInput = customColorDiv.querySelector('input');
      const customColorLabel = customColorDiv.querySelector('label');

      // Append the input and label to the new li if they exist.
      if (customColorInput !== null && customColorLabel !== null) {
        customColorLi.appendChild(customColorInput);
        customColorLi.appendChild(customColorLabel);

        // Append the new li to the ul.
        colorFormList.appendChild(customColorLi);

        // Remove the original custom color div.
        customColorDiv.remove();
      } else {
        console.error('Custom color input or label not found.');
      }
    } else {
      console.error('Custom color div or color form list not found.');
    }

    // Transfer custom rotate input from a div to a list item within a form.
    once('customRotateTransfer', '#edit-rotate--wrapper', context);

    // Find the custom rotate div.
    const customRotateDiv = context.querySelector('.form-item--rotate-angle');

    // Find the rotate form list (ul).
    const rotateFormList = context.querySelector('#edit-rotate');

    // If both the custom rotate div and the rotate
    // form list exist, proceed with the transfer.
    if (customRotateDiv !== null && rotateFormList !== null) {
      // Create a new li element.
      const customRotateLi = document.createElement('li');
      // Get the class and style attributes, handling potential null values.
      const classNameRotate = customRotateDiv.getAttribute('class') || '';
      const styleRotate = customRotateDiv.getAttribute('style') || '';

      customRotateLi.className = classNameRotate;
      customRotateLi.style = styleRotate;

      // Transfer the input and label from the div to the new li.
      const customRotateInput = customRotateDiv.querySelector('input');
      const customRotateLabel = customRotateDiv.querySelector('label');

      // Append the input and label to the new li if they exist.
      if (customRotateInput !== null && customRotateLabel !== null) {
        customRotateLi.appendChild(customRotateInput);
        customRotateLi.appendChild(customRotateLabel);

        // Append the new li to the ul.
        rotateFormList.appendChild(customRotateLi);

        // Remove the original custom rotate div.
        customRotateDiv.remove();
      } else {
        console.error('Custom rotate input or label not found.');
      }
    } else {
      console.error('Custom rotate div or rotate form list not found.');
    }
  };

})(jQuery, Drupal, drupalSettings, once);
