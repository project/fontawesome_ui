/**
 *  jQuery Font Awesome UI - Icon Picker.
 *
 *  An icon picker built on top of Font Awesome icons and jQuery.
 *  This plugin is specially provided for the Drupal Font Awesome UI module
 *  and has been rewritten and adapted to the Font Awesome 6 library.
 *
 *  https://www.drupal.org/project/fontawesome_ui
 *
 *  Made by Alessandro Benoit & Swashata.
 *  Refactoring and Rewritten by Mahyar Sabeti.
 *
 * {@link https://www.drupal.org/u/mahyarsbt}
 *
 *  Licensed under MIT License.
 */

(function (global, factory) {
  // Check the environment and define the module accordingly.
  typeof exports === 'object' && typeof module !== 'undefined' ?
    module.exports = factory(require('jquery')) :
    typeof define === 'function' && define.amd ?
      define(['jquery'], factory) :
      (global.initFontAwesomeIconPickerNode = factory(global.jQuery));
}(this, (function (jQuery) { 'use strict';

  // Ensure jQuery is defined properly.
  jQuery = jQuery && jQuery.hasOwnProperty('default') ? jQuery['default'] : jQuery;

  /**
   * Check the type of an object.
   *
   * @param {Object} obj - The object to check.
   * @return {string} - The type of the object.
   */
  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }
    return _typeof(obj);
  }

  /**
   * Convert an array to a consumable array.
   *
   * @param {Array} arr - The array to convert.
   * @return {Array} - The converted array.
   */
  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  /**
   * Handle arrays without holes.
   *
   * @param {Array} arr - The array to handle.
   * @return {Array} - The handled array.
   */
  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++)
        arr2[i] = arr[i];
      return arr2;
    }
  }

  /**
   * Handle iterable objects.
   *
   * @param {Iterable} iter - The iterable object.
   * @return {Array} - The converted array.
   */
  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) ||
      Object.prototype.toString.call(iter) === "[object Arguments]") {
      return Array.from(iter);
    }
  }

  /**
   * Handle non-iterable spread.
   */
  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  /**
   * Default configuration options of fontAwesomeIconPicker.
   */
  var options = {
    theme: 'faui-grey', // The CSS theme to use with this fontAwesomeIconPicker.
    source: false, // Icons source (array|false|object).
    defaultIcon: 'fa-solid fa-fontawesome', // The value of the default icon.
    emptyIcon: true, // Empty icon should be shown?
    emptyIconValue: '', // The value of the empty icon.
    autoClose: true, // Whether to close the icon picker automatically when clicked outside.
    iconsPerPage: 20, // Number of icons per page.
    hasSearch: true, // Is search enabled?
    searchSource: false, // Manual search values.
    appendTo: 'self', // Where to append the selector popup.
    useAttribute: false, // Use attribute selector for printing icons.
    attributeName: 'data-icon', // HTML Attribute name.
    convertToHex: true, // Convert to hexadecimal for attribute value.
    allCategoryText: 'From all categories', // Text for the select all category option.
    unCategorizedText: 'Uncategorized', // Text for the select uncategorized option.
    iconGenerator: null, // Icon Generator function.
    windowDebounceDelay: 150, // Debounce delay while fixing position on windowResize.
    searchPlaceholder: 'Search Icons' // Placeholder for the search input.
  };

  /**
   * Implementation of debounce function.
   *
   * @param {Function} func - Callback function.
   * @param {int} delay - Delay in milliseconds.
   * @return {Function} - Debounced function.
   */
  var debounce = function debounce(func, delay) {
    var inDebounce;
    return function () {
      var context = this;
      var args = arguments;
      clearTimeout(inDebounce);
      inDebounce = setTimeout(function () {
        return func.apply(context, args);
      }, delay);
    };
  };

  var $ = jQuery; // A guid for implementing namespaced event.

  var guid = 0;

  /**
   * FontAwesomeIconPicker Constructor.
   *
   * @param {HTMLElement} element - The target element.
   * @param {Object} options$$1 - Custom options.
   */
  function FontAwesomeIconPicker(element, options$$1) {
    this.element = $(element);
    this.settings = $.extend({}, options, options$$1);

    if (this.settings.emptyIcon) {
      this.settings.iconsPerPage--;
    }

    this.iconPicker = $('<div/>', {
      class: 'icons-selector',
      style: 'position: relative',
      html: this._getPickerTemplate(),
      attr: {
        'data-faui-origin': this.element.attr('id')
      }
    });
    this.iconContainer = this.iconPicker.find('.faui-icons-container');
    this.searchIcon = this.iconPicker.find('.selector-search i');
    this.selectorPopup = this.iconPicker.find('.selector-popup-wrap');
    this.selectorButton = this.iconPicker.find('.selector');
    this.iconsSearched = [];
    this.isSearch = false;
    this.totalPage = 1;
    this.currentPage = 1;
    this.currentIcon = false;
    this.iconsCount = 0;
    this.open = false;
    this.guid = guid++;
    this.eventNameSpace = ".fontAwesomeIconPicker".concat(guid);
    this.searchValues = [];
    this.availableCategoriesSearch = [];
    this.triggerEvent = null;
    this.backupSource = [];
    this.backupSearch = [];
    this.isCategorized = false;
    this.selectCategory = this.iconPicker.find('.icon-category-select');
    this.selectedCategory = false;
    this.availableCategories = [];
    this.unCategorizedKey = null;
    this.init();
  }

  FontAwesomeIconPicker.prototype = {
    /**
     * Initialize the icon picker.
     */
    init: function init() {
      this.iconPicker.addClass(this.settings.theme);
      this.iconPicker.css({
        left: -9999
      }).appendTo('body');
      var iconPickerHeight = this.iconPicker.outerHeight(),
        iconPickerWidth = this.iconPicker.outerWidth();
      this.iconPicker.css({
        left: ''
      });
      this.element.before(this.iconPicker);
      this.element.css({
        visibility: 'hidden',
        top: 0,
        position: 'relative',
        zIndex: '-1',
        left: '-' + iconPickerWidth + 'px',
        display: 'inline-block',
        height: iconPickerHeight + 'px',
        width: iconPickerWidth + 'px',
        padding: '0',
        margin: '0 -' + iconPickerWidth + 'px 0 0',
        border: '0 none',
        verticalAlign: 'top',
        float: 'none'
      });
      if (!this.element.is('select')) {
        this.triggerEvent = 'input';
      }
      if (!this.settings.source && this.element.is('select')) {
        this._populateSourceFromSelect();
      } else {
        this._initSourceIndex();
      }
      this._loadCategories();
      this._loadIcons();
      this._initDropDown();
      this._initCategoryChanger();
      this._initPagination();
      this._initIconSearch();
      this._initIconSelect();
      this._initAutoClose();
      this._initFixOnResize();
    },

    /**
     * Set icons after the Font Awesome icon picker has been initialized.
     *
     * @param {Array|Object} newIcons - New icons to set.
     */
    setIcons: function setIcons(newIcons) {
      this.settings.source = Array.isArray(newIcons) ? _toConsumableArray(newIcons) : $.extend({}, newIcons);

      this._initSourceIndex();
      this._loadCategories();
      this._resetSearch();
      this._loadIcons();
    },

    /**
     * Set currently selected icon programmatically.
     *
     * @param {string} [theIcon=''] - Current icon value.
     */
    setIcon: function setIcon() {
      var theIcon = arguments.length > 0 && arguments[0] !== undefined ?
        arguments[0] : '';
      this._setSelectedIcon(theIcon);
    },

    /**
     * Destroy picker and all events.
     */
    destroy: function destroy() {
      this.iconPicker.off().remove();
      this.element.css({
        visibility: '',
        top: '',
        position: '',
        zIndex: '',
        left: '',
        display: '',
        height: '',
        width: '',
        padding: '',
        margin: '',
        border: '',
        verticalAlign: '',
        float: ''
      });
      $(window).off('resize' + this.eventNameSpace);
      $('html').off('click' + this.eventNameSpace);
    },

    /**
     * Manually reset position.
     */
    resetPosition: function resetPosition() {
      this._fixOnResize();
    },

    /**
     * Manually set page.
     *
     * @param {int|string} pageNum - Page number to set.
     */
    setPage: function setPage(pageNum) {
      if ('first' === pageNum) {
        pageNum = 1;
      }
      if ('last' === pageNum) {
        pageNum = this.totalPage;
      }
      pageNum = parseInt(pageNum, 10);
      if (isNaN(pageNum)) {
        pageNum = 1;
      }
      if (pageNum > this.totalPage) {
        pageNum = this.totalPage;
      }
      if (1 > pageNum) {
        pageNum = 1;
      }
      this.currentPage = pageNum;
      this._renderIconContainer();
    },

    /**
     * Initialize fix on window resize with debouncing.
     */
    _initFixOnResize: function _initFixOnResize() {
      var _this = this;
      $(window).on('resize' + this.eventNameSpace, debounce(function () {
        _this._fixOnResize();
      }, this.settings.windowDebounceDelay));
    },

    /**
     * Initiate auto closing.
     */
    _initAutoClose: function _initAutoClose() {
      var _this2 = this;
      if (this.settings.autoClose) {
        $('html').on('click' + this.eventNameSpace, function (event) {
          var target = event.target;
          if (_this2.selectorPopup.has(target).length ||
            _this2.selectorPopup.is(target) ||
            _this2.iconPicker.has(target).length ||
            _this2.iconPicker.is(target)) {
            return;
          }
          if (_this2.open) {
            _this2._toggleIconSelector();
          }
        });
      }
    },

    /**
     * Select icon.
     */
    _initIconSelect: function _initIconSelect() {
      var _this3 = this;
      this.selectorPopup.on('click', '.faui-box', function (e) {
        var fauiBox = $(e.currentTarget);
        _this3._setSelectedIcon(fauiBox.attr('data-faui-value'));
        _this3._toggleIconSelector();
      });
    },

    /**
     * Initiate real-time icon search.
     */
    _initIconSearch: function _initIconSearch() {
      var _this4 = this;
      this.selectorPopup.on('input', '.icons-search-input', function (e) {
        var searchString = $(e.currentTarget).val();
        if ('' === searchString) {
          _this4._resetSearch();
          return;
        }
        _this4.searchIcon.removeClass('faui-icon-search');
        _this4.searchIcon.addClass('faui-icon-cancel');
        _this4.isSearch = true;
        _this4.currentPage = 1;
        _this4.iconsSearched = [];
        $.grep(_this4.searchValues, function (n, i) {
          if (0 <= n.toLowerCase().search(searchString.toLowerCase())) {
            _this4.iconsSearched[_this4.iconsSearched.length] =
              _this4.settings.source[i];
            return true;
          }
        });
        _this4._renderIconContainer();
      });
      this.selectorPopup.on('click', '.selector-search .faui-icon-cancel', function () {
        _this4.selectorPopup.find('.icons-search-input').focus();
        _this4._resetSearch();
      });
    },

    /**
     * Initiate pagination.
     */
    _initPagination: function _initPagination() {
      var _this5 = this;
      this.selectorPopup.on('click', '.selector-arrow-right', function (e) {
        if (_this5.currentPage < _this5.totalPage) {
          _this5.currentPage = _this5.currentPage + 1;
          _this5._renderIconContainer();
        }
      });
      this.selectorPopup.on('click', '.selector-arrow-left', function (e) {
        if (1 < _this5.currentPage) {
          _this5.currentPage = _this5.currentPage - 1;
          _this5._renderIconContainer();
        }
      });
    },

    /**
     * Initialize category changer dropdown.
     */
    _initCategoryChanger: function _initCategoryChanger() {
      var _this6 = this;
      this.selectorPopup.on('change keyup', '.icon-category-select', function (e) {
        if (false === _this6.isCategorized) {
          return false;
        }
        var targetSelect = $(e.currentTarget),
          currentCategory = targetSelect.val();
        if ('all' === targetSelect.val()) {
          _this6.settings.source = _this6.backupSource;
          _this6.searchValues = _this6.backupSearch;
        } else {
          var key = parseInt(currentCategory, 10);
          if (_this6.availableCategories[key]) {
            _this6.settings.source = _this6.availableCategories[key];
            _this6.searchValues = _this6.availableCategoriesSearch[key];
          }
        }
        _this6._resetSearch();
        _this6._loadIcons();
      });
    },

    /**
     * Initialize dropdown button.
     */
    _initDropDown: function _initDropDown() {
      var _this7 = this;
      this.selectorButton.on('click', function (event) {
        _this7._toggleIconSelector();
      });
    },

    /**
     * Get icon picker template string.
     *
     * @return {string} - The picker template.
     */
    _getPickerTemplate: function _getPickerTemplate() {
      let pickerTemplate =
        `<div class="selector" data-faui-origin="${this.element.attr('id')}">
          <span class="selected-icon">
            <i class="faui-icon-block"></i>
          </span>
          <span class="selector-button">
            <i class="faui-icon-down-dir"></i>
          </span>
        </div>
        <div class="selector-popup-wrap" data-faui-origin="${this.element.attr('id')}">
          <div class="selector-popup" style="display: none;">
            ${this.settings.hasSearch ? `<div class="selector-search">
              <input type="text" name="" value="" placeholder="${this.settings.searchPlaceholder}" class="icons-search-input"/>
              <i class="faui-icon-search"></i>
            </div>` : ''}
            <div class="selector-category">
              <select name="" class="icon-category-select" style="display: none"></select>
            </div>
            <div class="faui-icons-container"></div>
            <div class="selector-footer" style="display:none;">
              <span class="selector-pages">1/2</span>
              <span class="selector-arrows">
                <span class="selector-arrow-left" style="display:none;">
                  <i class="faui-icon-left-dir"></i>
                </span>
                <span class="selector-arrow-right">
                  <i class="faui-icon-right-dir"></i>
                </span>
              </span>
            </div>
          </div>
        </div>`;

      return pickerTemplate;
    },

    /**
     * Initialize the source & search index from the current settings.
     */
    _initSourceIndex: function _initSourceIndex() {
      if (typeof this.settings.source !== 'object') {
        return;
      }

      let allIcons = [];
      let allSearchValues = [];

      for (let category in this.settings.source) {
        for (let iconKey in this.settings.source[category]) {
          let iconData = this.settings.source[category][iconKey];
          allIcons = allIcons.concat(iconData.icons.map(icon => icon.class));
          allSearchValues = allSearchValues.concat(iconData.search_terms.split(' '));
        }
      }

      const uniqueIcons = [...new Set(allIcons)];
      const uniqueSearchValues = uniqueIcons.map(icon => {
        const index = allIcons.indexOf(icon);
        return allSearchValues[index];
      });

      this.uniqueIconsSource = uniqueIcons;
      this.uniqueSearchSource = uniqueSearchValues;

      if (Array.isArray(this.settings.source)) {
        this.isCategorized = false;
        this.selectCategory.html('').hide();
        this.settings.source = $.map(this.settings.source, function (e) {
          return e.toString();
        });
        this.searchValues = this.settings.source.slice(0);
      } else {
        let originalSource = $.extend(true, {}, this.settings.source);
        this.settings.source = [];
        this.searchValues = [];
        this.availableCategoriesSearch = [];
        this.selectedCategory = false;
        this.availableCategories = [];
        this.unCategorizedKey = null;
        this.isCategorized = true;
        this.selectCategory.html('');
        for (let categoryLabel in originalSource) {
          let thisCategoryKey = this.availableCategories.length;
          let categoryOption = $('<option />');
          categoryOption.attr('value', thisCategoryKey);
          categoryOption.html(categoryLabel);
          this.selectCategory.append(categoryOption);
          this.availableCategories[thisCategoryKey] = [];
          this.availableCategoriesSearch[thisCategoryKey] = [];
          for (let iconKey in originalSource[categoryLabel]) {
            let iconData = originalSource[categoryLabel][iconKey];
            iconData.icons.forEach(icon => {
              let newIconValue = icon.class;
              let newIconLabel = iconData.search_terms;
              if (typeof newIconValue.toString === 'function') {
                newIconValue = newIconValue.toString();
              }
              if (newIconValue && newIconValue !== this.settings.emptyIconValue) {
                this.settings.source.push(newIconValue);
                this.availableCategories[thisCategoryKey].push(newIconValue);
                this.searchValues.push(newIconLabel);
                this.availableCategoriesSearch[thisCategoryKey].push(newIconLabel);
              }
            });
          }
        }
      }
      this.backupSource = Array.isArray(this.settings.source) ? this.settings.source.slice(0) : $.extend(true, {}, this.settings.source);
      this.backupSearch = this.searchValues.slice(0);
    },

    /**
     * Populate source from select element.
     */
    _populateSourceFromSelect: function _populateSourceFromSelect() {
      var _this8 = this;
      this.settings.source = [];
      this.settings.searchSource = [];
      if (this.element.find('optgroup').length) {
        this.isCategorized = true;
        this.element.find('optgroup').each(function (i, el) {
          var thisCategoryKey = _this8.availableCategories.length,
            categoryOption = $('<option />');
          categoryOption.attr('value', thisCategoryKey);
          categoryOption.html($(el).attr('label'));
          _this8.selectCategory.append(categoryOption);
          _this8.availableCategories[thisCategoryKey] = [];
          _this8.availableCategoriesSearch[thisCategoryKey] = [];
          $(el).find('option').each(function (i, cel) {
            var newIconValue = $(cel).val(),
              newIconLabel = $(cel).html();
            if (newIconValue && newIconValue !== _this8.settings.emptyIconValue) {
              _this8.settings.source.push(newIconValue);
              _this8.availableCategories[thisCategoryKey].push(newIconValue);
              _this8.searchValues.push(newIconLabel);
              _this8.availableCategoriesSearch[thisCategoryKey].push(newIconLabel);
            }
          });
        });
        if (this.element.find('> option').length) {
          this.element.find('> option').each(function (i, el) {
            var newIconValue = $(el).val(),
              newIconLabel = $(el).html();
            if (!newIconValue || '' === newIconValue ||
              newIconValue == _this8.settings.emptyIconValue) {
              return true;
            }
            if (null === _this8.unCategorizedKey) {
              _this8.unCategorizedKey = _this8.availableCategories.length;
              _this8.availableCategories[_this8.unCategorizedKey] = [];
              _this8.availableCategoriesSearch[_this8.unCategorizedKey] = [];
              $('<option />').attr('value', _this8.unCategorizedKey)
                .html(_this8.settings.unCategorizedText).appendTo(_this8.selectCategory);
            }
            _this8.settings.source.push(newIconValue);
            _this8.availableCategories[_this8.unCategorizedKey].push(newIconValue);
            _this8.searchValues.push(newIconLabel);
            _this8.availableCategoriesSearch[_this8.unCategorizedKey].push(newIconLabel);
          });
        }
      } else {
        this.element.find('option').each(function (i, el) {
          var newIconValue = $(el).val(),
            newIconLabel = $(el).html();
          if (newIconValue) {
            _this8.settings.source.push(newIconValue);
            _this8.searchValues.push(newIconLabel);
          }
        });
      }
      this.backupSource = this.settings.source.slice(0);
      this.backupSearch = this.searchValues.slice(0);
    },

    /**
     * Load categories.
     */
    _loadCategories: function _loadCategories() {
      if (false === this.isCategorized) {
        return;
      }
      $('<option value="all">' + this.settings.allCategoryText + '</option>')
        .prependTo(this.selectCategory);
      this.selectCategory.show().val('all').trigger('change');
    },

    /**
     * Load icons.
     */
    _loadIcons: function _loadIcons() {
      this.iconContainer.html('<i class="faui-icon-spin3 animate-spin loading"></i>');
      if (Array.isArray(this.settings.source)) {
        this._renderIconContainer();
      }
    },

    /**
     * Generate icons.
     *
     * @param {string} icon - Icon to generate.
     * @return {string} - Generated icon HTML.
     */
    _iconGenerator: function _iconGenerator(icon) {
      if ('function' === typeof this.settings.iconGenerator) {
        return this.settings.iconGenerator(icon);
      }
      return '<i ' + (this.settings.useAttribute ?
        this.settings.attributeName + '="' +
        (this.settings.convertToHex ? '&#x' + parseInt(icon, 10).toString(16) + ';' :
          icon) + '"' : 'class="' + icon + '"') + '></i>';
    },

    /**
     * Render icons inside the popup.
     */
    _renderIconContainer: function _renderIconContainer() {
      let offset, iconsPaged;
      if (this.selectedCategory === 'all') {
        iconsPaged = this.uniqueIconsSource;
      } else {
        iconsPaged = this.isSearch ? this.iconsSearched : this.settings.source;
      }
      this.iconsCount = iconsPaged.length;
      this.totalPage = Math.ceil(this.iconsCount / this.settings.iconsPerPage);
      this.selectorPopup.find('.selector-footer').toggle(this.totalPage > 1);
      this.selectorPopup.find('.selector-pages').html(this.currentPage + '/' + this.totalPage + ' <em>(' + this.iconsCount + ')</em>');
      offset = (this.currentPage - 1) * this.settings.iconsPerPage;
      if (this.settings.emptyIcon) {
        this.iconContainer.html('<span class="faui-box" data-faui-value="faui-icon-block"><i class="faui-icon-block"></i></span>');
      } else if (iconsPaged.length === 0) {
        this.iconContainer.html('<span class="icons-picker-error" data-faui-value="faui-icon-block"><i class="faui-icon-block"></i></span>');
        return;
      } else {
        this.iconContainer.html('');
      }
      const uniqueIconsDisplayed = new Set();
      iconsPaged.slice(offset, offset + this.settings.iconsPerPage).forEach(icon => {
        if (!uniqueIconsDisplayed.has(icon)) {
          uniqueIconsDisplayed.add(icon);
          let fauiBoxTitle = icon;
          $.grep(this.settings.source, (e, i) => {
            if (e === icon) {
              fauiBoxTitle = this.searchValues[i];
              return true;
            }
            return false;
          });
          $('<span/>', {
            html: this._iconGenerator(icon),
            attr: { 'data-faui-value': icon },
            class: 'faui-box',
            title: fauiBoxTitle
          }).appendTo(this.iconContainer);
        }
      });
      if (!this.settings.emptyIcon && (!this.element.val() || !this.settings.source.includes(this.element.val()))) {
        this._setSelectedIcon(iconsPaged[0]);
      } else if (this.settings.defaultIcon) {
        this._setSelectedIcon(this.settings.defaultIcon);
      } else if (!this.settings.source.includes(this.element.val())) {
        this._setSelectedIcon('');
      } else {
        var passDefaultIcon = this.element.val() === this.settings.emptyIconValue ? 'faui-icon-block' : this.element.val();
        this._setSelectedIcon(passDefaultIcon);
      }
    },

    /**
     * Set highlighted icon.
     */
    _setHighlightedIcon: function _setHighlightedIcon() {
      this.iconContainer.find('.current-icon').removeClass('current-icon');
      if (this.currentIcon) {
        this.iconContainer.find('[data-faui-value="' + this.currentIcon + '"]').addClass('current-icon');
      }
    },

    /**
     * Set selected icon.
     *
     * @param {string} theIcon - The icon to select.
     */
    _setSelectedIcon: function _setSelectedIcon(theIcon) {
      if ('faui-icon-block' === theIcon) {
        theIcon = '';
      }
      var selectedIcon = this.iconPicker.find('.selected-icon');
      if ('' === theIcon) {
        selectedIcon.html('<i class="faui-icon-block"></i>');
      } else {
        selectedIcon.html(this._iconGenerator(theIcon));
      }
      var currentValue = this.element.val();
      this.element.val('' === theIcon ? this.settings.emptyIconValue : theIcon);
      if (currentValue !== theIcon) {
        this.element.trigger('change');
        if (null !== this.triggerEvent) {
          this.element.trigger(this.triggerEvent);
        }
      }
      this.currentIcon = theIcon;
      this._setHighlightedIcon();
    },

    /**
     * Recalculate the position of the popup.
     */
    _repositionIconSelector: function _repositionIconSelector() {
      var offset = this.iconPicker.offset(),
        offsetTop = offset.top + this.iconPicker.outerHeight(true),
        offsetLeft = offset.left;
      this.selectorPopup.css({
        left: offsetLeft,
        top: offsetTop
      });
    },

    /**
     * Fix window overflow of popup dropdown if needed.
     */
    _fixWindowOverflow: function _fixWindowOverflow() {
      var visibilityStatus = this.selectorPopup.find('.selector-popup').is(':visible');
      if (!visibilityStatus) {
        this.selectorPopup.find('.selector-popup').show();
      }
      var popupWidth = this.selectorPopup.outerWidth(),
        windowWidth = $(window).width(),
        popupOffsetLeft = this.selectorPopup.offset().left,
        containerOffset = 'self' == this.settings.appendTo ?
          this.selectorPopup.parent().offset() : $(this.settings.appendTo).offset();
      if (!visibilityStatus) {
        this.selectorPopup.find('.selector-popup').hide();
      }
      if (popupOffsetLeft + popupWidth > windowWidth - 20) {
        var pickerOffsetRight = this.selectorButton.offset().left +
          this.selectorButton.outerWidth();
        var preferredLeft = Math.floor(pickerOffsetRight - popupWidth - 1);
        if (0 > preferredLeft) {
          this.selectorPopup.css({
            left: windowWidth - 20 - popupWidth - containerOffset.left
          });
        } else {
          this.selectorPopup.css({
            left: preferredLeft
          });
        }
      }
    },

    /**
     * Fix on window resize.
     */
    _fixOnResize: function _fixOnResize() {
      if ('self' !== this.settings.appendTo) {
        this._repositionIconSelector();
      }
      this._fixWindowOverflow();
    },

    /**
     * Open/close popup (toggle).
     */
    _toggleIconSelector: function _toggleIconSelector() {
      this.open = !this.open ? 1 : 0;
      if (this.open) {
        if ('self' !== this.settings.appendTo) {
          this.selectorPopup.appendTo(this.settings.appendTo).css({
            zIndex: 1000
          }).addClass('icons-selector ' + this.settings.theme);
          this._repositionIconSelector();
        }
        this._fixWindowOverflow();
      }
      this.selectorPopup.find('.selector-popup').slideToggle(300, $.proxy(function () {
        this.iconPicker.find('.selector-button i').toggleClass('faui-icon-down-dir');
        this.iconPicker.find('.selector-button i').toggleClass('faui-icon-up-dir');
        if (this.open) {
          this.selectorPopup.find('.icons-search-input').trigger('focus').trigger('select');
        } else {
          this.selectorPopup.appendTo(this.iconPicker).css({
            left: '',
            top: '',
            zIndex: ''
          }).removeClass('icons-selector ' + this.settings.theme);
        }
      }, this));
    },

    /**
     * Reset search.
     */
    _resetSearch: function _resetSearch() {
      this.selectorPopup.find('.icons-search-input').val('');
      this.searchIcon.removeClass('faui-icon-cancel');
      this.searchIcon.addClass('faui-icon-search');
      this.currentPage = 1;
      this.isSearch = false;
      this._renderIconContainer();
    }
  };

  /**
   * Light weight wrapper to inject fontAwesomeIconPicker into jQuery.fn.
   *
   * @param {jQuery} $ - jQuery instance.
   * @return {boolean} - Whether the initialization was successful.
   */
  function fontAwesomeIconPickerShim($) {
    if (!$.fn) {
      return false;
    }
    if ($.fn && $.fn.fontAwesomeIconPicker) {
      return true;
    }
    $.fn.fontAwesomeIconPicker = function (options) {
      var _this = this;
      this.each(function () {
        if (!$.data(this, 'fontAwesomeIconPicker')) {
          $.data(this, 'fontAwesomeIconPicker', new FontAwesomeIconPicker(this, options));
        }
      });
      this.setIcons = function () {
        var newIcons = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
        var iconSearch = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        _this.each(function () {
          $.data(this, 'fontAwesomeIconPicker').setIcons(newIcons, iconSearch);
        });
      };
      this.setIcon = function () {
        var newIcon = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        _this.each(function () {
          $.data(this, 'fontAwesomeIconPicker').setIcon(newIcon);
        });
      };
      this.destroyPicker = function () {
        _this.each(function () {
          if (!$.data(this, 'fontAwesomeIconPicker')) {
            return;
          }
          $.data(this, 'fontAwesomeIconPicker').destroy();
          $.removeData(this, 'fontAwesomeIconPicker');
        });
      };
      this.refreshPicker = function (newOptions) {
        if (!newOptions) {
          newOptions = options;
        }
        _this.destroyPicker();
        _this.each(function () {
          if (!$.data(this, 'fontAwesomeIconPicker')) {
            $.data(this, 'fontAwesomeIconPicker', new FontAwesomeIconPicker(this, newOptions));
          }
        });
      };
      this.repositionPicker = function () {
        _this.each(function () {
          $.data(this, 'fontAwesomeIconPicker').resetPosition();
        });
      };
      this.setPage = function (pageNum) {
        _this.each(function () {
          $.data(this, 'fontAwesomeIconPicker').setPage(pageNum);
        });
      };
      return this;
    };
    return true;
  }

  /**
   * Initialize FontAwesomeIconPicker.
   *
   * @param {jQuery} jQuery$$1 - jQuery instance.
   * @return {boolean} - Whether the initialization was successful.
   */
  function initFontAwesomeIconPicker(jQuery$$1) {
    return fontAwesomeIconPickerShim(jQuery$$1);
  }

  if (jQuery && jQuery.fn) {
    initFontAwesomeIconPicker(jQuery);
  }

  var jquery_fontawesomeiconpicker = (function (jQuery$$1) {
    return initFontAwesomeIconPicker(jQuery$$1);
  });

  return jquery_fontawesomeiconpicker;

})));
