/**
 * @file
 * Contains the definition of the behavior for Font Awesome UI.
 */
(function ($, Drupal, drupalSettings, once) {
  "use strict";

  const tag = drupalSettings.fontAwesome.tag;

  Drupal.behaviors.fontAwesomeUI = {
    attach: function (context, settings) {
      const icons = drupalSettings.fontAwesome.icons;

      // Iterate through each icon configuration and apply the icon.
      $.each(icons, function (index, icon) {
        let options = {
          selector: icon.selector,
          position: icon.position,
          class: icon.class,
          style: icon.style,
        };

        // Apply icon to the single selector.
        if (once('fontAwesomeIcon', options.selector, context).length) {
          new Drupal.fontAwesomeIcon(options);
        }
      });
    }
  };

  /**
   * Applies Font Awesome classes and styles based on provided options.
   *
   * @param {object} options
   *   The options object containing selector, position, class, and style.
   */
  Drupal.fontAwesomeIcon = function (options) {
    // Check for the existence and non-empty values of class and style.
    const iconClass = options.class ? `class="${options.class}"` : '';
    const iconStyle = options.style ? `style="${options.style}"` : '';

    // Create the icon element string.
    const iconElement = `<${tag} ${iconClass} ${iconStyle}></${tag}>`;

    // Apply the icon element based on the specified position.
    switch (options.position) {
      case 'before':
        $(options.selector).before(iconElement);
        break;

      case 'append':
        $(options.selector).append(iconElement);
        break;

      case 'after':
        $(options.selector).after(iconElement);
        break;

      default:
        // Default action if position is not set or unrecognized.
        $(options.selector).prepend(iconElement);
        break;
    }
  };

})(jQuery, Drupal, drupalSettings, once);
